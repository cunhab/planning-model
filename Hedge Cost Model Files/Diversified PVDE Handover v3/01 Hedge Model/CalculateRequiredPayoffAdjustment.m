% --------------------CalculateRequiredPayoffAdjustment-------------------%
% Script #: 2.4.1
% Calls: N/A
% Variables: PreTaxHedgeNeed represents the target hedge gains (used in 
% GlobalOptimizer)
% InterestOnExcessCapital represents the interest gained on capital above
% CTE 90 (used in IndividualOuterLoopRun)
%
% This section calculates the target hedge gains in all capital market
% stresses relevant to the risk management framework
%-------------------------------------------------------------------------%

% Creates variable for TAR release from baseline (e.g. CTE 95)
TARRelease = zeros(1,numel(BaseSetofEquityShocks));

% Creates variable for the difference between beginning of year funding and
% the target funidng
ChangeInTAR = zeros(1,numel(BaseSetofEquityShocks));

% Creates variable for unadjusted target funding variable 
MasterCTESetOriginal = MasterCTESet;

% Determines the hedge need from available capital (excluding cash flows)
% and the maximum unhedged loss of the capital
for i = 1: numel(BaseSetofEquityShocks)  
%     TARRelease(i) = CTEValueBase(Node+1)/1000000000 - MasterCTESet(i); 
    TARRelease(i) = CombinedCTEBaselineShock(Node+1)/1000000000 - MasterCTESet(i);
    
    if KeepCapitalBuffer == 1 % keep capital buffer and use defined deductible
        Deductible = max([0, min([CapitalBuffer, DeductibleSchedule(Node),...
                        MasterFunding-CTEValueBase(Node)/1000000000])]);
        DeductibleCatch(Node) = Deductible;
        FundingUsedForHedgeLosses = CTEValueBase(Node)/1000000000+Deductible;
        if aboveCSV == 1
            % haircut accounts for CARVM decrease in equity shocks
            CARVMHaircut = min(HaircutFactor_CARVM * CARVMExpense(Node+1) * (BaseSetofEquityShocks(i)-1), 0);
            ChangeInTAR(i) = FundingUsedForHedgeLosses - (MasterCTESet(i) + CARVMExpense(Node+1) + CARVMHaircut);
        else
            ChangeInTAR(i) = FundingUsedForHedgeLosses - MasterCTESet(i);
        end
    else % use all available funding to reduce target hedge gains
        Deductible = NaN; % no deductible
        if aboveCSV == 1
            % haircut accounts for CARVM decrease in equity shocks
            CARVMHaircut = min(HaircutFactor_CARVM * CARVMExpense(Node+1) * (BaseSetofEquityShocks(i)-1), 0);
            ChangeInTAR(i) = MasterFunding - (MasterCTESet(i) + CARVMExpense(Node+1) + CARVMHaircut);
        else
            ChangeInTAR(i) = MasterFunding - MasterCTESet(i); 
        end
    end 
end 

InitialHedgeNeed = -ChangeInTAR;
InitialHedgeNeedCatch = InitialHedgeNeed;

disp(strcat('Funding = ', num2str(MasterFunding)));
disp(strcat('CTE 95 = ', num2str(CTEValueBase(Node+1)/1000000000)));
disp(strcat('Target funding = ', num2str(MasterCTESetOriginal)));

% Determines cash flows available to reduce target hedge gains.
% Pre-tax cash flows excluding interest on excess capital provided as an 
% input in MillimanInputs

% Sets reserves equal to CTE 70 post-stress
Reserves = CombinedSSRAllNodes(Node+1,:); 

% Tax reserve change proxied by the % change in SSR from the outer loop
if isShield == 1
    Reserves_noCSV = Reserves - ShieldCSVs(:,Node+1).';
    currentSSRBaseline = SSRBaselineGrid(Node+1) - NoShockCSVs(Node+1);
%     TaxReservePercentChange = Reserves./SSRBaselineGrid(Node+1);
    TaxReservePercentChange = Reserves_noCSV / currentSSRBaseline;
else
    TaxReservePercentChange = Reserves./SSRBaselineGrid(Node+1);
end
% Tax reserve change equal to the estimated tax reserve in a shock less the
% tax reserve in a baseline scenario
TaxReserveChange = TaxReservePercentChange*TaxReserve(Node+1)-TaxReserve(Node);

if newBusiness == 1
    if isShield == 1
%         CSVChange = zeros(size(BaseSetofEquityShocks));
%         for i_shock = 1:numel(BaseSetofEquityShocks) 
%             if BaseSetofEquityShocks(i_shock) < .9
%                 CSVChange(i_shock) = Base;
%             elseif BaseSetofEquityShocks(i_shock) > 1
%                 'add code'
%             end
%         end
%         noShield = (BaseSetofEquityShocks-1) < -.1 | (BaseSetofEquityShocks-1) > .2;
%         CSVChange = noShield .* (BaseSetofEquityShocks-1);
        CSVChange = ShieldCSVs(:,Node+1).' - NoShockCSVs(Node); 
        TaxReserveChange = TaxReserveChange + CSVChange;
    end
end
% DRD provided from MillimanInputs

% Additional interest on capital held above CTE 90 not accounted for in cash flows
% using the same rate in the cash flow statement
if(noInterestOnCapital == 1) 
    InterestOnExcessCapital = 0;
else
    InterestOnExcessCapital=((MasterFunding) - CTEValue90Base(Node))*.035;
end
%%
% Any haircut on fees to reflect the chance of a shock occurring earlier in
% the year
Haircut = zeros(1,numel(BaseSetofEquityShocks));
% REMOVED (1+ EquityShocksBase(1)) because base equity shock is always 0

CashflowShock = OuterLoopReal(Node+2,2)/((OuterLoopReal(Node+1,2)*OuterLoopBaseline(Node+2,2))/OuterLoopBaseline(Node+1,2))-1;
if UseRescaledShocks == 0 || (UseBaselineExpectation == 1 && Node == 1)
    CashflowShock = 0;
end;
ProjectedBaselineFees = RevenueFees/(1+CashflowShock*HaircutCashflows);
for i = 1:numel(Haircut)
    Haircut(i) = HaircutCashflows*ProjectedBaselineFees(Node+1)*(BaseSetofEquityShocks(i)-1);
%     Haircut(i) = HaircutCashflows*RevenueFees(Node+1)*(BaseSetofEquityShocks(i)-1);
    if Haircut(i) > 0 
        Haircut(i) = 0;
    end 
end 
%%
% Taxable income determined as cash flow from fees and investment income
% less expenses, minus the change in tax reserves minus dividend received
% deduction. 
TaxableIncome = PreTaxCashFlow(Node+1)+InterestOnExcessCapital-TaxReserveChange-DRD(Node+1)-CARVMExpense(Node+1)+Haircut;

% Tax estimated as 35% of taxable income
Tax = TaxableIncome*inputTaxRate;

% No DTA admittance floors tax at 0
if LimitDTANone == 1
    for i = 1:numel(Tax)
         if Tax(i) < 0
             Tax(i) = 0;
         end
    end
end

% Net cash flows determined as pre tax cash flows, less taxes, plus the
% interest on excess capital
NetCashFlow = PreTaxCashFlow(Node+1)+InterestOnExcessCapital-Tax+Haircut;

% if hedge strategy has a dynamic hedge components, include dynamic hedge
% payoffs
if hasDynamicHedge == 1
    NetCashFlow = NetCashFlow + dynamicHedgePayoff_bn(:,Node+1).';
end
 
% 
% Final hedge need is the initial hedge need less any cash flows received
% in the course of the node
FinalHedgeNeed = InitialHedgeNeed-NetCashFlow;
disp(strcat('Pre-floor final hedge need', num2str(FinalHedgeNeed)));

% Floors bear market scenarios required hedge payoff to 0, as in down
% markets selling upside should not be permitted (measured by both equities
% and rates)
if FloorBearMarket == 1
    for i = 1:numel(BaseSetofEquityShocks)
        if TARRelease(i) <= 0
            FinalHedgeNeed(i) = max(FinalHedgeNeed(i),0);
        end
    end
end

% Sets the allowed upside selling equal to the user-specified amount of TAR
% release
AllowedUpsideSellingPostTax = TARRelease;
    
% For down scenarios, determination of required hedge payout as what
% was previously calculated. For up scenarios, determined by capital release
% Down/up scenario defined by scenarios where target
% funding increase/decrease  
for i = 1: numel(BaseSetofEquityShocks)
    if TARRelease(i) >= 0
        FinalHedgeNeed(i) = -AllowedUpsideSellingPostTax(i);
    end 
end 

% Account for tax of the hedge payout that is above taxable income. If
% negative taxable income is greater than hedge gains, then gains are not
% taxed. If taxable income is negative but not greater than hedge gains,
% only the portion above taxable income. If taxable income is positive,
% then hedge gains are fully taxed. 
OptionSelected = zeros(1,numel(FinalHedgeNeed));
PreTaxHedgeNeed = zeros(1,numel(FinalHedgeNeed));
if MakeTaxAdjustment == 1
    for i = 1:numel(BaseSetofEquityShocks)
        if TaxableIncome(i) < 0 && (-TaxableIncome(i) >= FinalHedgeNeed(i))
            PreTaxHedgeNeed(i) = -FinalHedgeNeed(i);
            OptionSelected(i) = 0;
        elseif TaxableIncome(i) < 0 && (-TaxableIncome(i) < FinalHedgeNeed(i))
            PreTaxHedgeNeed(i) = -((-ChangeInTAR(i) - NetCashFlow(i) + TaxableIncome(i))/(1-inputTaxRate) - TaxableIncome(i));
            OptionSelected(i) = 0.5;
        elseif TaxableIncome(i) > 0 
            PreTaxHedgeNeed(i) = -FinalHedgeNeed(i)/(1-inputTaxRate);
            OptionSelected(i) = 1;
        end 
    end 
end

disp(strcat('Pre-tax hedge need = ', num2str(-PreTaxHedgeNeed)));

