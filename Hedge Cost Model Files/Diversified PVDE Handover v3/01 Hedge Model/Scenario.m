classdef Scenario
    %UNTITLED7 Summary of this class goes here
    %   Detailed explanation goes here
    
    properties
        equityRates
        treasuryRates
        swapRates
        rateType
        Compounding
        timestepPerYear
        t0value
        t0date
        isPriceIndex
        annualIndex
        PriceIndexAdjustment
        monthsPerTimestep
    end
    
    methods
        function obj = Scenario(scenarioSettings, varargin)
            obj.equityRates = scenarioSettings.inputScen{:,2};
            if ~ismember(scenarioSettings.rateType, {'annual', 'continuous'})
                error('rateType is invalid. Must be ''continuous'' or ''annual''.');
            end
            obj.rateType = scenarioSettings.rateType;
            if strcmp(obj.rateType, 'continuous')
                obj.Compounding = -1;
            elseif rstrcmp(obj.rateType, 'annual')
                obj.Compounding = 1;
            end
            obj.timestepPerYear = scenarioSettings.timestepPerYear;
            obj.monthsPerTimestep = 12 / obj.timestepPerYear;
            obj.t0value = scenarioSettings.SPX_T0;
            
            p = inputParser;
            addParameter(p,'isPriceIndex', false);
            addParameter(p,'PriceIndexAdjustment', 0);
            addParameter(p,'t0date', datenum(2016,3,31));
            parse(p,varargin{:});
            obj.isPriceIndex = p.Results.isPriceIndex;
            obj.PriceIndexAdjustment = p.Results.PriceIndexAdjustment;
            obj.t0date = p.Results.t0date;
            
            obj.annualIndex = zeros((size(obj.equityRates, 1) - 1) / obj.timestepPerYear + 1, 1);
            obj.annualIndex(1) = obj.t0value;
            if strcmp(obj.rateType, 'continuous')
                obj.annualIndex(2) = obj.t0value * exp(obj.equityRates(1));
            else
                obj.annualIndex(2) = obj.t0value * obj.equityRates(1);
            end
            for i = 3:length(obj.annualIndex)
                if strcmp(obj.rateType, 'continuous')
                    annualRate = prod(exp(obj.equityRates(((i - 3) * obj.timestepPerYear + 2):  ((i - 2) * obj.timestepPerYear + 1))));
                else
                    annualRate = prod(obj.equityRates(((i - 3) * obj.timestepPerYear + 1):  ((i - 2) * obj.timestepPerYear + 1)));
                end
                if obj.isPriceIndex == 1
                    annualRate = annualRate + obj.PriceIndexAdjustment;
                end
                obj.annualIndex(i) = obj.annualIndex(i-1) * annualRate;
            end
            obj.treasuryRates = scenarioSettings.inputScen(:,contains(scenarioSettings.inputScen.Properties.VariableNames, scenarioSettings.TreasuryRateKey));
            obj.swapRates = scenarioSettings.inputScen(:,contains(scenarioSettings.inputScen.Properties.VariableNames, scenarioSettings.SwapRateKey));
        end
        
        
        function [equityReturn] = getEquityReturn(obj, start_date, end_date, varargin)
            p = inputParser;
            addParameter(p,'Compounding', 0);
            parse(p,varargin{:});
            OutputCompounding = p.Results.Compounding;
                
            % assumes start_date and end_date are all in monthly increments
            if start_date == end_date
                if OutputCompounding == 0
                    equityReturn = 1;
                elseif OutputCompounding == -1
                    equityReturn = 0;
                end
            else
                start_idx = months(obj.t0date, start_date) / obj.monthsPerTimestep + 2; % +1 since matlabe indices start at 1, and +1 because start after the start date
                end_idx = months(obj.t0date, end_date) / obj.monthsPerTimestep + 1;

                if obj.Compounding == -1
                    if OutputCompounding == 0
                        equityReturn = prod(exp(obj.equityRates(start_idx:end_idx)));
                    elseif OutputCompounding == -1
                        equityReturn = log(prod(obj.equityRates(start_idx:end_idx)));
                    end
                end
            end
        end
        
        function [swapRate] = getSwapRates(obj, settle_date, start_date, end_date, varargin)
            p = inputParser;
            addParameter(p,'Compounding', -1);
            parse(p,varargin{:});
            OutputCompounding = p.Results.Compounding;
            
            settle_idx = months(obj.t0date, settle_date) / obj.monthsPerTimestep + 1; 
            start_month = months(settle_date, start_date) / 12;
            end_month = months(settle_date, end_date) / 12;
            start_col = floor(start_month) + 1;
            end_col = floor(end_month) + 1;
            
            rate_prop = ones(1, max(end_col - start_col + 1, 1));
            if start_col == end_col
                rate_prop(1) = end_month - start_month;
            else
                rate_prop(1) = start_col - start_month;
                rate_prop(end) = end_month - (end_col - 1);
            end
            swapRate = obj.swapRates{settle_idx, start_col:end_col} .* rate_prop;

            if obj.Compounding == -1
                if OutputCompounding == 0
                    swapRate = prod(exp(swapRate));
                elseif OutputCompounding == -1
                    swapRate = log(prod(exp(swapRate))) / (end_month - start_month) * -OutputCompounding;
                end
            end
        end
        
        function [growthRate] = getSimpleGrowthRate(obj, year)
            if strcmp(obj.rateType, 'continuous')
                growthRate = prod(exp(obj.rates(((year-1) * obj.timestepPerYear + 1):(year * obj.timestepPerYear + 1))));
            else
                growthRate = prod(obj.rates(((year - 1) * obj.timestepPerYear + 1):(year * obj.timestepPerYear + 1)));
            end
            if obj.isPriceIndex == 1
                growthRate = growthRate + obj.PriceIndexAdjustment;
            end
        end
        
        % year references the end of that year
        function [treasuryRate] = getTreasuryRate(obj, year, startDelay, maturityFromStart)
            if strcmp(obj.rateType, 'continuous')
                treasuryRate = mean(obj.treasuryRates{year * obj.timestepPerYear + 1, startDelay+1:startDelay+maturityFromStart}, 2);
            end
        end
        
        function [swapRate] = getSwapRate(obj, year, startDelay, maturityFromStart) % i.e. maturityFromStart = 10 for 1 year delay, 10 year swap
            if strcmp(obj.rateType, 'continuous')
                swapRate = mean(obj.swapRates{year * obj.timestepPerYear + 1, startDelay+1:startDelay+maturityFromStart}, 2);
            end
        end
    end
    
end

