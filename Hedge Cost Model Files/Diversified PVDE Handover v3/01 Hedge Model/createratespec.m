function [ SwapRateSpec ] = createratespec( SwapRates, CurrentDate)
    %CREATESWAPRATESPEC Creates 
    %   Detailed explanation goes here
    
    %Create a set of start dates for those treasury valuations
    StartDates= CurrentDate*ones(8,1);
    %Create a set of end dates for those treasuries tenors
    EndDates = [addtodate(CurrentDate,1,'year');addtodate(CurrentDate,2,'year');addtodate(CurrentDate,3,'year');addtodate(CurrentDate,4,'year');addtodate(CurrentDate,5,'year');addtodate(CurrentDate,7,'year');addtodate(CurrentDate,10,'year');addtodate(CurrentDate,20,'year')];
    %Sets the Compounding structure; set at continous
    Compounding = -1;
    SwapRateSpec= intenvset('Rates', SwapRates, 'StartDates', StartDates,'EndDates',EndDates, 'Compounding', Compounding);
    
    % Floors rates at 0
    for i = 1:numel(SwapRateSpec.Rates)
        if SwapRateSpec.Rates(i) < 0
            SwapRateSpec.Rates(i) = 0;
        end
    end
end

