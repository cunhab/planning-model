classdef HedgePortfolio < handle
    %UNTITLED2 Summary of this class goes here
    %   Detailed explanation goes here
    
    properties
        allInstruments
        options
        equityspread
        swaptions
        swaptionspread
        allSwaps
        NodeSwaps
        allInstrumentsCurrent
        StartingDate
        ExpectedEquityReturnsAnnual
    end
    
    methods
        function obj = HedgePortfolio(fileInput)
            obj.allInstruments = readtable(fileInput, 'Sheet', 'Instruments');
            
            equityspread = obj.allInstruments(ismember(obj.allInstruments.Instrument, {'Call spread', 'Put spread'}),:);
            optionInstrument = regexp(equityspread.Instrument, ' spread', 'split');
            optionInstrument = arrayfun(@(n) optionInstrument{n}(1),1:size(equityspread,1))';
            equityspread.OptionInstrument = optionInstrument;
            
            swaptionspread = obj.allInstruments(ismember(obj.allInstruments.Instrument, {'Receiver swaption spread', 'Payer swaption spread'}),:);
            swaptionInstrument = regexp(swaptionspread.Instrument, ' spread', 'split');
            swaptionInstrument = arrayfun(@(n) swaptionInstrument{n}(1),1:size(swaptionspread,1))';
            swaptionspread.SwaptionInstrument = swaptionInstrument;
            
            obj.equityspread = equityspread;
            obj.swaptionspread = swaptionspread;
        end
        
        function[allInstrumentsCurrent] = initializeInstruments(obj, marketConditions, varargin)
            p = inputParser;
            addParameter(p,'useOldSwapsPricing', true);
            addParameter(p,'useContinuousSwaps', false);
            parse(p,varargin{:});
            useOldSwapsPricing = p.Results.useOldSwapsPricing;
            useContinuousSwaps = p.Results.useContinuousSwaps;
            
            CurrentIndex = marketConditions.CurrentIndex;
            ExpectedEquityReturns = marketConditions.ExpectedEquityReturns;
            priceOnIndex = marketConditions.priceOnIndex;
            SwapRatesContinuous = marketConditions.SwapRatesContinuous;
            CurrentDate = marketConditions.CurrentDate;
            SwapRateSpec = marketConditions.SwapRateSpec;
            
            allInstrumentsCurrent = obj.allInstruments;
            allInstrumentsCurrent.BuyStrikeActual = nan(size(allInstrumentsCurrent,1),1);
            allInstrumentsCurrent.SellStrikeActual = nan(size(allInstrumentsCurrent,1),1);
            allInstrumentsCurrent.Price = nan(size(allInstrumentsCurrent,1),1);
            allInstrumentsCurrent.BuyVega = nan(size(allInstrumentsCurrent,1),1);
            allInstrumentsCurrent.SellVega = nan(size(allInstrumentsCurrent,1),1);
            allInstrumentsCurrent.Sigma = nan(size(allInstrumentsCurrent,1),1);
            %% OPTION
            options = struct; % contains the EquityOption Class objects
            optionTerms = unique(allInstrumentsCurrent(ismember(allInstrumentsCurrent.Instrument, 'Call'), 'Term'));
            optionTerms = table2array(optionTerms);
            for i = 1:numel(optionTerms)
                options(i).TermMonths = optionTerms(i) * 12; % term expressed in terms of months
                iCallRows = (ismember(allInstrumentsCurrent.Instrument, 'Call') & allInstrumentsCurrent.Term == optionTerms(i));
                iPutRows = (ismember(allInstrumentsCurrent.Instrument, 'Put') & allInstrumentsCurrent.Term == optionTerms(i));
                CallStrikePercent = allInstrumentsCurrent{iCallRows,'BuyStrike'};
                PutStrikePercent = allInstrumentsCurrent{iPutRows,'BuyStrike'};
                options(i).Option = EquityOption(CurrentDate, options(i).TermMonths, CurrentIndex, ExpectedEquityReturns, CallStrikePercent, PutStrikePercent, priceOnIndex);
                allInstrumentsCurrent{iCallRows,'BuyStrikeActual'} = options(i).Option.CallStrike;
                allInstrumentsCurrent{iPutRows, 'BuyStrikeActual'} = options(i).Option.PutStrike;
            end
            
            %% option spread
            allInstrumentList = allInstrumentsCurrent(:, {'Instrument', 'Term', 'BuyStrike', 'BuyStrikeActual'});
            allInstrumentList.Properties.VariableNames{'Instrument'} = 'OptionInstrument';
            equityspread = obj.equityspread;
            [~,ia,ib] = intersect(obj.equityspread(:,{'OptionInstrument', 'Term', 'BuyStrike'}),...
                allInstrumentList(:,{'OptionInstrument', 'Term', 'BuyStrike'}));
            equityspread{ia, 'BuyStrikeActual'} = allInstrumentList{ib, 'BuyStrikeActual'};
            allInstrumentList.Properties.VariableNames{'BuyStrike'} = 'SellStrike';
            [~,ia,ib] = intersect(obj.equityspread(:,{'OptionInstrument', 'Term', 'SellStrike'}),...
                allInstrumentList(:,{'OptionInstrument', 'Term', 'SellStrike'}));
            equityspread{ia, 'SellStrikeActual'} = allInstrumentList{ib, 'BuyStrikeActual'};
            
            [~,ia,ib] = intersect(allInstrumentsCurrent(:,{'Instrument', 'Term', 'BuyStrike', 'SellStrike'}),...
                equityspread(:,{'Instrument', 'Term', 'BuyStrike', 'SellStrike'}));
            allInstrumentsCurrent{ia, {'BuyStrikeActual', 'SellStrikeActual'}} = equityspread{ib, {'BuyStrikeActual', 'SellStrikeActual'}};
            
            %% SWAPTION
            swaptions = struct;
            swaptionTerms = unique(allInstrumentsCurrent(ismember(allInstrumentsCurrent.Instrument, {'Receiver swaption', 'Payer swaption'}),{'Term', 'Underlying'}));
            for i = 1:size(swaptionTerms,1)
                iTerm = swaptionTerms.Term(i);
                iUnderlying = swaptionTerms.Underlying(i);
                swaptions(i).TermMonths = iTerm * 12;
                swaptions(i).UnderlyingYears = iUnderlying;
                iPutRows = (allInstrumentsCurrent.Term == iTerm & allInstrumentsCurrent.Underlying == iUnderlying & ismember(allInstrumentsCurrent.Instrument, 'Receiver swaption'));
                iCallRows = (allInstrumentsCurrent.Term == iTerm & allInstrumentsCurrent.Underlying == iUnderlying & ismember(allInstrumentsCurrent.Instrument, 'Payer swaption'));
                PutStrikePercentIR = allInstrumentsCurrent{iPutRows, 'BuyStrike'};
                CallStrikePercentIR = allInstrumentsCurrent{iCallRows, 'BuyStrike'};
                iSwap = mean(SwapRatesContinuous(iTerm+1:iTerm+iUnderlying)); 
                swaptions(i).Swaption = Swaption(CurrentDate, swaptions(i).TermMonths, PutStrikePercentIR, CallStrikePercentIR, iUnderlying, iSwap, 1);
                allInstrumentsCurrent{iPutRows, 'BuyStrikeActual'} = swaptions(i).Swaption.PutStrikeIR;
                allInstrumentsCurrent{iCallRows, 'BuyStrikeActual'} = swaptions(i).Swaption.CallStrikeIR;
            end
            %% swaption spread
            allInstrumentList = allInstrumentsCurrent(:, {'Instrument', 'Term', 'Underlying', 'BuyStrike', 'BuyStrikeActual'});
            allInstrumentList.Properties.VariableNames{'Instrument'} = 'SwaptionInstrument';
            swaptionspread = obj.swaptionspread;
            [~,ia,ib] = intersect(obj.swaptionspread(:,{'SwaptionInstrument', 'Term', 'Underlying', 'BuyStrike'}),...
                allInstrumentList(:,{'SwaptionInstrument', 'Term', 'Underlying', 'BuyStrike'}));
            swaptionspread{ia, 'BuyStrikeActual'} = allInstrumentList{ib, 'BuyStrikeActual'};
            allInstrumentList.Properties.VariableNames{'BuyStrike'} = 'SellStrike';
            [~,ia,ib] = intersect(obj.swaptionspread(:,{'SwaptionInstrument', 'Term', 'Underlying', 'SellStrike'}),...
                allInstrumentList(:,{'SwaptionInstrument', 'Term', 'Underlying', 'SellStrike'}));
            swaptionspread{ia, 'SellStrikeActual'} = allInstrumentList{ib, 'BuyStrikeActual'};
            
            [~,ia,ib] = intersect(allInstrumentsCurrent(:,{'Instrument', 'Term', 'Underlying', 'BuyStrike', 'SellStrike'}),...
                swaptionspread(:,{'Instrument', 'Term', 'Underlying', 'BuyStrike', 'SellStrike'}));
            allInstrumentsCurrent{ia, {'BuyStrikeActual', 'SellStrikeActual'}} = swaptionspread{ib, {'BuyStrikeActual', 'SellStrikeActual'}};
            
            %% Swaps
            
            % determine fixed rate at initial issue such that price = 0
            allSwaps = unique(allInstrumentsCurrent(ismember(allInstrumentsCurrent.Instrument, {'Swap'}),{'Instrument', 'Term', 'Underlying', 'BuyStrike'}));
            if useOldSwapsPricing == 1
                % Sets the settle date as the current date from the macro environment set
                % in MasterMacroHedgeOL
                SettleSwap = CurrentDate;
                StartDate = rowfun(@(x) addtodate(SettleSwap, x, 'year'), allSwaps, 'InputVariables', 'Term');
                StartDate = table2array(StartDate);
                allSwaps.StartDate = StartDate;
                Maturity = rowfun(@(x, y) addtodate(y, x, 'year'), allSwaps, 'InputVariables', {'Underlying', 'StartDate'});
                Maturity = table2array(Maturity);
                allSwaps.Maturity = Maturity;
                
                % Calculate and set the fixed rate that makes swap prices zero at initial purchase
                LegReset = [4, 4]; % swaps settled quarterly
                SwapPrincipal = [1];
                LegRate = [nan(size(allSwaps.BuyStrike)) allSwaps.BuyStrike];
                LegType = [1 0]; % Receive fixed
                
                % correct 1 year floating rate
                allSwaps.FloatingRate = ones(numel(allSwaps.BuyStrike),1) * SwapRateSpec.Rates(1);

                if useContinuousSwaps == 0
                    [~, SwapRate] = swapbyzero(SwapRateSpec, LegRate, SettleSwap, Maturity, 'LegReset', LegReset, 'Principal', SwapPrincipal,...
                        'LegType', LegType, 'StartDate', StartDate);
                    allSwaps.FixedCouponRateCC = SwapRate;
                    
                    % convert swap rates to annual form
                    OriginalRates = SwapRateSpec.Rates;
                    ZeroRates = exp(SwapRateSpec.Rates)-1;
                    SwapRateSpec.Rates = ZeroRates;
                    SwapRateSpec.Compounding = 1;
                    [SwapPrice, SwapRate] = swapbyzero(SwapRateSpec, LegRate, SettleSwap, Maturity, 'LegReset', LegReset, 'Principal', SwapPrincipal,...
                        'LegType', LegType, 'StartDate', StartDate);
                    allSwaps.FixedCouponRate = SwapRate;
                    allSwaps.Price = SwapPrice;
                    
                    % TEST CODE
                    
                    NodeSwaps = Swaps(CurrentDate, StartDate, allSwaps.Underlying, SwapRateSpec);
                    
                    % cache fixed coupon rate
                    % SwapRateCatch(Node) = allSwaps.FixedCouponRate(1);
                    SwapRateSpec.Rates = OriginalRates;
                    SwapRateSpec.Compounding = -1;
                else
                    [SwapPrice, SwapRate] = swapbyzero(SwapRateSpec, LegRate, SettleSwap, Maturity, 'LegReset', LegReset, 'Principal', SwapPrincipal,...
                        'LegType', LegType, 'StartDate', StartDate);
                    allSwaps.FixedCouponRateCC = SwapRate;
                    allSwaps.Price = SwapPrice;
                    
                    NodeSwaps = Swaps(StartingDate, allSwaps.Underlying, SwapRateSpec);
                end
            else
                if useContinuousSwaps == 0
                    OriginalRates = SwapRateSpec.Rates;
                    ZeroRates = exp(SwapRateSpec.Rates)-1;
                    SwapRateSpec.Rates = ZeroRates;
                    SwapRateSpec.Compounding = 1;
                end
                
                NodeSwaps = Swaps(StartingDate, allSwaps.Underlying, SwapRateSpec);
                
                if useContinuousSwaps == 0
                    SwapRateSpec.Rates = OriginalRates;
                    SwapRateSpec.Compounding = -1;
                end
                
                
            end

            [~,ia,ib] = intersect(allInstrumentsCurrent(:,{'Instrument', 'Term', 'Underlying', 'BuyStrike'}),...
                allSwaps(:,{'Instrument', 'Term', 'Underlying', 'BuyStrike'}));
            allInstrumentsCurrent(ia, 'Price') = allSwaps(ib, 'Price');
            
            obj.ExpectedEquityReturnsAnnual = ExpectedEquityReturns(1);
            allInstrumentsCurrent{ismember(allInstrumentsCurrent.Instrument, {'Future'}), 'BuyStrikeActual'} = CurrentIndex;

            % set the Instrument objects in the properties
            obj.options = options;
            obj.swaptions = swaptions;
            obj.NodeSwaps = NodeSwaps;
            obj.allSwaps = allSwaps;
            obj.allInstrumentsCurrent = allInstrumentsCurrent;
            obj.StartingDate = CurrentDate;
        end
        
        function [priceArray, allInstrumentsCurrent] = priceInstruments(obj, ShockDelay, CurrentIndex, EquityVols, ImpliedVolsRates, SwapRates_Q, SwapRateSpec, currentFloatingRate, varargin)
            p = inputParser;
            addParameter(p,'useOldSwapsPricing', true);
            addParameter(p,'useContinuousSwaps', false);
            addParameter(p,'testSwapDifference', false);
            parse(p,varargin{:});
            useOldSwapsPricing = p.Results.useOldSwapsPricing;
            useContinuousSwaps = p.Results.useContinuousSwaps;
            testSwapDifference = p.Results.testSwapDifference;
            
            allInstrumentsCurrent = obj.allInstrumentsCurrent;
 
            % price options
            for i = 1:numel(obj.options)
                iCallRows = (ismember(allInstrumentsCurrent.Instrument, 'Call') & allInstrumentsCurrent.Term == obj.options(i).TermMonths / 12);
                iPutRows = (ismember(allInstrumentsCurrent.Instrument, 'Put') & allInstrumentsCurrent.Term == obj.options(i).TermMonths / 12);
                [PutPrice, CallPrice, PutVega, CallVega, PutSigma, CallSigma] = getEquityOptionPrice(obj.options(i).Option, ShockDelay, CurrentIndex, EquityVols, SwapRateSpec, 1);
                allInstrumentsCurrent{iCallRows,{'Price', 'BuyVega', 'Sigma'}} = [CallPrice, CallVega, CallSigma];
                allInstrumentsCurrent{iPutRows, {'Price', 'BuyVega', 'Sigma'}} = [PutPrice, PutVega, PutSigma];
            end
            %% Option spread
            % calculate equity spread price
            allInstrumentList = allInstrumentsCurrent(:, {'Instrument', 'Term', 'BuyStrike', 'Price', 'BuyVega'});
            allInstrumentList.Properties.VariableNames{'Instrument'} = 'OptionInstrument';
            equityspread = obj.equityspread;
            [~,ia,ib] = intersect(obj.equityspread(:,{'OptionInstrument', 'Term', 'BuyStrike'}),...
                allInstrumentList(:,{'OptionInstrument', 'Term', 'BuyStrike'}));
            equityspread{ia, {'BuyPrice', 'BuyVega'}} = allInstrumentList{ib, {'Price', 'BuyVega'}};
            allInstrumentList.Properties.VariableNames{'BuyStrike'} = 'SellStrike';
            [~,ia,ib] = intersect(obj.equityspread(:,{'OptionInstrument', 'Term', 'SellStrike'}),...
                allInstrumentList(:,{'OptionInstrument', 'Term', 'SellStrike'}));
            equityspread{ia, {'SellPrice', 'SellVega'}} = allInstrumentList{ib, {'Price', 'BuyVega'}};
            equityspread.Price = equityspread.BuyPrice - equityspread.SellPrice;
            
            [~,ia,ib] = intersect(allInstrumentsCurrent(:,{'Instrument', 'Term', 'BuyStrike', 'SellStrike'}),...
                equityspread(:,{'Instrument', 'Term', 'BuyStrike', 'SellStrike'}));
            allInstrumentsCurrent{ia, {'Price', 'BuyVega', 'SellVega'}} = equityspread{ib, {'Price', 'BuyVega', 'SellVega'}};
            %% SWAPTION
            % price swaptions
            for i = 1:numel(obj.swaptions)
                iTerm = obj.swaptions(i).TermMonths / 12;
                iUnderlying = obj.swaptions(i).UnderlyingYears;
                iPutRows = (allInstrumentsCurrent.Term == iTerm & allInstrumentsCurrent.Underlying == iUnderlying & ismember(allInstrumentsCurrent.Instrument, 'Receiver swaption'));
                iCallRows = (allInstrumentsCurrent.Term == iTerm & allInstrumentsCurrent.Underlying == iUnderlying & ismember(allInstrumentsCurrent.Instrument, 'Payer swaption'));
                iImpliedVols = ImpliedVolsRates([ImpliedVolsRates.Underlying] == iUnderlying).Surface;
                [PutSwaption, CallSwaption, PutVol, CallVol] = obj.swaptions(i).Swaption.getSwaptionPrice(ShockDelay, SwapRates_Q, iImpliedVols, SwapRateSpec);
                allInstrumentsCurrent{iPutRows, 'Price'} = PutSwaption;
                allInstrumentsCurrent{iCallRows, 'Price'} = CallSwaption;
            end
            %% Swaption spread
            % calculate swaption spread price
            allInstrumentList = allInstrumentsCurrent(:, {'Instrument', 'Term', 'Underlying', 'BuyStrike', 'Price'});
            allInstrumentList.Properties.VariableNames{'Instrument'} = 'SwaptionInstrument';
            swaptionspread = obj.swaptionspread;
            [~,ia,ib] = intersect(obj.swaptionspread(:,{'SwaptionInstrument', 'Term', 'Underlying', 'BuyStrike'}),...
                allInstrumentList(:,{'SwaptionInstrument', 'Term', 'Underlying', 'BuyStrike'}));
            swaptionspread{ia, 'BuyPrice'} = allInstrumentList{ib, 'Price'};
            allInstrumentList.Properties.VariableNames{'BuyStrike'} = 'SellStrike';
            [~,ia,ib] = intersect(obj.swaptionspread(:,{'SwaptionInstrument', 'Term', 'Underlying', 'SellStrike'}),...
                allInstrumentList(:,{'SwaptionInstrument', 'Term', 'Underlying', 'SellStrike'}));
            swaptionspread{ia, 'SellPrice'} = allInstrumentList{ib, 'Price'};
            swaptionspread.Price = swaptionspread.BuyPrice - swaptionspread.SellPrice;
            
            [~,ia,ib] = intersect(allInstrumentsCurrent(:,{'Instrument', 'Term', 'Underlying', 'BuyStrike', 'SellStrike'}),...
                swaptionspread(:,{'Instrument', 'Term', 'Underlying', 'BuyStrike', 'SellStrike'}));
            allInstrumentsCurrent{ia, 'Price'} = swaptionspread{ib, 'Price'};
            
            
            %% Swaps
            
            % used fixed rate at initial issue such that price = 0
            allSwaps = obj.allSwaps;
            NodeSwaps = obj.NodeSwaps;
            
            % Set the floating rate as the 1 year spot swap rate
            if ShockDelay ~= 0
                allFloatingRates = interp1([0; ShockDelay], [allSwaps.FloatingRate(1); currentFloatingRate],0:3:ShockDelay);
            end
            
            %Set Current Date based on the length of the ShockDelay
            CurrentDate=addtodate(obj.StartingDate,ShockDelay,'month');
            %Prices the swaps
            TestSwaps;
            
            [~,ia,ib] = intersect(allInstrumentsCurrent(:,{'Instrument', 'Term', 'Underlying', 'BuyStrike'}),...
                allSwaps(:,{'Instrument', 'Term', 'Underlying', 'BuyStrike'}));
            allInstrumentsCurrent(ia, 'Price') = allSwaps(ib, 'Price');
            
            % Price future
            ExpectedIndex = ((obj.ExpectedEquityReturnsAnnual - 1) * ShockDelay / 12 + 1) * allInstrumentsCurrent{ismember(allInstrumentsCurrent.Instrument, {'Future'}),'BuyStrikeActual'};
            allInstrumentsCurrent{ismember(allInstrumentsCurrent.Instrument, {'Future'}),'Price'} = ExpectedIndex - CurrentIndex;
            
            priceArray = allInstrumentsCurrent.Price;
        end
    end
    
end

