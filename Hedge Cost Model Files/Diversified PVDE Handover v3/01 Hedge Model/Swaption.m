% ----------------------------------Swaptions-----------------------------%
% Script #: 
% Calls: 
% Variables: 
%
% This class inclues all the methods for pricing swaptions
%-------------------------------------------------------------------------%

classdef Swaption
   properties (SetAccess = protected) % should it be immutable?
       PutStrikeMoneynessIR % NSTRIKES
       CallStrikeMoneynessIR
       PutStrikeIR
       CallStrikeIR
       StartingDate
       Term
       PutExerciseDatesIR
       CallExerciseDatesIR
       PutMaturitiesIR
       CallMaturitiesIR
       SwapTenor
       Spread = 0
       OptSpecPutIR
       OptSpecCallIR
       Principal = 1
   end
   
   methods
       % constructor of equity option starting on StartingDate with given 
       % Term (in months), CallStrike and PutStrike moneyness levels
       % ForwardSwap is the expected fixed rate for a Swap starting in Term
       % months with tenor of SwapTenor years
      function obj = Swaption(StartingDate, Term, PutStrikeMoneynessIR, CallStrikeMoneynessIR, SwapTenor, ForwardSwap, Principal)
          obj.StartingDate = StartingDate;
          obj.Term = Term;
          obj.SwapTenor = SwapTenor;
          obj.PutStrikeMoneynessIR = PutStrikeMoneynessIR;
          obj.CallStrikeMoneynessIR = CallStrikeMoneynessIR;
          obj.PutStrikeIR = max(PutStrikeMoneynessIR + ForwardSwap, 0);
          obj.CallStrikeIR = max(CallStrikeMoneynessIR + ForwardSwap, 0);
          obj.PutExerciseDatesIR = ones(size(obj.PutStrikeIR)) *  addtodate(StartingDate, Term, 'Month');
          obj.CallExerciseDatesIR = ones(size(obj.CallStrikeIR)) *  addtodate(StartingDate, Term, 'Month');
          
          MaturitiesIR = addtodate(obj.PutExerciseDatesIR(1), SwapTenor, 'year');
          obj.PutMaturitiesIR = ones(size(obj.PutStrikeIR)) * MaturitiesIR;
          obj.CallMaturitiesIR = ones(size(obj.CallStrikeIR)) * MaturitiesIR;

          obj.OptSpecPutIR = cell(size(obj.PutStrikeIR));
          obj.OptSpecPutIR(:) = {'put'};
          obj.OptSpecCallIR = cell(size(obj.CallStrikeIR));
          obj.OptSpecCallIR(:) = {'call'};
          
          obj.Principal = Principal;
      end
      
      function [PutSwaptionPrice, CallSwaptionPrice, PutVol, CallVol] = getSwaptionPrice(obj, ShockDelay, CurrentSwapRates, ImpliedVolsIR, CurrentRateSpec)          
          CurrentDate = addtodate(obj.StartingDate, ShockDelay, 'Month');
          PutSettle = CurrentDate*ones(size(obj.PutStrikeIR));
          CallSettle = CurrentDate*ones(size(obj.CallStrikeIR));
          
          % return NaN if the swaption has already matured
          if CurrentDate > obj.PutExerciseDatesIR(1)
              PutSwaptionPrice = NaN(size(obj.PutStrikeIR));
              CallSwaptionPrice = NaN(size(obj.CallStrikeIR));
          else
              [PutVol, CallVol] = getSigma(obj, ShockDelay, CurrentSwapRates, ImpliedVolsIR);
              PutSwaptionPrice = swaptionbyblk(CurrentRateSpec, obj.OptSpecPutIR, obj.PutStrikeIR, PutSettle, obj.PutExerciseDatesIR,obj.PutMaturitiesIR, PutVol, 'Principal', obj.Principal);
              CallSwaptionPrice = swaptionbyblk(CurrentRateSpec, obj.OptSpecCallIR, obj.CallStrikeIR, CallSettle, obj.CallExerciseDatesIR,obj.CallMaturitiesIR, CallVol, 'Principal', obj.Principal);
              if any(isnan(PutSwaptionPrice)) == 1 || any(isnan(CallSwaptionPrice)) == 1
                  PutSwaptionPrice(isnan(PutSwaptionPrice)) = 0; % WHY IS THIS A PROBLEM?!
              end
          end
      end
   end
   
   methods (Access = private)
       function [PutVol, CallVol] = getSigma(obj, ShockDelay, CurrentSwapRates, ImpliedVolsIR)
          %Recalculates the ITMness based on current conditions.
          [PutITMness, CallITMness] = getITMnessIR(ShockDelay, CurrentSwapRates, obj.Term, obj.SwapTenor, obj.PutStrikeIR, obj.CallStrikeIR);
          
          VolatilityITMIR = ImpliedVolsIR(1,2:size(ImpliedVolsIR,2));
          VolatilityMatureMonthsIR = ImpliedVolsIR(2:size(ImpliedVolsIR,1),1);
          ImpliedVols = ImpliedVolsIR(2:size(ImpliedVolsIR,1),2:size(ImpliedVolsIR,2));
          
          % Recalculates the months to maturity based on how much time has passed and
          % pulls out the row from the volatility tables that that months to maturity
          % corresponds to
          [rowIndices,~]=find(VolatilityMatureMonthsIR>=(obj.Term-ShockDelay));
          MTMrow = rowIndices(1);
          
          % why does it do a step function for swaptions instead of
          % interpolating? what should I do about vols outside this range?
%           PutVol = interp1(VolatilityITMIR, ImpliedVols(MTMrow,:),PutITMness, 'linear', ImpliedVols(MTMrow,size(ImpliedVols,2)));
%           CallVol = interp1(VolatilityITMIR, ImpliedVols(MTMrow,:),CallITMness, 'linear', ImpliedVols(MTMrow,size(ImpliedVols,2)));
          PutVol = interp1(VolatilityITMIR, ImpliedVols(MTMrow,:),PutITMness, 'linear');
          CallVol = interp1(VolatilityITMIR, ImpliedVols(MTMrow,:),CallITMness, 'linear', ImpliedVols(MTMrow,size(ImpliedVols,2)));
      end
   end
end

function [PutITMness, CallITMness] = getITMnessIR(ShockDelay, CurrentSwapRates, Term, swapTenor, PutStrike, CallStrike)
    timeArray = zeros(size(CurrentSwapRates));
    timeArray(ceil((Term - ShockDelay)/12 + 1):ceil((Term - ShockDelay)/12 + 1)+swapTenor-1) = 1;
    if rem((Term - ShockDelay), 12) ~= 0
        timeArray(ceil((Term - ShockDelay)/12)) = ShockDelay/12;
        timeArray(ceil((Term - ShockDelay)/12 + 1)+swapTenor-1) = 1 - ShockDelay/12;
    end
    PutITMness = PutStrike - sum(CurrentSwapRates.*timeArray) / swapTenor;
    CallITMness = CallStrike - sum(CurrentSwapRates.*timeArray) / swapTenor;
    PutITMness = round(PutITMness, 15); % deals with rounding errors
    CallITMness = round(CallITMness, 15); % deals with rounding errors
end

