% -------------------------createBaselineInputs---------------------------%
% Script #:
% Calls: 
% Inputs: baselineScenario and accountValueSets. By default,
% baselineScenario is 1, and accountValueSets is [2,4,6,8,10]. If you will
% be running a different accountValueSet, make sure to specify this.

% Create baseline inputs from specified new business .mat file. Note that 
% the baseline inputs will be named 'IRC Inputs/[NB_INPUT_DATE]_S1_InputSet_[PRODUCT]'
% The NB_INPUT_DATE is the date associated with the new business .mat file,
% which has the naming convention '[DATE]_NewBusiness_....'.
%-------------------------------------------------------------------------%

function[] = createBaselineInputs(varargin)
    %% Load inputs
    p = inputParser;
    
    addParameter(p,'baselineScenario',1)
    addParameter(p, 'accountValueSets', (0:5)*2)
    addParameter(p, 'NB_filename', NaN)
    addParameter(p, 'CSV_input_file', NaN);
    parse(p,varargin{:});
    baselineScenario = p.Results.baselineScenario;
    accountValueSets = p.Results.accountValueSets;
    NB_filename = p.Results.NB_filename;
    CSV_input_file = p.Results.CSV_input_file;
    
    if isnan(NB_filename)
        [FileName,PathName] =  uigetfile({'*.mat',  'Baseline input file (*.mat)'}, 'Select baseline new business input file');
        NB_filename = sprintf('%s%s', PathName, FileName);
    end
    load(NB_filename)
    
    
    start_identifier = strfind(NB_filename, '(');
    end_identifier = strfind(NB_filename, ')');
    fileSuffix = NB_filename(start_identifier:end_identifier);
    
    iInputFileDate = regexp(NB_filename, '[0-9]*_NewBusiness');
    inputFileDate = NB_filename(iInputFileDate:iInputFileDate+7);
    
    InForceInputs = sprintf('IRC Inputs/InForceInputs_S%d',baselineScenario);
    runIRC(InForceInputs, NewBusinessByCell, 'fileSuffixProduct', fileSuffix,...
        'accountValueSets', accountValueSets, 'createBaselineInputs', 1,...
        'currentDate', inputFileDate, 'CSV_input_file', CSV_input_file);
end