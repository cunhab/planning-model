NodestoRun = parameters.NodestoRun;
NodeMonths = parameters.NodeMonths;
RebalanceMonths = parameters.RebalanceMonths;
HedgeToShockDelay = parameters.HedgeToShockDelay;
priceOnIndex = parameters.priceOnIndex;
PriceIndexAdjustment = parameters.PriceIndexAdjustment;
BSRateVols = parameters.BSRateVols;
loadShockedTermStructure = parameters.loadShockedTermStructure;
ChangeATMVols = parameters.ChangeATMVols;
DeductibleSchedule = parameters.DeductibleSchedule;
BaseSetofEquityShocks = parameters.BaseSetofEquityShocks;
BaseSetofRateShocks = parameters.BaseSetofRateShocks;
MCEVHedging = parameters.MCEVHedging;
MCEVLink = parameters.MCEVLink;
UseBaselineExpectation = parameters.UseBaselineExpectation;
UseRescaledShocks = parameters.UseRescaledShocks;
Grading = parameters.Grading;
PinchPayout = parameters.PinchPayout;
OptimizerA = parameters.OptimizerA;
OptimizerB = parameters.OptimizerB;
OptimizerC = parameters.OptimizerC;
OptimizerD = parameters.OptimizerD;
PercentTARReleaseToSell = parameters.PercentTARReleaseToSell;
SellIRUpside = parameters.SellIRUpside;
PercentIRShockTARReleaseToSell = parameters.PercentIRShockTARReleaseToSell;
PositiveTAROnly = parameters.PositiveTAROnly;
RestrictEquityShocks = parameters.RestrictEquityShocks;
FloorBearMarket = parameters.FloorBearMarket;
SPFloor = parameters.SPFloor;
HaircutCashflows = parameters.HaircutCashflows;
AdditionalExpense = parameters.AdditionalExpense;
LimitDTANone = parameters.LimitDTANone;
MakeTaxAdjustment = parameters.MakeTaxAdjustment;
RebalancedNodes = parameters.RebalancedNodes;
AddPAD = parameters.AddPAD;
AddPADRates = parameters.AddPADRates;
VolSensitivity = parameters.VolSensitivity;
PADMin = parameters.PADMin;
PADMax = parameters.PADMax;
VolMultiplier = parameters.VolMultiplier;
SpecifyStartingFunding = parameters.SpecifyStartingFunding;
StartingFunding = parameters.StartingFunding;
KeepCapitalBuffer = parameters.KeepCapitalBuffer;
T0Buffer = parameters.T0Buffer;
CapitalBuffer = parameters.CapitalBuffer;
CapitalRollforward = parameters.CapitalRollforward;
NoDividends = parameters.NoDividends;
noInterestOnCapital = parameters.noInterestOnCapital;
CreateBedrock = parameters.CreateBedrock;
BedrockPercent = parameters.BedrockPercent;
outputStrikes = parameters.outputStrikes;
outputPayoffPerInstrument = parameters.outputPayoffPerInstrument;
outputHedgeNeedDetails = parameters.outputHedgeNeedDetails;
includeCover = parameters.includeCover;
outputAttribution = parameters.outputAttribution;
instantaneousShock = parameters.instantaneousShock;
SetofInstantEquityShocks = parameters.SetofInstantEquityShocks;
SetofInstantRateShocks = parameters.SetofInstantRateShocks;
optimizeQuarterly = parameters.optimizeQuarterly;
optimizeToNewBaseline = parameters.optimizeToNewBaseline;
newIndexGrowth = parameters.newIndexGrowth;
newIndexGrowthAbsolute = parameters.newIndexGrowthAbsolute;
RateShock_New = parameters.RateShock_New;
useRealVols = parameters.useRealVols;
realVolsDelay = parameters.realVolsDelay;
inputTaxRate = parameters.inputTaxRate
if isfield(parameters, 'hasDynamicHedge')
	hasDynamicHedge = parameters.hasDynamicHedge;
else
	hasDynamicHedge = 0;
end

% adjust CTE numbers quoted above CSV to be quoted above time 0 CSV
% set aboveCSV = 1 to make change in CalculateRequiredPayoffAdjustment
% 2 to make change in runHedgeModel, 0 to keep numbers as is (2 is not fully debugged)
HaircutFactor_CARVM = parameters.HaircutFactor_CARVM;
aboveCSV = parameters.aboveCSV;

Setting_AddPAD = [string(''), string(' AddPAD')];
Setting_rollover = [string(' no rollforward'), string(' rollforward')];
Setting_grading = [string(''), string(' CTE90Grading'), string(' Grading'), string(' Grading')];
Setting_instruments = [string(''), string(' SwapsOnly'), string(' SwaptionsOnly'),...
    string(' Swaps&Swaptions'), string(' allInstruments'), string(' noLinearInstruments'),...
    string(' LinearEquitiesAllRate'), string(' AllEquitiesLinearRate'), string(' allLinear')];
Setting_priceIndex = [string(' TotalReturn'), string('')];
Setting_FlattenVols = [string(''), string('FlattenVols')];

%7. Use old version of swap model

testSwapDifference = 1;

if testSwapDifference == 1
    LegRateDiff = [];
    SwapPrice_SpecDiff = [];
    SwapPriceNoCFsDiff = [];
    SwapCFsDiff= [];
end