function [StressedValues, inputFinancials, scenarioSettings] = importscenario( varargin )
    %IMPORTSCENARIO Imports scenario file into matlab variables
        %   imports provided scenario file and turns into matlab variables
        %   for use in the hedge model.
    p = inputParser;
    
    addParameter(p,'fileInput', NaN);
    addParameter(p, 'taxRate', 0.21);
    addParameter(p, 'outputFilename', NaN);
    
    parse(p,varargin{:});
    fileInput = p.Results.fileInput;
    taxRate = p.Results.taxRate;
    outputFilename = p.Results.outputFilename;
    
    if isnan(fileInput)
        [fileName,pathName] =  uigetfile({'*.xlsx;*.xlsm',  'Excel file (*.xlsx, *.xlsm)'; '*.*',...
            'All Files'}, 'Select processed scenario input file');
        fileInput = sprintf('%s%s', pathName, fileName);
    end
    inputCTE = readtable(fileInput, 'Sheet', 'Template_Input_CTE');
    disp('Finished reading inputCTE')
    inputSSR = readtable(fileInput, 'Sheet', 'Template_Input_SSR');
    disp('Finished reading inputSSR')
    inputMCEV = readtable(fileInput, 'Sheet', 'Template_Input_MCEV');
    disp('Finished reading inputMCEV')
    inputFinancials = readtable(fileInput, 'Sheet', 'Template_Input_Financials', 'ReadRowName',true);
    disp('Finished reading inputFinancials')
    inputScen = readtable(fileInput, 'Sheet', 'Template_Input_Scen');
    disp('Finished reading inputScen')
    SPX_T0 = xlsread(fileInput, 'Parameters','T0_SPX'); 
    [~, rateType] = xlsread(fileInput, 'Parameters', 'RateType');
    timestepPerYear = xlsread(fileInput, 'Parameters', 'Timestep');
    [~, TreasuryRateKey] = xlsread(fileInput, 'Parameters', 'TreasuryRateKey');
    TreasuryRateKey = strrep(TreasuryRateKey, ' ', '');
    [~, SwapRateKey] = xlsread(fileInput, 'Parameters', 'SwapRateKey');
    SwapRateKey = strrep(SwapRateKey, ' ', '');
    disp('Finished reading parameters')
    
    if isnan(outputFilename)
        outputFilename = inputdlg('Enter output file name:',...
             'Output File Name', [1 50]);
    end
    
    % Nodes that were provided from Milliman
    NodesFromMilliman = sort(unique(inputCTE.Time));
    EquityShocksMilliman = xlsread(fileInput, 'Parameters', 'EquityShock');
    InterestRateShocksMilliman = xlsread(fileInput, 'Parameters', 'RateShock');
    inputNodesString = strtrim(cellstr(num2str(NodesFromMilliman, 'x%d'))).';
    
    taxAdjustmentTable = outerjoin(inputCTE, inputSSR, 'Type', 'left', 'MergeKeys', true);
    financialsYears = inputFinancials.Properties.VariableNames;
    financialsYears = strrep(financialsYears, 'x','');
    financialsYears = str2double(financialsYears).';
    shockZero = zeros(numel(financialsYears),1);
    taxReserves = table(financialsYears, shockZero, inputFinancials{'Tax Reserves in Excess of CSV',:}.',...
        'VariableName', {'Time', 'Shock', 'TaxReserves'});
    taxAdjustmentTable = outerjoin(taxAdjustmentTable, taxReserves, ...
        'Type', 'left', 'MergeKeys', true, 'Keys', {'Time', 'Shock'});

    timesteps = unique(taxAdjustmentTable.Time);
    for iTimestep = timesteps.'
        iTimestepRows = taxAdjustmentTable.Time == iTimestep;
        taxAdjustmentTable.TaxReserves(iTimestepRows) = ...
            taxAdjustmentTable.SSR(iTimestepRows) / ...
            taxAdjustmentTable.SSR(iTimestepRows & taxAdjustmentTable.Shock == 0) * ...
            taxAdjustmentTable.TaxReserves(iTimestepRows & taxAdjustmentTable.Shock == 0);
    end
    
    taxAdjustmentTable.CTE90TaxAdjusted = -(-taxAdjustmentTable.CTE90PostTax + (taxAdjustmentTable.TaxReserves ...
        .* taxAdjustmentTable.FFactorCTE90 .* taxRate));
    taxAdjustmentTable.CTE95TaxAdjusted = -(-taxAdjustmentTable.CTE95PostTax + (taxAdjustmentTable.TaxReserves ...
        .* taxAdjustmentTable.FFactorCTE95 .* taxRate));
    
    CTE70 = unstack(taxAdjustmentTable(:,{'Time','Shock', 'CTE70PreTax'}), 'CTE70PreTax', 'Time');
    CTE90 = unstack(taxAdjustmentTable(:,{'Time','Shock', 'CTE90TaxAdjusted'}), 'CTE90TaxAdjusted', 'Time');
    CTE95 = unstack(taxAdjustmentTable(:,{'Time','Shock', 'CTE95TaxAdjusted'}), 'CTE95TaxAdjusted', 'Time');
    SSR = unstack(inputSSR(:,{'Time','Shock', 'SSR'}), 'SSR', 'Time');
    MCEV = unstack(inputMCEV(:,{'Time','Shock', 'MCEV'}), 'MCEV', 'Time');
    CTE90 = sortrows(CTE90, {'Shock'});
    CTE95 = sortrows(CTE95, {'Shock'});
    SSR = sortrows(SSR, {'Shock'});
    MCEV = sortrows(MCEV, {'Shock'});
    
    
    StressedValues = struct;
    StressedValues.CTE70 = -CTE70{:, inputNodesString};
    StressedValues.CTELow = -CTE90{:, inputNodesString};
    StressedValues.CTEHigh = -CTE95{:, inputNodesString};
    StressedValues.SSR = -SSR{:, inputNodesString};
    StressedValues.MCEV = -MCEV{:, inputNodesString};
    StressedValues.NodesFromMilliman = NodesFromMilliman;
    StressedValues.EquityShocksMilliman = EquityShocksMilliman;
    StressedValues.InterestRateShocksMilliman = InterestRateShocksMilliman;
    
    scenarioSettings = struct;
    scenarioSettings.inputScen = inputScen;
    scenarioSettings.rateType = rateType;
    scenarioSettings.timestepPerYear = timestepPerYear;
    scenarioSettings.SPX_T0 = SPX_T0;
    scenarioSettings.TreasuryRateKey = TreasuryRateKey;
    scenarioSettings.SwapRateKey = SwapRateKey;
    currentScenario = strrep(unique(inputCTE.Scenario),'S','');
    scenarioSettings.scenario = str2num(currentScenario{:});
    if iscell(outputFilename)
        outputname = outputFilename {:};
    else
        outputname = outputFilename;
    end
    save(outputname, 'StressedValues', 'inputFinancials', 'scenarioSettings', 'taxAdjustmentTable');
end