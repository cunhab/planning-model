% -----------------------------------Swaps--------------------------------%
% Script #:
% Calls:
% Variables:
%
% This class inclues all the methods for pricing swaps
%-------------------------------------------------------------------------%

classdef Swaps
    properties (SetAccess = protected) % should it be immutable?
        SettleDate
        StartDate
        Spread
        LegReset
        LegType
        SwapPrincipal
        Maturities
        LegRate
        Tenors
    end
    
    methods
        function obj = Swaps(SettleDate, StartDate, Tenors, StartingSwapRateSpec, varargin)
            p = inputParser;
            defaultLegReset = [4, 4];
            defaultLegType = [1 0];
            defaultSpread = zeros(size(Tenors));
            defaultPrincipal = [1];
            
            addParameter(p,'LegReset',defaultLegReset);
            addParameter(p,'LegType',defaultLegType);
            addParameter(p, 'Spread', defaultSpread);
            addParameter(p, 'SwapPrincipal', defaultPrincipal);
            addParameter(p,'ForwardSwap',0); % change ForwardSwap to number of years to delay swap start
            
            parse(p,varargin{:});
            obj.LegReset = p.Results.LegReset;
            obj.LegType = p.Results.LegType;
            obj.Spread = p.Results.Spread;
            obj.SwapPrincipal = p.Results.SwapPrincipal;
            ForwardSwap = p.Results.ForwardSwap;
            
            obj.SettleDate = SettleDate;
            obj.StartDate = StartDate;
            obj.Tenors = Tenors;
            
            obj.Maturities = zeros(numel(Tenors),1);
            for i = 1:numel(Tenors)
                obj.Maturities(i) = addtodate(StartDate(i), Tenors(i), 'year');
            end
            
            obj.LegRate = nan(numel(Tenors),2);
            obj.LegRate(:,2) = obj.Spread;
            FixedLegRate = getFixedLegRate(obj, SettleDate, StartDate, StartingSwapRateSpec);
            obj.LegRate(:,1) = FixedLegRate;
            
        end
        
        function Price = getSwapPrice(obj, CurrentRateSpec, CurrentTimeStep)
            SettleSwap = addtodate(obj.SettleDate, CurrentTimeStep, 'Month');
            
            % Determine swap price at the given time -- this does not account for cash
            % flows that have already been disbursed.
            [Price] = swapbyzero(CurrentRateSpec, obj.LegRate, SettleSwap, obj.Maturities, 'LegReset', obj.LegReset, 'Principal', obj.SwapPrincipal,...
                'LegType', obj.LegType, 'StartDate', obj.StartDate);
        end
        
        
        % calculates the cash flows since LastRepriced (last CurrentTimeStep)
        function SwapCashFlows = getSwapCashFlows(obj, CurrentTimeStep, LastTimestep, Node, OuterLoopRealSwaps)
            % Calculates swap cash flows
            numSwapCFs = floor((CurrentTimeStep - LastTimestep) / 3);
            
            SwapCashFlows = zeros(size(obj.Tenors));
            
            for SwapQuarter = 1:numSwapCFs
                for i = 1:numel(obj.Tenors)
                    if CurrentTimeStep/12 <= obj.Tenors(i)
                        SwapCashFlows(i) = SwapCashFlows(i) + (obj.SwapPrincipal * (obj.LegRate(i,1)/4 - OuterLoopRealSwaps((Node-1)*4+1+SwapQuarter-1, 1)/4));
                    else
                        SwapCashFlows(i) = NaN;
                    end
                end
            end
        end
    end
    
    methods (Access = private)
        
        % start of the swap
        
        % Calculates the fixed leg rate that would price swap at 0 at time
        % of purchase
        function[LegRate] = getFixedLegRate(obj, SettleSwap, StartDate, SwapRateSpec)            
            [Price, LegRate] = swapbyzero(SwapRateSpec, obj.LegRate, SettleSwap, obj.Maturities, 'LegReset', obj.LegReset, 'Principal', obj.SwapPrincipal,...
                'LegType', obj.LegType, 'StartDate', StartDate);
        end
    end
end

