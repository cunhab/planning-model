%--------------------------------ProcessNBInputs-------------------------%
% This function processes the a single input from a given product (containing
% the GPVADs and financials).  
%
% If the processing of a file failed, but you already loaded in the inputs
% from excel into matlab, they are saved in the IRC Inputs/temp file, and
% they can be processed by running the following code:
% ProcessNBInputs('NB_input_file', 'IRC Inputs/temp', 'PreloadedInputs', 1); 
%-------------------------------------------------------------------------%

function[OutputFilename, NewBusinessByCell, scenarioSet] = ProcessNBInputs(varargin)
    p = inputParser;
    scenarioCols = strtrim(cellstr(num2str((1:1000)', 'x%d'))).';
    
    addParameter(p,'NB_input_file',0)
    addParameter(p,'scenarioSet', 0);
    addParameter(p,'StressedTemplate', 0);
    addParameter(p,'fileSuffix', NaN);
    addParameter(p,'PreloadedInputs', 0);
    addParameter(p,'startT0', 1); % financials inputs start with a T0 column (note: set to 0 if using VASB outputs)

    parse(p,varargin{:});
    input_file = p.Results.NB_input_file;
    scenarioSet = p.Results.scenarioSet; % if no scenarioSet is fed in, it creates an output for each scenario contained in the file
    StressedTemplate = p.Results.StressedTemplate;
    fileSuffix = p.Results.fileSuffix;
    PreloadedInputs = p.Results.PreloadedInputs;
    startT0 = p.Results.startT0;

    % if parameter is not passed, then read in / load parameter
    if PreloadedInputs == 1
        load(input_file)
    else
        if input_file == 0
            [FileName,PathName] =  uigetfile({'*.xlsx;*.xlsm',  'Excel input file (*.xlsx, *.xlsm)'; '*.*',...
                'All Files'}, 'Select input file');
            input_file = sprintf('%s%s', PathName, FileName);
        end
        disp('Loading Aggregated Financials');
        tic
        AggregatedFinancialsAll = readtable(input_file, 'Sheet', 'AggregatedRollforwardFinancials');
        toc
        disp('Finished loading Aggregated Financials');
        
        disp('Loading GPVAD file');
        tic
        NewBusinessGPVAD_tableAll = readtable(input_file, 'Sheet', 'New Business');
        toc
        disp('Finished loading GPVAD file');
        
        save('IRC Inputs/temp')
    end
    
    if scenarioSet == 0
        scenarioSet = unique(NewBusinessGPVAD_tableAll.Scenario).';
    end
    
    if ~istable(StressedTemplate)
        load('IRC Inputs/DefaultInputs/StressedTemplate');
    end
    
    for currentScenario = scenarioSet
        NewBusinessGPVAD_table = NewBusinessGPVAD_tableAll(NewBusinessGPVAD_tableAll.Scenario == currentScenario,:);
        AggregatedFinancials = AggregatedFinancialsAll(AggregatedFinancialsAll.Scenario == currentScenario,:);
        assert(~isempty(AggregatedFinancials), 'No Scenario %d entries found in AggregatedFinancials. Check your inputs contain the scenario.', scenarioSet)
        
        % inputsReal.StressedValues table into structure array
        uniqueCells = unique(NewBusinessGPVAD_table.PolicyID);
        GPVADsByCell = struct;
        disp('Processing Cells');
        for i_cell = 1:numel(uniqueCells)
            currentCellGPVADs = NewBusinessGPVAD_table(NewBusinessGPVAD_table.PolicyID == uniqueCells(i_cell),:);
            GPVADsByCell(i_cell).PolicyID = uniqueCells(i_cell);
            
            currentCellPolicy = unique(currentCellGPVADs.Policy);
            if numel(currentCellPolicy) ~= 1
                error('Cell should only correspond to one product')
            end
            GPVADsByCell(i_cell).PolicyName = currentCellPolicy;
            
            currentCellWeight = unique(currentCellGPVADs.Weight);
            if numel(currentCellWeight) ~= 1
                error('Cell should only have one weight')
            end
            GPVADsByCell(i_cell).Weight = currentCellWeight;
            
            [currentCellGPVADs_ordered, ia] = outerjoin(StressedTemplate, currentCellGPVADs,...
                'Type', 'left', 'MergeKeys', true, 'RightVariables', scenarioCols);
            [~, sortinds] = sort(ia);
            currentCellGPVADs_ordered = currentCellGPVADs_ordered(sortinds,:);
            GPVADsByCell(i_cell).GPVADsTable = currentCellGPVADs_ordered;
            
            currentCellGPVADs_matrix = table2array(currentCellGPVADs_ordered(:,scenarioCols));
            GPVADsByCell(i_cell).GPVADs = currentCellGPVADs_matrix;
        end
        
        GPVADsByCell_table = struct2table(GPVADsByCell);
        
        % inputsReal.StressedValues financials
        disp('Processing NB Financials')
        NewBusinessFinancials_allCells = ProcessNBFinancials(AggregatedFinancials, 'IsFilename', 0, 'ignoreFAMargin', 1, 'currentScenario', currentScenario, 'startT0', startT0);
        
        NewBusinessFinancials_allCells_table = struct2table(NewBusinessFinancials_allCells);
        % join with GPVADsByCell
        NewBusinessByCell = outerjoin(GPVADsByCell_table, NewBusinessFinancials_allCells_table,...
            'Type', 'left','Keys', 'PolicyID', 'MergeKeys', true);
        
        
        formatOut = 'yyyymmdd';
        currentDate = datestr(now,formatOut);
        productfilename = unique(NewBusinessGPVAD_table.Policy);
        OutputFilename = sprintf('IRC Inputs/%s_NewBusiness_S%d_%s', ...
            currentDate, currentScenario, productfilename{:});
        if ~isnan(fileSuffix)
            OutputFilename = strjoin({OutputFilename, fileSuffix});
        end
        save(OutputFilename, 'NewBusinessByCell', 'NewBusinessGPVAD_table', 'AggregatedFinancials');
    end
end

