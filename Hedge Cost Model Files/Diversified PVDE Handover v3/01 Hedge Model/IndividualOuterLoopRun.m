% -----------------------IndividualOuterLoopRun --------------------------%
% Script #: 2.4
% Calls: CalculateRequiredPayoffAdjustment (2.4.1), MasterMacroHedge
% (2.4.2), DetermineDividends (2.4.3) CreateResultsMacroHedge (2.4.4)
% Variables: MasterFunding represents the level of VA capitalization held
% by BHF, SumPayoutReal represents the hedge asset gain / loss in the scenario
% for the node, MasterCTESet represents the set of target funding levels 
% across all shocks for the node
%
% This script is the overarching file to calculate hedge cost for all
% nodes. It
%   - Determines starting funding
%   - Estimates target hedge gain
%   - Creates a set of hedge instruments and prices them in different shock
%   environments
%   - Optimizes on payoffs from the hedge of instruments to determine an
%   optimal portfolio to meet funding requirements
%   - Determines dividends
%   - Exports results to an excel

%-------------------------------------------------------------------------%

% MasterFunding sets the funding equal to target funding from capital
% plus additional funding from other sources (capital buffer).
MasterFunding = CTEValueBase(1)/1000000000 + CapitalBuffer;

% If user parameter specified, starts funding at user-specified level
if SpecifyStartingFunding == 1
    MasterFunding = StartingFunding;
end

% Declares end of period funding
EoPFunding = MasterFunding;
GradingFactorBaseCatch = zeros(1,20);
EoPFundingAllNodes = zeros(1, numel(NodestoRun));

% Post-tax hedge cost variable, initially set to 0
AdjSumPayout = 0;
SumPayoutReal = 0;

% Dividends variable, initially set to 0
Dividends = zeros(1,20);    

% Vol sensitivity catches
if AddPAD == 1
    VolSensitivityCatch = zeros(1,20);
    SumPayoutRealOriginalCatch = zeros(1,20);
    VolPremiumCostsCatch = zeros(1,20);
    OptionsCostsVolCatch = zeros(113,20);
end 

if AddPADRates == 1
    VolPremiumCostsCatchRates = zeros(1,20);
    OptionsCostsVolCatchRates = zeros(113,20);
end 

if KeepCapitalBuffer == 1
    DeductibleCatch = zeros(1,20);
end; 

% reset exitflags
exitflags = [];

% Runs the model for as many nodes specified in the Dashboard.
for j = 1:numel(NodestoRun)
    
    % Sets node
    Node=NodestoRun(j);
    
    % Sets rebalancing for the node
    if RebalancedNodes(Node) == 1
        RebalanceQuarterly = 1; 
    else 
        RebalanceQuarterly = 0;
    end
    
    disp(strcat('Node #: ', num2str(Node)));
    
    % Sets capital roll forward preference
    if CapitalRollforward == 1
        

        % If there are no dividends in the previous period, beginning of
        % period funding in current period is equal to previous period's
        % funding plus net cash flows from non-hedge assets plus hedge gain
        % /loss. 
        MasterFunding = EoPFunding;
    else 
        % Otherwise, Brighthouse is always funded to target funding + the max 
        % capital buffer
        MasterFunding = CTEValueBase(Node)/1000000000 + CapitalBuffer;
    end

    % Following code is commented out--if Dashboard has a user parameter
    % called "AdjustGrading" set to 1 and code is uncommneted, 
    % recalculates grading factors and target funding levels if starting 
    % funding is less than CTE 95.
    % Subsequently, shocks at or better than baseline expectations are
    % graded to be equivalent to the level that baseline funding would have 
    % to be graded to to equal starting funding, and moderate shocks
    % grade down to the most severe shocks where target 
    % funding is determined by the midpoint between CTE 90 and CTE 95.
    % This code has NOT been tested in its final form. 
    %
    %     if AdjustGrading == 1
    %         if Node == 1
    %             if UseBaselineExpectations == 0
    %                 GradingFactorBase = (MasterFunding - CTEValue90Base(Node))/(CTEValueBase(Node)/1000000000-CTEValue90Base(Node));
    %             end 
    %         else
    %             GradingFactorBase = (MasterFunding - CombinedCTEBaselineShock90(Node)/1000000000)/(CombinedCTEBaselineShock95(Node)/1000000000-CombinedCTEBaselineShock90(Node)/1000000000);
    %         end 
    %         if GradingFactorBase > 1
    %             GradingFactorBase = 1;
    %         end 
    %         GradingFactorBaseCatch(Node) = GradingFactorBase;
    %         GradingFactors = BaseSetofEquityShocks - (1-GradingFactorBase);
    %         for i = 1:numel(GradingFactors)
    %             if GradingFactors(i) < 0.5
    %                 GradingFactors(i) = 0.5;
    %             end 
    %             if GradingFactors(i) > GradingFactorBase
    %                 GradingFactors(i) = GradingFactorBase;
    %             end 
    %         end
    %         
    %         % Reinterpolate the target funding grid for the node
    %         ShockedTargetFunding = zeros(1,numel(BaseSetofEquityShocks));
    %         for i = 1: numel(GradingFactors)
    %             ShockedTargetFunding(i) = CTE90BaselineFullSet(Node+1,i) + GradingFactors(i)*(CTE95BaselineFullSet(Node+1, i) - CTE90BaselineFullSet(Node+1, i));
    %         end 
    %         CombinedCTESetAllNodes(Node+1,:) = ShockedTargetFunding;
    %         
    %         if MCEVHedging == 1
    %             for k = 1:numel(BaseSetofEquityShocks)
    %                 if BaseSetofEquityShocks(k) < 1
    %                     CombinedCTESetAllNodes(Node+1,k) = max(CombinedCTESetAllNodes(Node+1,k),CombinedMCEVSetAllNodes(Node+1,k)/1000000000);
    %                 end 
    %             end 
    %         end 
    %         
    %         CTEValueBase(Node+1) = CTEValue90Base(Node+1)*1000000000 + GradingFactorBase*(CTEValue95Base(Node+1) - CTEValue90Base(Node+1)*1000000000);
    %         CombinedCTEBaselineShock(Node+1) = CombinedCTEBaselineShock90(Node+1) + GradingFactorBase*(CombinedCTEBaselineShock95(Node+1)-CombinedCTEBaselineShock90(Node+1));
    %         CTEValueBaseFollow(Node+1) = CombinedCTEBaselineShock(Node+1);
    %     end 
    
    % Assigns target funding grid for the node
    MasterCTESet=CombinedCTESetAllNodes(Node+1,:);
        
    % Sets the income statement to the baseline. Used to adjust the target funding in
    % CalculateRequiredPayoffAdjustment
    if UseBaselineExpectation == 1 
        if Node == 1
            MillimanInputs(:,2) = MillimanInputsBaseline(:,2);
            SetIncomeStatement;  
            UseRescaledShocks = 0;
        else
            MillimanInputs = MillimanInputsReal;
            SetIncomeStatement;
            UseRescaledShocks = 1;
        end 
    end 
   
    % Determines the required hedge payoff for all shocks relevant to the
    % risk appetite framework
    CalculateRequiredPayoffAdjustment;
    
    % Defines and prices hedge instruments, and conducts optimization of
    % optimal hedge portfolio
    MasterMacroHedgeOL;
    
    % Resets CTE values to actual capital markets scenario
    MillimanInputs = MillimanInputsReal;
    SetIncomeStatement;
    
    % Adjusts hedge costs for volatility sensitivity
    if AddPAD == 1
        PremiumCosts = sum(PurchaseAmounts'.*OptionsCosts)*1000;
        VolPremiumCosts = sum(PurchaseAmounts'.*OptionsCostsVolCatch(:,Node))*1000;
        EquityVolPremiumPurchaseAmounts = [PurchaseAmounts(1:36), PurchaseAmounts(72:101)];
        EquityVolOptionsCosts = [OptionsCostsVolCatch(1:36,Node); OptionsCostsVolCatch(72:101,Node)];
        VolPremiumCosts_EquitiesOnly = sum(EquityVolPremiumPurchaseAmounts'.*EquityVolOptionsCosts) * 1000;
        VolPremiumCostsCatch(Node) = VolPremiumCosts;
        
        % Volatility sensitivity determined by the premium outlay of all
        % hedge instruments purchased with a +2% volatility increase over
        % the original premium outlay
        VolSensitivityValue = VolPremiumCosts/PremiumCosts - 1;
        SumPayoutRealOriginal = SumPayoutReal;
        VolSensitivityCatch(Node) = VolSensitivityValue;
        SumPayoutRealOriginalCatch(Node) = SumPayoutRealOriginal;
        
        % Hedge costs are multiplied by the volatility sensitivity times
        % the volatility multiplier and capped at PADMax and floored at
        % PADMin
        SumPayoutRealVol = SumPayoutRealOriginal*(1+VolSensitivityValue*VolMultiplier);
        if SumPayoutRealVol < SumPayoutRealOriginal*PADMax
            SumPayoutRealVolAdj = SumPayoutRealOriginal*PADMax;
        elseif SumPayoutRealVol > SumPayoutRealOriginal*PADMin
            SumPayoutRealVolAdj = SumPayoutRealOriginal*PADMin;
        else
            SumPayoutRealVolAdj = SumPayoutRealVol;
        end
        SumPayoutReal = SumPayoutRealVolAdj;
        
        % If there are hedge gains, hedge gains are equal to initial hedge
        % gains
        if SumPayoutRealOriginal > 0
            SumPayoutReal = SumPayoutRealOriginal;
        end
    end
    
    if AddPADRates == 1
        RateVolPremiumPurchaseAmounts = [PurchaseAmounts(37:69), PurchaseAmounts(102:113)];
        RateVolOptionsCosts = [OptionsCostsVolCatchRates(37:69,Node); OptionsCostsVolCatchRates(102:113,Node)];
        VolPremiumCosts_RatesOnly = sum(RateVolPremiumPurchaseAmounts'.*RateVolOptionsCosts) * 1000;
        VolPremiumCostsRates = sum(PurchaseAmounts'.*OptionsCostsVolCatchRates(:,Node))*1000;
        VolPremiumCostsCatchRates(Node) = VolPremiumCostsRates;
    end
    
    % Conducts capital roll forward (CRF)
    PreTaxCashFlowWithHedge = PreTaxCashFlow(Node+1) + InterestOnExcessCapital+SumPayoutReal;
    TaxReserveChangeCRF = TaxReserve(Node+1) - TaxReserve(Node);
    TaxableIncomeCRF = PreTaxCashFlowWithHedge - (TaxReserveChangeCRF + DRD(Node+1) + CARVMExpense(Node+1));
    TaxesPaidCRF = max(TaxableIncomeCRF*inputTaxRate,0);
    NetCashFlowCRF = PreTaxCashFlowWithHedge - TaxesPaidCRF;
    EoPFunding = MasterFunding + NetCashFlowCRF;
    if aboveCSV == 1
        % express funding above end of year CSV
        EoPFunding = EoPFunding - CARVMExpense(Node+1);
    end
    
    % Determines the dividends distributed
    DetermineDividends;
    
    % Estimates post-tax hedge cost with 35% tax rate
    AdjSumPayout = SumPayoutReal * (1-inputTaxRate);
    
    EoPFundingAllNodes(j) = EoPFunding; 
    % Outputs values into an Excel spreadsheet
    CreateResultsMacroHedge
end