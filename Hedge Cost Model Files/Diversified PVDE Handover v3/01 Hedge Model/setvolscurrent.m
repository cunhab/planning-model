% ----------------------------setvolscurrent----------------------------- %
% Script #: 3.2
% Calls: N/A
% Variables: ImpliedVolsCurrent represents the implied volatility surface
% used by the model to price hedge instruments
%
% This script selects the implied vol surfaces used to price hedge
% instruments at the given time and given equity conditions
% ----------------------------------------------------------------------- %

%Determines which equity volatility surface is most appropriate.  Dependent
%on the degree of the equity shock in any given pricing scenario. Assigns
%the proper equity volatility surface prior to pricing.

function[ImpliedVolsCurrent] = setvolscurrent(ImpliedVols, shockToBaseline)
    % Selects implied volatility surface based on the change in equities from
    % the previous year--rounds up (e.g. any drop greater than -50% relative to
    % baseline uses the -50% implied vol surface)
    
    closestShockSurface = min([ImpliedVols([ImpliedVols.Shock] >= shockToBaseline).Shock]);
    if isempty(closestShockSurface)
        closestShockSurface = max(ImpmliedVols.Shock);
    end
    % keep vol surface = to 0% shock surface for any positive shocks
    closestShockSurface = min(closestShockSurface, 1);
    ImpliedVolsCurrent = ImpliedVols(arrayfun(@(x) all(x.Shock == closestShockSurface), ImpliedVols));
    ImpliedVolsCurrent = ImpliedVolsCurrent.Surface;
end