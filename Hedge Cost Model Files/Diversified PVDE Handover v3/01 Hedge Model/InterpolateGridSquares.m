% ------------------------ InterpolateGridSquares -------------------------%
% Script #: 2.2.2
% Calls: GradeCTEGrid (2.2.2.2) 
% Variables: 
%
% This script 
%   1. Calls upon RunRegression to regress additional values for all shocks
%   relevant to the BHF risk management framework
%   2. Grades the regressed values to reach the target funding level
%   in a given shock
%
% ------------------------------------------------------------------------%

function [TARGrids, BaselineTARs, Coefficients] = ...
    InterpolateGridSquares(i, UseRescaledShocks, StressedValues, SPFloor, MillimanInputsStruc, parameters)
    
    % if variables are not provided as inputs, use the values from the main
    % script
    if (~exist('SPFloor', 'var') || isempty(SPFloor)) SPFloor = evalin('caller','SPFloor'); end
    if (~exist('UseBaselineExpectation', 'var') || isempty(UseBaselineExpectation)) UseBaselineExpectation = evalin('caller','UseBaselineExpectation'); end
    
    EquityShocksMilliman = StressedValues.EquityShocksMilliman;
    IRShocksMilliman = StressedValues.InterestRateShocksMilliman;
    NodesFromMilliman = StressedValues.NodesFromMilliman;
    BaseSetofEquityShocks = parameters.BaseSetofEquityShocks;
    BaseSetofRateShocks = parameters.BaseSetofRateShocks;
    UseBaselineExpectation = parameters.UseBaselineExpectation;
    
    Node = NodesFromMilliman(i);
    
    OptimizerC = evalin('caller', 'OptimizerC');
    OuterLoopReal = evalin('caller', 'OuterLoopReal');
    OuterLoopBaseline = evalin('caller', 'OuterLoopBaseline');
    
    
    % S&P level in the capital market scenario and a baseline scenario
    OuterLoopRealSP = OuterLoopReal(Node+2, 2);
    OuterLoopBaselineSP = OuterLoopReal(Node+1,2)*(OuterLoopBaseline(Node+2,2)/OuterLoopBaseline(Node+1,2));
    
    if UseRescaledShocks == 0 || ((UseBaselineExpectation == 1) && (Node == 1))
        EquityShocks = EquityShocksMilliman;
        InterestRateShocks = IRShocksMilliman;
    else       
        EquityShocks = ((OuterLoopRealSP*(1+EquityShocksMilliman))/OuterLoopBaselineSP)-1;
        
        % 10YT in the capital market scenario and baseline scenario
        OuterLoopReal10YT = OuterLoopReal(Node+1, 9);
        Change10YTBaseline = OuterLoopBaseline(Node+2, 9) - OuterLoopBaseline(Node+1, 9);
        Converted10YT = OuterLoopReal10YT+Change10YTBaseline;
        
        InterestRateShocks = OuterLoopReal(Node+2, 9)-Converted10YT+IRShocksMilliman;
    end
    
    % Takes the inputs generated from DetermineCTEValues and regresses them for
    % a full set of shocked CTEs for CTE 90 of baseline behavior
    [CTELowBaselineGrid, CoefficientsCTE90, CTEBaselineShock90] = RunRegression(UseRescaledShocks, StressedValues.CTELow(:,i));
    if UseBaselineExpectation == 1 && Node == 1
        CTEBaselineShock95 = MillimanInputsStruc.MillimanInputsBaseline(3,Node + 1);
    elseif UseRescaledShocks == 0
        CTEBaselineShock95 = MillimanInputsStruc.MillimanInputsReal(3,Node + 1);
    end
    
    % Full set of shocked CTEs for CTE 95
    if OptimizerC == 1
        [CTEHighBaselineGrid95, CoefficientsCTE95, CTEBaselineShock95, CTENoShock] = RunRegression(UseRescaledShocks, StressedValues.CTEHigh(:,i));
    else
        [CTEHighBaselineGrid95, CoefficientsCTE95, CTEBaselineShock95] = RunRegression(UseRescaledShocks, StressedValues.CTEHigh(:,i));
    end
    if UseBaselineExpectation == 1 && Node == 1
        CTEBaselineShock95 = MillimanInputsStruc.MillimanInputsBaseline(2,Node + 1); % this is only the CTE 95 scenario
    elseif UseRescaledShocks == 0
        CTEBaselineShock95 = MillimanInputsStruc.MillimanInputsReal(2,Node + 1); % this is only the CTE95 scenario
    end
    
    % Full Set of shocked SSRs for SSR
    [SSRGrid, CoefficientsSSR] = RunRegression(UseRescaledShocks, StressedValues.SSR(:,i));
    
    
    [MCEVGrid, CoefficientsMCEV, MCEV_BaselineShock] = RunRegression(UseRescaledShocks, StressedValues.MCEV(:,i));
    
    if UseRescaledShocks == 0
        MCEV_BaselineShock = StressedValues.MCEV(1, i);
    end
    
    %%
    % Grades the CTE 90 and 95 grids to reach target funding levels, graded to
    % CTE 95
    GradedBaselineGrid95 = GradeCTEGrid(parameters, Node, CTEHighBaselineGrid95, CTELowBaselineGrid, BaseSetofEquityShocks);
    
    CombinedCTEGrid95 = GradedBaselineGrid95/1000000000;
    CombinedSSRGrid = SSRGrid/1000000000;
    
    %%
    % Grades the MCEV grid
    
    % MCEV) OR max(CTE95, MCEV) - 1Bn
    GradedMCEVGrid = zeros(1,numel(MCEVGrid));
    for i = 1:numel(GradedMCEVGrid) % THIS IS THE PROBLEM (BASELINESHOCK)
        GradedMCEVGrid(i) = CTEBaselineShock95 + parameters.MCEVLink*(MCEVGrid(i)-MCEV_BaselineShock); % these do not regress the baseline--need to figure this out for non-S1 scenarios
    end
    
    Coefficients = struct;
    Coefficients.CoefficientsCTE90 = CoefficientsCTE90;
    Coefficients.CoefficientsCTE95 = CoefficientsCTE95;
    Coefficients.CoefficientsSSR = CoefficientsSSR;
    Coefficients.CoefficientsMCEV = CoefficientsMCEV;
    
    TARGrids = struct;
    TARGrids.CombinedCTEGrid95 = CombinedCTEGrid95;
    TARGrids.CombinedSSRGrid = CombinedSSRGrid;
    TARGrids.GradedMCEVGrid = GradedMCEVGrid;
    TARGrids.CTELowBaselineGrid = CTELowBaselineGrid;
    TARGrids.CTEHighBaselineGrid95 = CTEHighBaselineGrid95;
    
    BaselineTARs = struct;
    BaselineTARs.CTEBaselineShock90 = CTEBaselineShock90;
    BaselineTARs.CTEBaselineShock95 = CTEBaselineShock95;
    if OptimizerC == 1
        BaselineTARs.CTENoShock = CTENoShock;
    end
    
    %%
    function [RegressedCTEs, Coefficients, CTEBaselineShock, CTENoShock, StatEstimates] = RunRegression(UseRescaledShocks, CTE_values)
        % Other terms in the regression
        EquitySquared = EquityShocks.^2;
        InterestSquared = InterestRateShocks.^2;
        EquityxInterest = EquityShocks.*InterestRateShocks;
        
        % See Documentation 6.2.1 and 4.3.1.2.1 for the regression formula
        % Creates regression structure
        % StatisticsTable = [EquityShocks,InterestRateShocks,EquitySquared,InterestSquared,EquityxInterest, CTE_values];
        StatisticsTable = table;
        StatisticsTable.EquityShocks = EquityShocks;
        StatisticsTable.InterestRateShocks = InterestRateShocks;
        StatisticsTable.EquitySquared = EquitySquared;
        StatisticsTable.InterestSquared = InterestSquared;
        StatisticsTable.EquityxInterest = EquityxInterest;
        
        StatisticsTable.Results = CTE_values;
        
        % Runs the regression
        StatisticsResults = fitlm(StatisticsTable, 'ResponseVar', 'Results');
        
        % Pulls the coefficients (Greeks) out of the regression structure
        Coefficients = StatisticsResults.Coefficients;
        StatEstimates = Coefficients{:,1};
        
        % Interpolates additional shocks using coefficients from the regression
        RegressionShocks = [BaseSetofEquityShocks-1; BaseSetofRateShocks];
        BaseSetofEquityShocksPercent = BaseSetofEquityShocks-1;
        RegressionShocksTable = table;
        RegressionShocksTable.EquityShocks = BaseSetofEquityShocksPercent.';
        RegressionShocksTable.InterestRateShocks = BaseSetofRateShocks.';
        
        % Floors protection at S&P 800
        for j = 1:numel(RegressionShocks)
            if (1+RegressionShocks(j))*OuterLoopBaselineSP < SPFloor
                RegressionShocks(j) = SPFloor/OuterLoopBaselineSP-1;
            end
        end
        
        % Interpolates values associated with shocks not provided from actuarial
        % models
        RegressedCTEs = StatEstimates(1)+...
            StatEstimates(2)*RegressionShocksTable.EquityShocks+...
            StatEstimates(3)*RegressionShocksTable.InterestRateShocks+...
            StatEstimates(4)*RegressionShocksTable.EquityShocks.^2+...
            StatEstimates(5)*RegressionShocksTable.InterestRateShocks.^2+...
            StatEstimates(6)*(RegressionShocksTable.EquityShocks .* RegressionShocksTable.InterestRateShocks);
        RegressedCTEs = RegressedCTEs.';
        
        % Uses non-regressed values (i.e. values from imports) when relevant, i.e.
        % for node 1 following baseline expectations or a baseline scenario

        
        if UseRescaledShocks == 0 && UseBaselineExpectation == 0 % is this a proxy for being baseline scenario? 
            [~,ia,ib] = intersect(RegressionShocksTable, StatisticsTable(:,{'EquityShocks', 'InterestRateShocks'}));
            RegressedCTEs(ia) = CTE_values(ib);
        elseif UseBaselineExpectation == 1 && Node == 1
            [~,ia,ib] = intersect(RegressionShocksTable, StatisticsTable(:,{'EquityShocks', 'InterestRateShocks'}));
            RegressedCTEs(ia) = CTE_values(ib);
        end
        
        % Creates the CTE value in a baseline shock. In a baseline scenario, the
        % value is the value imported as CTE 95 in the outer loop
        CTEBaselineShock = StatEstimates(1);
        
        % Creates the no growth value used for optimizer C
        if OptimizerC == 1
            CTENoShock = StatEstimates(1) + StatEstimates(2)*(1/(OuterLoopBaseline(Node+2,2)/OuterLoopBaseline(Node+1,2))- 1) + StatEstimates(4)*((1/(OuterLoopBaseline(Node+2,2)/OuterLoopBaseline(Node+1,2)) - 1)^2);
        end
    end
end
