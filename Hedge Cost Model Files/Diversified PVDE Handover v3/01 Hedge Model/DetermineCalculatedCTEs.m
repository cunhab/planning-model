% ------------------------DetermineCalculatedCTEs-------------------------%
% Script #: 2.2
% Calls: DetermineCTEValues (2.2.1) InterpolateGridSquares (2.2.2), 
% InterpolateAdditionalNodes (2.2.3)
% Variables: CTE90BaselineFullSetTemp, CTE95BaselineFullSetTemp carried
% into InterpolateAdditionalNodes. SSRBaselineGrid,
% CombinedCTESetAllNodes, CombinedSSRAllNodes, CombinedCTEBaselineShock and
% CombinedMCEVAllNodes are carried into CalculateUpside,
% IndividualOuterLoopRun and CalculateRequiredPayoffAdjustment

% This script creates the target funding levels by interpolating different equity and interest
% rate shocks from values generated by actuarial models, then linearly
% interpolates values in missing nodes. 

% The process is done for both CTEs/MCEVs in the real outer loop and a "baseline"
% scenario for Node 1.

%% 

% ------------------------------ 1. Set up ------------------------------ %
% The interpolated CTEs, post-graded, for all shocks and all nodes, grading to CTE 95
CombinedCTESetAllNodes95 = zeros(maxNodetoRun + 1, numel(BaseSetofEquityShocks));

% The interpolated values for all shocks and nodes for AG43 SSR 
CombinedSSRAllNodes= zeros(maxNodetoRun + 1, numel(BaseSetofEquityShocks));

% The baseline SSR across all nodes (CTE 95 and 97 are done in Excel
% through the BHF statutory financials import)
SSRBaselineGrid = zeros(maxNodetoRun + 1, 1);

% The interpolated MCEV values for all shocks and all nodes
CombinedMCEVSetAllNodes = zeros(maxNodetoRun + 1, numel(BaseSetofEquityShocks));

% The interpolated baseline CTE 95 in a baseline scenario  , for
% all nodes 
CombinedCTEBaselineShock = zeros(1,maxNodetoRun + 1);

% The calculated CTEs if there is no growth in equities
CombinedCTENoShock = zeros(1,maxNodetoRun + 1);
% RegressedCTE70BaselineShock = zeros(1,21);

% Catches the delta, gamma, rho, rho convexity, cross-Greek and constant in
% each of the calculated CTE levels
% CoefficientCatch90 = zeros(6,numel(NodestoRun));
% CoefficientCatch97 = zeros(6,numel(NodestoRun));
% CoefficientCatch70 = zeros(6,numel(NodestoRun));

%%
% ------------------------------ 2. BASELINE ------------------------------ %

% Interpolates the CTEs and MCEV values in the baseline scenario. Used as
% the CTE values for Node 1 

% The results are used to create target CTEs in outer loops that are not
% baseline that have very out-of-sample results from the regression otherwise. 
% These values are only applicable for Node 1, given
% that that there is no data to accurately estimate what the CTE values
% would be if the scenario returned to baseline after having a non-baseline
% stress

originalNode = Node;

if UseBaselineExpectation == 1 && Node == 1
    CombinedCTESetAllNodes_BaselineOL = zeros(maxNodetoRun + 1, numel(BaseSetofEquityShocks));
    CombinedCTESetSSRAllNodes_BaselineOL = zeros(maxNodetoRun + 1, numel(BaseSetofEquityShocks));
    CombinedMCEVSetAllNode_BaselineOL = zeros(maxNodetoRun + 1, numel(BaseSetofEquityShocks));
         
    % Takes variables and runs regression for interpolation and
    % conducts grading
    [TARGrids, BaselineTARs, Coefficients] = InterpolateGridSquares(2, UseRescaledShocks, inputsBaseline.StressedValues, SPFloor, MillimanInputsStruc, parameters);
    
    % CTE values in the set of stresses
    CombinedCTESetAllNodes_BaselineOL(Node+1,:) = TARGrids.CombinedCTEGrid95;
    CombinedCTESetSSRAllNodes_BaselineOL(Node+1,:) = TARGrids.CombinedSSRGrid;
    CombinedMCEVSetAllNode_BaselineOL(Node+1,:) = TARGrids.GradedMCEVGrid;
    
    % CTE values in the baseline scenario
    CombinedCTEBaselineShock90(Node+1)= BaselineTARs.CTEBaselineShock90;
    CombinedCTEBaselineShock(Node+1) = BaselineTARs.CTEBaselineShock95;
    if OptimizerC == 1
        CombinedCTENoShock(Node+1) = BaselineTARs.CTENoShock;
    end
    
    % Transforms the inputs for SSR, which provides limited nodes, into a format
    % that can be interpolated inbetween nodes
    SSRBaselineGrid(inputsBaseline.StressedValues.NodesFromMilliman + 1) = inputsBaseline.StressedValues.SSR(1,:)/1000000000;

    % Full set of CTE values in the equity/rate shocks (no MCEV / grading
    % applies) - used for outputting
    CTE90BaselineFullSetBaseline = TARGrids.CTELowBaselineGrid/10^9;
    CTE95BaselineFullSetBaseline = TARGrids.CTEHighBaselineGrid95/10^9;
    SSR_BaselineOL = SSRBaselineGrid;

    newCTE95Baseline = [];
    for j = 1:numel(newIndexGrowth)
        StatEstimates = table2array(Coefficients.CoefficientsCTE95(:,1));
        % 10YT in the capital market scenario and baseline scenario
        Change10YTBaseline = OuterLoopBaseline(Node+2, 9) - OuterLoopBaseline(Node+1, 9);
        
        if newIndexGrowthAbsolute == 1
            % scale shock to be expressed relative to end of the year
            StartingIndexDelay = OuterLoopReal(Node+2, 2); % Next node's S&P level in real outer loop
            StartingIndexDelayBaseline = OuterLoopBaseline(Node + 2, 2); % Next node's S&P level in baseline
            IndexGrowthBaseline = StartingIndexDelayBaseline/StartingIndexDelay; % Equity growth in baseline
            
            nonRASEquityShock = newIndexGrowth(j) / IndexGrowthBaseline - 1;
            nonRASRateShock = RateShock_New;
        else
            nonRASEquityShock = newIndexGrowth(j) - 1;
            nonRASRateShock = RateShock_New;
        end
        
        newCTE95 = StatEstimates(1)+...
            StatEstimates(2)*(nonRASEquityShock)+...
            StatEstimates(3)*(nonRASRateShock)+...
            StatEstimates(4)*(nonRASEquityShock)^2+...
            StatEstimates(5)*(nonRASRateShock)^2+...
            StatEstimates(6)*(nonRASEquityShock*nonRASRateShock);
        
        newCTE95Baseline = [newCTE95Baseline newCTE95];
    end
    
    % Sets the target funding as the maximum of target statutory and target
    % economic funding. 
    if MCEVHedging == 1
        for j = 1:numel(BaseSetofEquityShocks)
            if BaseSetofEquityShocks(j) < 1
                CombinedCTESetAllNodes_BaselineOL(Node + 1,j) = max(CombinedCTESetAllNodes_BaselineOL(Node + 1,j),CombinedMCEVSetAllNode_BaselineOL(Node + 1,j)/1000000000);
            end
        end
    end
end 

%%
% --------------------- 3. REAL OUTER LOOP ------------------------------ %
% Repeats the process for the real outer loop.

CTE90BaselineFullSetTemp = zeros(numel(NodesFromMilliman), numel(BaseSetofEquityShocks));
CTE95BaselineFullSetTemp = zeros(numel(NodesFromMilliman), numel(BaseSetofEquityShocks));
newCTE95FullSet = zeros(numel(NodesFromMilliman), numel(newIndexGrowth));

% calculates the CTEs across different shocks
for i = 1: numel(NodesFromMilliman)
    Node = NodesFromMilliman(i);
    
    % Takes variables and runs regression for interpolation and
    % conducts grading
    [TARGrids, BaselineTARs, Coefficients] = InterpolateGridSquares(i, UseRescaledShocks, inputsReal.StressedValues, SPFloor, MillimanInputsStruc, parameters);
    
    CombinedCTESetAllNodes95(Node+1,:) = TARGrids.CombinedCTEGrid95;
    CombinedSSRAllNodes(Node+1,:) = TARGrids.CombinedSSRGrid;
    CombinedMCEVSetAllNodes(Node+1,:) = TARGrids.GradedMCEVGrid;
    
    CTE90BaselineFullSetTemp(i,:) = TARGrids.CTELowBaselineGrid;
    CTE95BaselineFullSetTemp(i,:) = TARGrids.CTEHighBaselineGrid95;
    
    % fills in the catches previously set
    CombinedCTEBaselineShock90(Node+1)= BaselineTARs.CTEBaselineShock90;
    CombinedCTEBaselineShock(Node+1) = BaselineTARs.CTEBaselineShock95;
    if OptimizerC == 1
        CombinedCTENoShock(Node+1) = BaselineTARs.CTENoShock;
    end
    
        
    for j = 1:numel(newIndexGrowth)
        StatEstimates = table2array(Coefficients.CoefficientsCTE95(:,1));
        % 10YT in the capital market scenario and baseline scenario
        Change10YTBaseline = OuterLoopBaseline(Node+2, 9) - OuterLoopBaseline(Node+1, 9);
        
        if newIndexGrowthAbsolute == 1
            % scale shock to be expressed relative to end of the year
            StartingIndexDelay = OuterLoopReal(Node+2, 2); % Next node's S&P level in real outer loop
            StartingIndexDelayBaseline = OuterLoopBaseline(Node + 2, 2); % Next node's S&P level in baseline
            IndexGrowthBaseline = StartingIndexDelayBaseline/StartingIndexDelay; % Equity growth in baseline
            
            nonRASEquityShock = newIndexGrowth(j) / IndexGrowthBaseline - 1;
            nonRASRateShock = RateShock_New;
        else
            nonRASEquityShock = newIndexGrowth(j) - 1;
            nonRASRateShock = RateShock_New;
        end
        
        newCTE95 = StatEstimates(1)+...
            StatEstimates(2)*(nonRASEquityShock)+...
            StatEstimates(3)*(nonRASRateShock)+...
            StatEstimates(4)*(nonRASEquityShock)^2+...
            StatEstimates(5)*(nonRASRateShock)^2+...
            StatEstimates(6)*(nonRASEquityShock*nonRASRateShock);
        
        newCTE95FullSet(Node+1, j) = newCTE95;
    end
end 

if numel(newIndexGrowth) > 0 && ~isnan(newIndexGrowth)
    newCTE95FullSet = interp1(NodesFromMilliman,newCTE95FullSet(NodesFromMilliman+1), 0:20,'linear');
end

% Transforms the inputs for CTE 70, which provides limited nodes, into a format
% that can be interpolated inbetween nodes
SSRBaselineGrid(inputsReal.StressedValues.NodesFromMilliman + 1) = inputsReal.StressedValues.SSR(1,:)/1000000000;

% Linearly interpolates for the missing nodes. Outputs the full set of
% calculated CTEs and its component parts -- CTE 90, CTE 95, MCEV and SSR
InterpolateAdditionalNodes;

% Sets the target funding grid equal to target funding at CTE 95
CombinedCTESetAllNodes = CombinedCTESetAllNodes95;

% Sets the target funding as the maximum of target statutory and target
% economic funding. THIS DOESNT TAKE INTO ACCOUNT BASELINE
if MCEVHedging == 1
    for i = 1:(maxNodetoRun + 1)
        for j = 1:numel(BaseSetofEquityShocks)
            if BaseSetofEquityShocks(j) < 1
                CombinedCTESetAllNodes(i,j) = max(CombinedCTESetAllNodes(i,j),CombinedMCEVSetAllNodes(i,j)/1000000000);
            end 
        end 
    end 
end

%---------------------------- 4. Combine grids -------------------------- %

% Combines node 1 of the baseline calculated CTE grid with the actual one 
% if target funding should precisely match those related to a "baseline" 
% economic variable.
% These variables are used in the calculation of tax reserves and the
% target hedge gains. If more information from Milliman is provided
% regarding baseline expectations, this should be updated. 
% A calculated baseline shock value is needed with non-baseline outer loop
% values. If it is a baseline run, the actual baseline values can be used. 
if UseBaselineExpectation == 1 && originalNode == 1
    SSRBaselineGrid(2) = SSR_BaselineOL(2); 
    CombinedCTESetAllNodes(2,:) = CombinedCTESetAllNodes_BaselineOL(2,:);
    CombinedSSRAllNodes(2,:) = CombinedCTESetSSRAllNodes_BaselineOL(2,:);
    CombinedCTEBaselineShock(2) = MillimanInputsBaseline(2,2);
    CombinedMCEVSetAllNodes(2,:) = CombinedMCEVSetAllNode_BaselineOL(2,:);
    
    if numel(newIndexGrowth) > 0
        newCTE95FullSet(:,2) = newCTE95Baseline;
    end
end 

