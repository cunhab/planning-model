% --------------------------ProcessInForceInputs--------------------------%
% Script #:
% Calls:
% Variables:

% This script runs IRC for a single product
%-------------------------------------------------------------------------%

function[InForceGPVADs, InForceCTE95, currentProduct_taxAdjustment, Milliman_Inputs_Original] = ...
        ProcessInForceInputs(varargin)
    p = inputParser;
    scenarioCols = strtrim(cellstr(num2str((1:1000)', 'x%d'))).';
    
    addParameter(p,'GPVAD_input_file', NaN)
    addParameter(p,'scenario_input_file', NaN)
    addParameter(p,'currentScenario', 0);
    addParameter(p,'StressedTemplate', 0);
    addParameter(p,'tax_rate', 0.35);
    
    parse(p,varargin{:});
    GPVAD_input_file = p.Results.GPVAD_input_file;
    scenario_input_file = p.Results.scenario_input_file;
    currentScenario = p.Results.currentScenario;
    StressedTemplate = p.Results.StressedTemplate;
    tax_rate = p.Results.tax_rate;
    
    if ~istable(StressedTemplate)
        load('IRC Inputs/DefaultInputs/StressedTemplate');
    end
    
    % if parameter is not passed, then read in / load parameter
    if isnan(GPVAD_input_file)
        [FileName,PathName] =  uigetfile({'*.xlsx;*.xlsm',  'Excel input file (*.xlsx, *.xlsm)'; '*.*',...
            'All Files'}, 'Select In-force GPVAD input file');
        GPVAD_input_file = sprintf('%s%s', PathName, FileName);
    end
    disp('Loading In-force GPVAD file');
    tic
    InForceGPVAD_table = readtable(GPVAD_input_file, 'Sheet', 'In-force');
    toc
    disp('Finished loading GPVAD file');

    if currentScenario ~= 0
        InForceGPVAD_table = InForceGPVAD_table(InForceGPVAD_table.Scenario == currentScenario,:);
    else
        currentScenario = unique(InForceGPVAD_table.Scenario);
        assert(numel(currentScenario) == 1, 'More than one scenario provided in inputs. Specify a scenario')
    end
    
    % order the In Force GPVADs based on template file
    rows = InForceGPVAD_table.Scenario == currentScenario;
    [orderedGPVADs_inforce, ia] = outerjoin(StressedTemplate, InForceGPVAD_table(rows,:),...
        'Type', 'left', 'MergeKeys', true, 'RightVariables', scenarioCols);
    [~, sortinds] = sort(ia);
    orderedGPVADs_inforce = orderedGPVADs_inforce(sortinds,:);
    
    % if parameter is not passed, then read in / load parameter
    if isnan(scenario_input_file)
        [FileName,PathName] =  uigetfile({'*.mat',  'Excel input file (*.mat)'; '*.*',...
            'All Files'}, 'Select processed scenario input file');
        scenario_input_file = sprintf('%s%s', PathName, FileName);
    end
    
    % loads the processed scenario information
    inputScenario = load(scenario_input_file);
    Milliman_Inputs_Original = inputScenario.inputFinancials;
    scenarioSettings = inputScenario.scenarioSettings;
    
    [Input_CTE_TaxAdjusted_ordered, ia] = outerjoin(StressedTemplate, inputScenario.taxAdjustmentTable,'Type','left', 'MergeKeys', true,...
        'RightVariables', {'TaxReserves', 'FFactorCTE90', 'FFactorCTE95'});
    [~, sortinds] = sort(ia);
    Input_CTE_TaxAdjusted_ordered = Input_CTE_TaxAdjusted_ordered(sortinds,:);
    
    taxAdjustment = table2array(Input_CTE_TaxAdjusted_ordered(:,{'FFactorCTE90', 'FFactorCTE95'}));
    taxAdjustment = taxAdjustment * tax_rate;
    
    % calculate tax adjusted CTEs
    InForceGPVADs = table2array(orderedGPVADs_inforce(:,scenarioCols));
    [InForceCTE90, InForceCTE95] = calculateCTEs(1,InForceGPVADs);
    currentProduct_taxAdjustment = taxAdjustment .* table2array(Input_CTE_TaxAdjusted_ordered(:,{'TaxReserves'}));
    InForceCTE95 = -(-InForceCTE95 + currentProduct_taxAdjustment(:,2));
    
    inforceBusinessinputs_filename = sprintf('IRC Inputs/InForceInputs_S%d',currentScenario);
    save(inforceBusinessinputs_filename, 'InForceGPVADs', 'InForceCTE95', 'currentProduct_taxAdjustment', 'inputScenario', 'Milliman_Inputs_Original', 'scenarioSettings');
end