% function [ OptionsPayoutAllShocks ] = getinstrumentpayoffs( portfolio )
    %GETINSTRUMENTPAYOFFS Calculates the payoffs of all the instruments
    %   Detailed explanation goes here
    
    ShockDelay = 0;

    %Set Current Date based on the length of the ShockDelay
    CurrentDate=addtodate(StartingDate,ShockDelay,'month');
    
    %Calculates the shock to equities and rates
    CurrentIndex= StartingIndex;
    Rates = StartingRates;
    SwapRates = StartingSwapRates;
    SwapRates_Q = StartingSwapRatesQuarterly;

    
    if ChangeATMVols == 1
        ATMVolTermStructure = ATMVolTermStructureReal_Y;
        [ImpliedVols, ImpliedVolsMet] = createvolsurfaces(ATMVolTermStructure(:,Node), -0.5:0.05:0.25, ImpliedVolatilitySpread);
        if Node == 1
            AnnualIndexChange = 1;
        else
            AnnualIndexChange = OuterLoopReal(Node+1,2)/OuterLoopReal(Node,2);
        end
        ImpliedVolsCurrent = ImpliedVolsMet;
        ImpliedVolsLastYear = ImpliedVolsCurrent;
    end
    
    SwapRateSpec = createratespec(SwapRates, CurrentDate);
    SwapRatesContinuous = OuterLoopRealSwaps((Node-1)*inputsReal.scenario.timestepPerYear+1,:);
    marketConditions = struct;
    marketConditions.CurrentIndex = CurrentIndex;
    marketConditions.ExpectedEquityReturns = ExpectedEquityReturns;
    marketConditions.priceOnIndex = priceOnIndex;
    marketConditions.SwapRatesContinuous = SwapRatesContinuous; 
    marketConditions.CurrentDate = CurrentDate;
    marketConditions.SwapRateSpec = SwapRateSpec;
    portfolio.initializeInstruments(marketConditions);
    
    currentFloatingRate = SwapRateSpec.Rates(1);
    [OptionsCosts, allInstrumentsCurrent] = portfolio.priceInstruments(ShockDelay, CurrentIndex, ImpliedVolsCurrent, ImpliedVolsRates, SwapRates_Q, SwapRateSpec, currentFloatingRate);
    OptionsVegas = allInstrumentsCurrent(:,{'BuyVega', 'SellVega'});
    OptionsStrikes = allInstrumentsCurrent(:,{'BuyStrikeActual', 'SellStrikeActual'});
    OptionsImpliedVols = allInstrumentsCurrent.Sigma;
    
    if AddPADRates == 1
        ImpliedVolsRates_PAD = ImpliedVolsRates;
        for i = 1:numel(ImpliedVolsRates_PAD)
            ImpliedVolsRates_PAD(i).Surface(2:end, 2:end) = ImpliedVolsRates(i).Surface(2:end, 2:end) + VolSensitivity;
        end
        [priceArray, allInstrumentsCurrent] = portfolio.priceInstruments(ShockDelay, CurrentIndex, ImpliedVolsCurrent, ImpliedVolsRates_PAD, SwapRates_Q, SwapRateSpec);
        OptionsCostsVolCatchRates(:,Node) = priceArray;
    end
    
    if AddPAD == 1
        ImpliedVolsCurrent_PAD = ImpliedVolsCurrent;
        ImpliedVolsCurrent_PAD(2:end,2:end) = ImpliedVolsLastYear(2:end,2:end) + VolSensitivity;
        [priceArray, allInstrumentsCurrent] = portfolio.priceInstruments(ShockDelay, CurrentIndex, ImpliedVolsCurrent_PAD, ImpliedVolsRates, SwapRates_Q, SwapRateSpec);
        OptionsCostsVolCatch(:,Node) = priceArray;
    end
    
    %% RAS Shock
    if ChangeATMVols == 1
        ATMVolTermStructure = ATMVolTermStructureBase_Y;
        ImpliedVols = createvolsurfaces(ATMVolTermStructure(:,Node + 1), -0.5:0.05:0.25, ImpliedVolatilitySpread);
    end
    ShockedPrices = [];
    EquityOptionsVolsAllShocks = [];
    ShockDelay=HedgeToShockDelay;
    currentFloatingRate = SwapRatesProjectionBaseline(1); 
    for n = 1:numel(SetofEquityShocks)
        % Creates the macroeconomic environment and selects the implied vol
        % surface
        EquityShock=SetofEquityShocks(n);
        RatesShock = SetofRateShocks(n);
        CurrentDate=addtodate(StartingDate,ShockDelay,'month');
        CurrentIndex= StartingIndex*EquityShock;
        Rates = RatesDelayBaseline+RatesShock; % this is inside T0, equities is in mastermacrohedge
        SwapRates = SwapRatesProjectionBaseline+RatesShock; % this is growing rates by baseline, then shocking them
        SwapRates_Q = SwapRatesProjectionBaselineQuarterly + RatesShock;
        %         Spread5=0;
        %         Spread10=0;
        ImpliedVolsCurrent = setvolscurrent(ImpliedVols, BaseSetofEquityShocks(n));
        SwapRateSpec = createratespec(SwapRates, CurrentDate);
        [priceArray, allInstrumentsCurrent] = portfolio.priceInstruments(ShockDelay, CurrentIndex, ImpliedVolsCurrent, ImpliedVolsRates, SwapRates_Q, SwapRateSpec, currentFloatingRate);
        EquityOptionsVolsAllShocks = [EquityOptionsVolsAllShocks, allInstrumentsCurrent.Sigma];
        ShockedPrices = [ShockedPrices, priceArray];
    end
    
    % Declares variable with T0 prices for each instrument as each column
    OptionsCostsMatrix = OptionsCosts*ones(1,numel(SetofEquityShocks));
    
    % Calculates instrument level hedge payoff in each of the shocks designated
    % for the risk appetite statement
    OptionsPayoutAllShocks = ShockedPrices - OptionsCostsMatrix;
    
    %% Baseline
        
    % Calculates yearly hedge gain / loss in the baseline scenario
    
    % Resets macroenvironment for end-of-the-year values following a baseline
    % path
    CurrentIndex = StartingIndex*IndexGrowthBaseline;
    Rates = RatesDelayBaseline;
    SwapRates = SwapRatesProjectionBaseline;
    SwapRates_Q = SwapRatesProjectionBaselineQuarterly;
        
    % Changes implied vol surfaces to end of year values
    if ChangeATMVols == 1
        ATMVolTermStructure = ATMVolTermStructureBase_Y;
        [ImpliedVols, ImpliedVolsMet] = createvolsurfaces(ATMVolTermStructure(:,Node+1), -0.5:0.05:0.25, ImpliedVolatilitySpread);
        ImpliedVolsCurrent = ImpliedVolsMet;
    else
        AnnualIndexChange = CurrentIndex/StartingIndex;
        BaselineIndexChange = OuterLoopBaseline(Node+2,2)/OuterLoopBaseline(Node+1,2);
        % BaselineIndexChange = OuterLoopBaseline(Node+1,2)/OuterLoopBaseline(Node,2);
        shockToBaseline = AnnualIndexChange / BaselineIndexChange;
        ImpliedVolsCurrent = setvolscurrent(ImpliedVols, shockToBaseline);
    end
    
    ShockDelay=NodeMonths; 
    CurrentDate=addtodate(StartingDate,ShockDelay,'month');
    
    SwapRateSpec = createratespec(SwapRates, CurrentDate);
    currentFloatingRate = SwapRateSpec.Rates(1);
    [PricesBaselineAnnual] = portfolio.priceInstruments(ShockDelay, CurrentIndex, ImpliedVolsCurrent, ImpliedVolsRates, SwapRates_Q, SwapRateSpec, currentFloatingRate);
    PayoutBaselineAnnualPerInstrument = PricesBaselineAnnual - OptionsCosts;
    
    %% Baseline - Quarterly
    
    % Calculates quarterly hedge gain / loss in the baseline scenario
    % Resets macroenvironment for quarterly values following baseline path,
    % linearly interpolating from the annual values
    ShockDelay = RebalanceMonths; 
    CurrentDate=addtodate(StartingDate,ShockDelay,'month');
    
    CurrentIndex = StartingIndex*(((IndexGrowthBaseline-1)*NodePerRebalance)+1);
    SwapRatesDelayBaselineQuarter = StartingSwapRates + (SwapRatesProjectionBaseline - StartingSwapRates)*NodePerRebalance;
    SwapRates = SwapRatesDelayBaselineQuarter;
    SwapRates_Q = StartingSwapRatesQuarterly + ...
        (OuterLoopBaselineSwaps(1 + (Node-1) * StepsPerNode_B...
        + ShockDelay / inputsBaseline.scenario.monthsPerTimestep,:)'...
        - OuterLoopBaselineSwaps(1 + (Node-1) * StepsPerNode_B,:)');
    QuarterlyIndexChange = CurrentIndex/StartingIndex-1; % baseline growth
    
    % Changes implied vol surfaces to the quarter values
    if ChangeATMVols == 1
        ATMVolTermStructure = ATMVolTermStructureBase_Q;
        [ImpliedVols, ImpliedVolsMet] = createvolsurfaces(ATMVolTermStructure(:,Node), -0.5:0.05:0.25, ImpliedVolatilitySpread);
        ImpliedVolsCurrent = ImpliedVolsMet;
    else
        if ShockDelay == 0
            shockToBaseline = CurrentIndex/StartingIndex;
        else
            AnnualIndexChange = QuarterlyIndexChange / NodePerRebalance + 1;
            BaselineIndexChange = OuterLoopBaseline(Node+2,2)/OuterLoopBaseline(Node+1,2);
            % BaselineIndexChange = OuterLoopBaseline(Node+1,2)/OuterLoopBaseline(Node,2);
            shockToBaseline = AnnualIndexChange / BaselineIndexChange;
        end
        ImpliedVolsCurrent = setvolscurrent(ImpliedVols, shockToBaseline);
    end
    
    SwapRateSpec = createratespec(SwapRates, CurrentDate);
    currentFloatingRate = SwapRateSpec.Rates(1); 
    [PricesBaselineQuarterly] = portfolio.priceInstruments(ShockDelay, CurrentIndex, ImpliedVolsCurrent, ImpliedVolsRates, SwapRates_Q, SwapRateSpec, currentFloatingRate);
    PayoutBaselineQuarterlyPerInstrument = PricesBaselineQuarterly - OptionsCosts;
    
    %% Real - Node (i.e. annual)
    
    ShockDelay=NodeMonths; 
    CurrentDate=addtodate(StartingDate,ShockDelay,'month');
    
    % Sets macroenvironment for the end of the node in the capital markets
    % scenario
    if Node == 0
        RealIndex = OuterLoopReal(Node+3,2);
        RealRates = OuterLoopReal(Node+3,3:10);
        RealSwapRates = OuterLoopReal(Node+3,26:33);
        RealSwapRates = RealSwapRates';
        RealSwapRates_Q = OuterLoopRealSwaps(1 + (Node+1) * StepsPerNode_R, :)';
    else
        RealIndex = OuterLoopReal(Node+2,2);
        RealRates = OuterLoopReal(Node+2,3:10);
        RealSwapRates = OuterLoopReal(Node+2,26:33);
        RealSwapRates = RealSwapRates';
        RealSwapRates_Q = OuterLoopRealSwaps(1 + (Node) * StepsPerNode_R, :)';
    end
    
    CurrentIndex=RealIndex;
    RealRates = RealRates';
    Rates = RealRates;
    RatesDelay = RealRates;
    SwapRates = RealSwapRates;
    SwapRates_Q = RealSwapRates_Q;
    
    % Changes implied vol surfaces to end of year values
    if ChangeATMVols == 1
        ATMVolTermStructure = ATMVolTermStructureReal_Y;
        [ImpliedVols, ImpliedVolsMet] = createvolsurfaces(ATMVolTermStructure(:,Node+1), -0.5:0.05:0.25, ImpliedVolatilitySpread);
        ImpliedVolsCurrent = ImpliedVolsMet;
    else
        if useRealVols == 1 && realVolsDelay == NodeMonths
            ImpliedVolsCurrent = ImpliedVolsMet_Real;
        elseif ShockDelay == 0
            shockToBaseline = CurrentIndex/StartingIndex;
            ImpliedVolsCurrent = setvolscurrent(ImpliedVols, shockToBaseline);
        else
            AnnualIndexChange = CurrentIndex/StartingIndex;
            shockToBaseline = AnnualIndexChange / BaselineIndexChange;
            ImpliedVolsCurrent = setvolscurrent(ImpliedVols, shockToBaseline);
        end
    end
    
    if useRealVols == 1
        % ImpliedVolsRates
    end
    
    SwapRateSpec = createratespec(SwapRates, CurrentDate);
    currentFloatingRate = SwapRateSpec.Rates(1); 
    [PricesRealAnnual] = portfolio.priceInstruments(ShockDelay, CurrentIndex, ImpliedVolsCurrent, ImpliedVolsRates, SwapRates_Q, SwapRateSpec, currentFloatingRate);
    PayoutRealPerInstrumentAnnual = PricesRealAnnual - OptionsCosts;
    
    %% Real - RebalanceMonths (i.e. Quarter)
    
    ShockDelay = RebalanceMonths; 
    CurrentDate=addtodate(StartingDate,ShockDelay,'month');
    
    % Sets macroenvironment
    if Node == 0
        RealIndex = (OuterLoopReal(Node+3,2)-OuterLoopReal(Node+2,2))*NodePerRebalance + OuterLoopReal(Node+2,2);
        RealRates = (OuterLoopReal(Node+3,3:10)-OuterLoopReal(Node+2,3:10))*NodePerRebalance + OuterLoopReal(Node+2,3:10);
        RealSwapRates = (OuterLoopReal(Node+3,26:33)-OuterLoopReal(Node+2,26:33))*NodePerRebalance + OuterLoopReal(Node+2,26:33);
        RealSwapRates_Q = OuterLoopRealSwaps(1 + (Node) * StepsPerNode_R + ...
            ShockDelay / inputsBaseline.scenario.monthsPerTimestep, :);
        RealSwapRates = RealSwapRates';
        RealSwapRates_Q = RealSwapRates_Q';
    else
        RealIndex = (OuterLoopReal(Node+2,2)-OuterLoopReal(Node+1,2))*NodePerRebalance + OuterLoopReal(Node+1,2);
        RealRates = (OuterLoopReal(Node+2,3:10)-OuterLoopReal(Node+1,3:10))*NodePerRebalance + OuterLoopReal(Node+1,3:10);
        RealSwapRates = (OuterLoopReal(Node+2,26:33)-OuterLoopReal(Node+1,26:33))*NodePerRebalance + OuterLoopReal(Node+1,26:33);
        RealSwapRates_Q = OuterLoopRealSwaps(1 + (Node-1) * StepsPerNode_R +...
            ShockDelay / inputsBaseline.scenario.monthsPerTimestep, :);
        RealSwapRates = RealSwapRates';
        RealSwapRates_Q = RealSwapRates_Q';
    end
    
    CurrentIndex=RealIndex;
    RealRates = RealRates';
    Rates = RealRates;
    RatesDelay = RealRates;
    SwapRates = RealSwapRates;
    SwapRates_Q = RealSwapRates_Q;
    
    % Changes implied vol surfaces to the quarter values
    if ChangeATMVols == 1
        ATMVolTermStructure = ATMVolTermStructureReal_Q;
        [ImpliedVols, ImpliedVolsMet] = createvolsurfaces(ATMVolTermStructure(:,Node), -0.5:0.05:0.25, ImpliedVolatilitySpread);
        ImpliedVolsCurrent = ImpliedVolsMet;
    else
        if useRealVols == 1 && realVolsDelay == RebalanceMonths
            ImpliedVolsCurrent = ImpliedVolsMet_Real;
        elseif ShockDelay == 0
            shockToBaseline = CurrentIndex/StartingIndex;
        else
            QuarterlyIndexChange = CurrentIndex/StartingIndex-1;
            AnnualIndexChange = QuarterlyIndexChange / NodePerRebalance + 1;
            shockToBaseline = AnnualIndexChange / BaselineIndexChange;
            ImpliedVolsCurrent = setvolscurrent(ImpliedVols, shockToBaseline);
        end
    end
    
    SwapRateSpec = createratespec(SwapRates, CurrentDate);
    currentFloatingRate = SwapRateSpec.Rates(1); 
    [PricesRealQuarterly] = portfolio.priceInstruments(ShockDelay, CurrentIndex, ImpliedVolsCurrent, ImpliedVolsRates, SwapRates_Q, SwapRateSpec, currentFloatingRate);
    PayoutRealPerInstrumentQuarter = PricesRealQuarterly - OptionsCosts;
    %% Other non-RAS shock payoffs
    
    % instrument payoffs in given index growth scenarios
    if numel(newIndexGrowth) > 0 && ~isnan(newIndexGrowth)
        % Resets macroenvironment for end-of-the-year values following the given
        % path
        if newIndexGrowthAbsolute == 1
            CurrentIndex = StartingIndex*IndexGrowthBaseline_New;
        else
            CurrentIndex = StartingIndex*IndexGrowthBaseline*IndexGrowthBaseline_New;
        end
        
        % Sets macroenvironment for the end of the year in the capital markets
        % scenario
        
        if Node == 0
            RealRates = OuterLoopReal(Node+3,3:10);
            RealSwapRates = OuterLoopReal(Node+3,26:33);
            RealSwapRates = RealSwapRates';
            RealSwapRates_Q = OuterLoopRealSwaps((Node+1)*StepsPerNode_R+1, :)';
        else
            RealRates = OuterLoopReal(Node+2,3:10);
            RealSwapRates = OuterLoopReal(Node+2,26:33);
            RealSwapRates = RealSwapRates';
            RealSwapRates_Q = OuterLoopRealSwaps((Node)*StepsPerNode_R+1, :)';
        end
        
        if RateShock_New ~= 0
            RealRates = RatesDelayBaseline + RateShock_New;
            RealSwapRates = SwapRatesProjectionBaseline+RateShock_New;
            RealSwapRates_Q = SwapRatesProjectionBaselineQuarterly+RateShock_New;
        end
        
        RealRates = RealRates';
        Rates = RealRates; 
        
        RatesDelay = RealRates; 
        SwapRates = RealSwapRates;
        SwapRates_Q = RealSwapRates_Q;
        AnnualIndexChange = CurrentIndex/StartingIndex;
        
        IndexGrowthScenarioPayout = zeros(size(OptionsCosts,1), numel(newIndexGrowth));
        IndexGrowthScenarioPayout_Q = zeros(size(OptionsCosts,1), numel(newIndexGrowth));
        IndexGrowthCTEs = zeros(1, numel(newIndexGrowth));
        for i_scenario = 1:numel(newIndexGrowth)
            IndexGrowthBaseline_New = newIndexGrowth(i_scenario);
            CalculateHedgeLoss_ChangeIndexGrowth;
            IndexGrowthScenarioPayout(:,i_scenario) = PayoutBaselineAnnualPerInstrument;
            IndexGrowthScenarioPayout_Q(:,i_scenario) = PayoutBaselineQuarterlyPerInstrument;
            IndexGrowthCTEs(i_scenario) = newCTE95;
        end
    end
% end
