%---------------------------------GlobalOptimizer-------------------------%
% Script #: 2.4.2.4
% Calls: N/A
% Variables: PurchaseAmounts represents the number of contracts of each
% hedge instrument purchased

% This script optimizes for a set of hedge instruments given various constraints. 
% Users may define which optimizer (in terms of upside selling) is used. 

%-------------------------------------------------------------------------%
%%

NumInstruments = size(portfolio.allInstruments,1);

% 1. Create optimization constraints
% Picks a starting guess for each value.  Each value in the array
% corresponds with a different instrument
x0=zeros(1,NumInstruments);

% Set Constraint Matrix which results in AA*[# contracts of each
% instrument] > BB, where AA is the payoff in ecah shock and BB is the
% target hedge gain of each shock
AA = [OptionsPayoutAllShocks];
BB = [PreTaxHedgeNeed];

% Sets constraint matrix AA equal to the per instrument hedge payoff across
% different risk appetite shocks in bear scenarios and BB as the target 
% hedge gain in bear scenarios, to be used in the optimizer.
% Also creates alternate sets of constraint matrices (Aeq, Aineq, Beq and Bineq)
% for a "pinched" method that requires the first scenario's actual payouts
% be exactly equal to the first scenario's required hedge payout
AA = [];
BB = [];
for i = 1: numel(BaseSetofEquityShocks)
    if BaseSetofEquityShocks(i) < 1 
        AA = [AA, OptionsPayoutAllShocks(:,i)];
        BB = [BB, PreTaxHedgeNeed(i)];

        Aeq = AA(:,1);
        Beq = BB(:,1);
        Aineq = AA(:, 2:size(AA,2));
        Bineq = BB(:, 2:size(BB,2));

    end 
end

% For optimizer D, creates additional constraints on the percent of upside
% that is permitted to be sold in upside scenarios, where AA_2 is the payoffs
% of each hedge instrument in the upside scenarios and BB_2 is the percent
% of upside sold permitted specified in the Dashboard
if OptimizerD == 1
    AA_2 = zeros(size(CombinedUpsidePayoffsNoBase));

    for i = 1: size(AA_2,2)
        if PositiveTAROnly == 1 
            AA_2(:,i) = (((CombinedUpsidePayoffsNoBase(:,i) - BaselineTestPayout)*(1-inputTaxRate))*1000000000)/(PositiveTARRelease(i)*1000000000);
        elseif SellIRUpside == 1
            AA_2(:,i) = (((CombinedUpsidePayoffsNoBase(:,i) - BaselineTestPayout)*(1-inputTaxRate))*1000000000)/(TARRelease(54+(i*5-1))*1000000000);
        else
            AA_2(:,i) = (((CombinedUpsidePayoffsNoBase(:,i) - BaselineTestPayout)*(1-inputTaxRate))*1000000000)/(TARRelease(54+i)*1000000000);
        end
    end
 
    AA = [AA, AA_2];
    Aineq = [Aineq, AA_2];
  
    BB_2 = zeros(1,size(CombinedUpsidePayoffsNoBase,2));
    for i = 1:numel(BB_2)
        if SellIRUpside == 1
            if SelectedRateShocks(i) == 0
                BB_2(i) = PercentTARReleaseToSell(Node);
            else
                BB_2(i) = PercentIRShockTARReleaseToSell(Node);
            end
        else
            BB_2(i) = PercentTARReleaseToSell(Node);
        end
    end 
    % BB_2 = PercentTARReleaseToSell(Node); % should be more efficient than
    % above
    BB = [BB, BB_2];
    Bineq = [Bineq, BB_2];
    
end 

% Transposes matrices 
AA = -[transpose(AA)];
Aeq = -Aeq';
Aineq = -Aineq';

% ------------------------------------------------------------------------%
% 2. Set instrument level constraints

% Default of lower bounds (min contracts) of zero for each instrument
lb = zeros(1,NumInstruments);

% Default of unlimited upper bound (max contracts), realized as impossibly
% high upper bound value for each instrument
ub = ones(1,NumInstruments)*1000; 

% Sets the upper and lower bounds for all 113 hedge instruments.
lb = portfolio.allInstruments.LowerBound / 1000;
lb(ismember(portfolio.allInstruments.BoundType, 'Notional')) = lb(ismember(portfolio.allInstruments.BoundType, 'Notional')) / StartingIndex;
ub = portfolio.allInstruments.UpperBound / 1000;
ub(ismember(portfolio.allInstruments.BoundType, 'Notional')) = ub(ismember(portfolio.allInstruments.BoundType, 'Notional')) / StartingIndex;

% Establishes a "bedrock" of the 80% 5 year put such that it provides 
% 25% of the payoff of the -50%, -100 bps risk appetite stress required hedge
% payout. 
if CreateBedrock == 1
    instrumentIndex = find(ismember(portfolio.allInstruments.Instrument, 'Put') & ...
        portfolio.allInstruments.Term == 5 & ...
        portfolio.allInstruments.BuyStrike == 0.8);
    
    shockIndex = find(BaseSetofEquityShocks == 0.5 & BaseSetofRateShocks == -0.01);
    RequiredPayoff5y = BedrockPercent*(-PreTaxHedgeNeed(shockIndex));
    BedrockAmount = RequiredPayoff5y/(ShockedPrices(14, 1)-OptionsCosts(14));
    ub(instrumentIndex) = BedrockAmount; % 20% OTM 5yr put
    lb(instrumentIndex) = BedrockAmount;
end 

% This is a constraint on the inputs to the optimizer such that if 
% payoffs are negative in all scenarios, the instrument is precluded from
% the optimization
TestNegativePayouts = zeros(size(OptionsPayoutAllShocks,1),size(OptionsPayoutAllShocks,2));

for i = 1:length(OptionsPayoutAllShocks)
    for j = 1:size(OptionsPayoutAllShocks,2)
        if OptionsPayoutAllShocks(i,j) >= 0
            TestNegativePayouts(i,j) = 1;
        else 
            TestNegativePayouts(i,j) = 0;
        end 
    end
end


for i = 1:length(OptionsPayoutAllShocks)
    TestNegativePayoutsSum= sum(TestNegativePayouts(i,:));
    if TestNegativePayoutsSum == 0
        ub(i) = 0;
        lb(i) = 0;
    end 
end 

%-------------------------------------------------------------------------%

% 3. Run solver algorithm 

% Sets the mechanics of the global optimizer, the inputs of
% which are set in the dashboard. 
options=optimoptions(@fmincon, 'Algorithm', 'interior-point', 'MaxFunctionEvaluations', GlobalMaxFunctionEvaluations, 'MaxIterations', GlobalMaxIterations);

% Creates a series of optimization problems that differ in the way they
% treat upside
if OptimizerA == 1
    problem = createOptimProblem('fmincon','x0',x0,'Aineq', AA, 'bineq', BB, 'lb', lb, 'ub', ub, 'options', options, 'objective',@(x)max(-(x*CombinedUpsidePayoffs)));
    disp('Opt A used');
elseif OptimizerB == 1
    if PinchPayout == 1 
        problem = createOptimProblem('fmincon','x0',x0,'Aineq', Aineq, 'bineq', Bineq, 'Aeq', Aeq, 'beq', Beq, 'lb', lb, 'ub', ub, 'options', options, 'objective',@(x)max(-((x*CombinedUpsidePayoffsNoBase))./BullScenarioTARDecreases(Node+1,:)));
    else
        problem = createOptimProblem('fmincon','x0',x0,'Aineq', AA, 'bineq', BB, 'lb', lb, 'ub', ub, 'options', options, 'objective',@(x)max(-((x*CombinedUpsidePayoffsNoBase))./BullScenarioTARDecreases(Node+1,:)));
    end  
    disp('Opt B used');
elseif OptimizerC == 1
    if PinchPayout == 1  
        problem = createOptimProblem('fmincon','x0',x0,'Aineq', Aineq, 'bineq', Bineq, 'Aeq', Aeq, 'beq', Beq, 'lb', lb, 'ub', ub, 'options', options, 'objective',@(x)max(-((x*CombinedUpsidePayoffs))./BullScenarioTARDecreases(Node+1,:)));
    else 
        problem = createOptimProblem('fmincon','x0',x0,'Aineq', AA, 'bineq', BB, 'lb', lb, 'ub', ub, 'options', options, 'objective',@(x)max(-((x*CombinedUpsidePayoffs))./BullScenarioTARDecreases(Node+1,:)));
    end 
    disp('Opt C used');
elseif OptimizerD == 1
    if PinchPayout == 1
        problem = createOptimProblem('fmincon','x0',x0,'Aineq', Aineq, 'bineq', Bineq, 'Aeq', Aeq, 'beq', Beq, 'lb', lb, 'ub', ub, 'options', options, 'objective',@(x)-sum(x.*BaselineTestPayout')); 
    else
        if optimizeToNewBaseline == 1
            problem = createOptimProblem('fmincon','x0',x0,'Aineq', AA, 'bineq', BB,'lb', lb, 'ub', ub, 'options', options, 'objective',@(x)-sum(x.*IndexGrowthScenarioPayout(:,1)'));
        elseif optimizeQuarterly == 1
            problem = createOptimProblem('fmincon','x0',x0,'Aineq', AA, 'bineq', BB,'lb', lb, 'ub', ub, 'options', options, 'objective',@(x)-sum(x.*BaselineTestPayout_Q'));
        else
            problem = createOptimProblem('fmincon','x0',x0,'Aineq', AA, 'bineq', BB,'lb', lb, 'ub', ub, 'options', options, 'objective',@(x)-sum(x.*BaselineTestPayout'));
        end
    end
    disp('Opt D used');
end 

% Runs the optimization problem
gs=GlobalSearch;
[PurchaseAmounts,fval,exitflag] = run(gs, problem);
exitflags = [exitflags, exitflag];




