
if useOldSwapsPricing == 1
    PriceSwaps;
    
    if testSwapDifference ~= 0
        if useContinuousSwaps == 0
            OriginalRates = SwapRateSpec.Rates;
            SwapRateSpecOriginal = SwapRateSpec;
            ZeroRates = exp(SwapRateSpec.Rates)-1;
            SwapRateSpec.Rates = ZeroRates; 
            SwapRateSpec.Compounding = 1;
            
            SwapPrice_Temp = getSwapPrice(NodeSwaps, SwapRateSpec, ShockDelay);
            if RASShock == 1
                SwapCashFlows_Temp = getSwapCashFlows(NodeSwaps, ShockDelay, 0, Node, OuterLoopBaselineSwaps);
            else
                SwapCashFlows_Temp = getSwapCashFlows(NodeSwaps, ShockDelay, 0, Node, OuterLoopRealSwaps); 
            end
            
            SwapRateSpec.Rates = OriginalRates;
            SwapRateSpec.Compounding = -1;
        else
            SwapPrice_Temp = getSwapPrice(NodeSwaps, SwapRateSpec, ShockDelay);
            if RASShock == 1
                SwapCashFlows_Temp = getSwapCashFlows(NodeSwaps, ShockDelay, 0, Node, OuterLoopBaselineSwaps); 
            else
                SwapCashFlows_Temp = getSwapCashFlows(NodeSwaps, ShockDelay, 0, Node, OuterLoopRealSwaps); 
            end
        end
        LegRateDiff = [LegRateDiff, NodeSwaps.LegRate(:,1) - allSwaps.FixedCouponRate];
        SwapPriceNoCFsDiff = [SwapPriceNoCFsDiff, SwapPricesNoCF - SwapPrice_Temp];
        SwapCFsDiff = [SwapCFsDiff, SwapCashFlows - SwapCashFlows_Temp];
    end
else
    if useContinuousSwaps == 0
        OriginalRates = SwapRateSpec.Rates;
        SwapRateSpecOriginal = SwapRateSpec;
        ZeroRates = exp(SwapRateSpec.Rates)-1;
        SwapRateSpec.Rates = ZeroRates;
        SwapRateSpec.Compounding = 1;
        
        SwapPrice_Temp = getSwapPrice(NodeSwaps, SwapRateSpec, ShockDelay);
        if RASShock == 1
            SwapCashFlows_Temp = getSwapCashFlows(NodeSwaps, ShockDelay, 0, Node, OuterLoopBaselineSwaps); 
        else
            SwapCashFlows_Temp = getSwapCashFlows(NodeSwaps, ShockDelay, 0, Node, OuterLoopRealSwaps); 
        end
        
        SwapRateSpec.Rates = OriginalRates;
        SwapRateSpec.Compounding = -1;
    else
        SwapPrice_Temp = getSwapPrice(NodeSwaps, SwapRateSpec, ShockDelay);
        if RASShock == 1
            SwapCashFlows_Temp = getSwapCashFlows(NodeSwaps, ShockDelay, 0, Node, OuterLoopBaselineSwaps);
        else
            SwapCashFlows_Temp = getSwapCashFlows(NodeSwaps, ShockDelay, 0, Node, OuterLoopRealSwaps); 
        end
    end
    
    SwapPrices = SwapPrice_Temp + SwapCashFlows_Temp;
end
