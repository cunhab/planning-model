% ------------------------CalculateUpside -------------------------------%
% Script #: 2.3
% Calls: N/A
% Variables: BullScenarioTARDecreases represents the "upside" or capital
% release in upside scenarios (used in GlobalOptimizer
% 
% This calculates the capital release in upside scenarios, which can be used 
% in the optimizer to minimize upside sold. 

% ------------------------------------------------------------------------%

% Selects CTE 95 in a baseline scenario
if UseRescaledShocks == 1 && UseBaselineExpectation == 0
    CTEValueBaseFollow = CombinedCTEBaselineShock';
elseif UseRescaledShocks == 1 && UseBaselineExpectation == 1
    CTEValueBaseFollow = CombinedCTEBaselineShock'; 
%     CTEValueBaseFollow(2) = CTEValueBase(2);
else 
    CTEValueBaseFollow = CTEValueBase(1:maxNodetoRun+1)';
end 

% For optimizer C, determines the TAR decrease in baseline from a no shock
% value
TARDecreaseBaseline = zeros(1,21);
for i = 1:min(numel(CTEValueBase), numel(CombinedCTENoShock))
    TARDecreaseBaseline(i) = CombinedCTENoShock(i) - CTEValueBaseFollow(i); 
end

% Optimizer B uses the baseline CTE values. Optimizer C includes the
% decrease in baseline TAR, and sets the base measurement to be equal to
% the calculated no shock (i.e., zero growth) CTE levels. 
if OptimizerB == 1 || OptimizerA == 1 || OptimizerD == 1 
    BullScenarioTARDecreases = [];
%     CTEValueBaseT = CombinedCTEBaselineShock';
    CTEValueBaseT = CTEValueBaseFollow;
elseif OptimizerC == 1 
    BullScenarioTARDecreases = TARDecreaseBaseline'/1000000000;
    CTEValueBaseT = CombinedCTENoShock'; 
end

% Calculates the capital release in up scenarios from the base measurement
% (either baseline or no shock). BullScenarioTARDecreases is used in the
% optimizer. If economic hedging is used, only scenarios with no rate shock
% is used to measure upside
for i = 1:numel(BaseSetofEquityShocks) 
    if MCEVHedging == 1
        if BaseSetofEquityShocks(i) > 1 && BaseSetofRateShocks(i) == 0
            BullScenarioTARDecreasesTemp = CTEValueBaseT/1000000000-CombinedCTESetAllNodes(:,i);
            BullScenarioTARDecreases = [BullScenarioTARDecreases, BullScenarioTARDecreasesTemp];    
        end
    else
        if BaseSetofEquityShocks(i) > 1 
            BullScenarioTARDecreasesTemp = CTEValueBaseT/1000000000-CombinedCTESetAllNodes(:,i);
            BullScenarioTARDecreases = [BullScenarioTARDecreases, BullScenarioTARDecreasesTemp];  
        end 
    end
end 

