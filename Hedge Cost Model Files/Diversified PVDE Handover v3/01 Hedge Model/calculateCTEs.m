%% calculateCTEs function
% Determines the CTE 90 and CTE 95 from GPVADs. If only one GPVAD (double array) is
% supplied, determines CTE of that one GPVAD, scaled by numNewPolicies.
% If two GPVAD double arrays are supplied, determines the CTE of the
% NewBusinessGPVADs scaled by numNewPolicies with InForceGPVADs (not
% scaled).
function [CTE90, CTE95] = calculateCTEs(numNewPolicies, NewBusinessGPVADs, InForceGPVADs)
    switch nargin
        case 2
            combinedGPVADs = min(NewBusinessGPVADs * numNewPolicies, 0);
        case 3
            NewBusinessGPVADs_extended = nan(size(InForceGPVADs));
            NewBusinessGPVADs_extended(1:size(NewBusinessGPVADs,1),:) = NewBusinessGPVADs;
            nanIndices = isnan(NewBusinessGPVADs_extended);
            combinedGPVADs = min(NewBusinessGPVADs_extended * numNewPolicies + InForceGPVADs, 0);
            combinedGPVADs(nanIndices) = NaN;
        otherwise
            error('Not enough arguments provided.');
    end
    sortedGPVADs = sort(combinedGPVADs, 2);
    numScenarios = size(sortedGPVADs,2);
    CTE90 = mean(sortedGPVADs(:,1:(numScenarios*.1)),2);
    CTE95 = mean(sortedGPVADs(:,1:(numScenarios*.05)),2);
end