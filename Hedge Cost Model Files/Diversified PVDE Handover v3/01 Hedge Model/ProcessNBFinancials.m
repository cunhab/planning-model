%------------------------------ProcessNBFinancials------------------------%
% Script #: 

% This script sets the default user paramaters.
% Set ignoreFAMargin to 1 for Shield to exclude NII from total cashflows as
% NII is calculated elsewhere in the model. FAMargin is 0 across all times 
% for Flex.
%-------------------------------------------------------------------------%

function [allCells_struct] = ProcessNBFinancials(filename, varargin)
    p = inputParser;
    defaultSheetName = 'AggregatedRollforwardFinancials';
    addParameter(p,'SheetName',defaultSheetName);
    addParameter(p,'IsFilename', 1);
    addParameter(p,'ignoreFAMargin',0); % if 1, ignore the earned interest b/c hedge model calculates it
    addParameter(p,'currentScenario', NaN);
    addParameter(p,'startT0', 1); % inputs start with a T0 column (note: set to 0 if using VASB outputs)

    parse(p,varargin{:});
    SheetName = p.Results.SheetName;
    IsFilename = p.Results.IsFilename;
    ignoreFAMargin = p.Results.ignoreFAMargin;
    currentScenario = p.Results.currentScenario;
    startT0 = p.Results.startT0;
    
    if IsFilename == 1
        AggregatedFinancials = readtable(filename, 'Sheet', SheetName);
    else
        AggregatedFinancials = filename;
    end
    
    % get the line item names, if none are provided in the file, use
    % defaults
    LineItems = unique(AggregatedFinancials.Cashflow, 'stable');
    if ~isempty(LineItems)
        LineItems = LineItems(~strcmp(LineItems, ''),:);
    end
    if iscell(LineItems)
        nanValues = cellfun(@(V) any(isnan(V(:))), LineItems);
        nanValues = any(nanValues);
    else
        nanValues = isnan(LineItems);
    end
    if isempty(LineItems) || nanValues
        LineItems = {'AnnFees'; 'DBFees'; 'FA Margin'; 'LBFees'; 'MEFees';...
            'RevShare';'SurrCharges';'UpfrontCommissions';'TrailCommissions';...
            'AcqExpenses'; 'MaintExpensesF'; 'MaintExpensesV'; 'MaintExpensesA';...
            'DBClaims';'LBClaims_LC';'LBClaims_NLC'; 'CARVM Interest';'CARVM Runoff'};
    end
    
    varnames = AggregatedFinancials.Properties.VariableNames;
    others = ~ismember(varnames,{'Var1', 'Shock', 'Year', 'Scenario', 'Cell', 'Cashflow'});
    monthCols = varnames(:,others);
    
    % select only the last year and 0 Shock outputs if those columns exist
    if any(strcmp('Shock', AggregatedFinancials.Properties.VariableNames))
        rows_unique = AggregatedFinancials.Year == max(AggregatedFinancials.Year) & AggregatedFinancials.Shock == 0;
        AggregatedFinancials_unique = AggregatedFinancials(rows_unique, :);
    else
        AggregatedFinancials_unique = AggregatedFinancials;
    end
    
    % select only the given scenario
    if ~isnan(currentScenario)
        AggregatedFinancials_unique = AggregatedFinancials(AggregatedFinancials.Scenario == currentScenario,:);
    else
        assert(numel(unique(AggregatedFinancials.Scenario)) == 1, 'More than one scenario provided in inputs. Specify a scenario');
    end
    
    allCells_struct = struct;
%     numOfCells = max(AggregatedFinancials_unique{:,'Cell'});
%     uniqueCells = (1:ceil(numOfCells/2)) * 2 - 1;
    % ignore even cells because they are just duplicates of odd cells -
    % no longer applicable and adjusted the formula to mod (1)==0 because
    % duplicates are removed
    % with different rate shock
    uniqueCells = unique(AggregatedFinancials_unique{:,'Cell'});
    uniqueCells = uniqueCells(mod(uniqueCells,1) == 0);
    for i_cell = 1:numel(uniqueCells)
        currentCellFinancials = AggregatedFinancials_unique(AggregatedFinancials_unique.Cell == uniqueCells(i_cell),:);
        currentCellFinancials.Properties.RowNames = LineItems;
        
        annualFinancials = zeros(11,21);
        
        % if all the columns are labeled, then we can name the rows
        if ignoreFAMargin == 1
            RevenueFeesMonthly = sum(table2array(currentCellFinancials({'MEFees','RevShare'},monthCols)));
        else
            RevenueFeesMonthly = sum(table2array(currentCellFinancials({'FA Margin','MEFees','RevShare'},monthCols)));
        end
        PerPolicyChargeMonthly = table2array(currentCellFinancials({'AnnFees'},monthCols));
        SurrenderChargeMonthly = table2array(currentCellFinancials({'SurrCharges'},monthCols));
        RiderFeeMonthly = sum(table2array(currentCellFinancials({'DBFees', 'LBFees'},monthCols)));
        ExpensesMonthly = sum(table2array(currentCellFinancials({'AcqExpenses', 'MaintExpensesF','MaintExpensesV', 'MaintExpensesA'},monthCols)));
        CommissionsMonthly = table2array(currentCellFinancials({'TrailCommissions'},monthCols));
        BenefitClaimsMonthly = sum(table2array(currentCellFinancials({'DBClaims', 'LBClaims_LC','LBClaims_NLC'},monthCols)));
        
        monthlyFinancials = vertcat(RevenueFeesMonthly, PerPolicyChargeMonthly, SurrenderChargeMonthly,...
            RiderFeeMonthly, ExpensesMonthly, CommissionsMonthly, BenefitClaimsMonthly);
   
        % 09/21 if startT0 == 1, then there is a T0 column in the financials
        % input, otherwise, the inputs start in year 1
        if startT0 == 1
            monthlyFinancials_postT0 = monthlyFinancials(:,2:end);
            if mod(size(monthlyFinancials_postT0,2),12) ~= 0
                warning('Financials do not have 12 months of data for each year. Adding months to the end with zeros.')
                monthlyFinancials_postT0(:,size(monthlyFinancials_postT0,2)+1:(ceil(size(monthlyFinancials_postT0,2)/12)*12)) = 0;
            end
            annualFinancials(1:7,1) = monthlyFinancials(:,1);
            annualFinancials(1:7,2:(size(monthlyFinancials_postT0,2)/12 + 1)) = monthlyToAnnual(monthlyFinancials_postT0);
        else
            if mod(size(monthlyFinancials,2),12) ~= 0
                warning('Financials do not have 12 months of data for each year. Adding months to the end with zeros.')
                monthlyFinancials_postT0(:,size(monthlyFinancials,2)+1:(ceil(size(monthlyFinancials,2)/12)*12)) = 0;
            end
            annualFinancials(1:7,2:(ceil(size(monthlyFinancials,2)/12) + 1)) = monthlyToAnnual(monthlyFinancials);
            annualFinancials(6,1) = table2array(currentCellFinancials({'UpfrontCommissions'},monthCols(1)));
        end
   % 2017/09/18 changed below to convert empty column (NAs)  to zeros
        CARVMMonthly_noNaNs = table2array(currentCellFinancials({'CARVM Runoff'},monthCols));
        if any(isnan(CARVMMonthly_noNaNs))
            warning('Coercing empty cells to 0. Make sure projection is for the entire timespan.')
            CARVMMonthly_noNaNs(isnan(CARVMMonthly_noNaNs)) = 0;
        end
        CARVMMonthly = cumsum(CARVMMonthly_noNaNs, 'reverse');     
        CARVMIndices = (0:(size(CARVMMonthly,2)-1)/12) * 12 + 1;
        annualFinancials(8,1:(size(CARVMMonthly(CARVMIndices),2))) = CARVMMonthly(CARVMIndices);
   % end of 09/18 change    
        allCells_struct(i_cell).PolicyID = uniqueCells(i_cell);
        allCells_struct(i_cell).Financials = annualFinancials;
    end
end
    
function[annualMatrix] = monthlyToAnnual(monthlyFinancials)
    annualMatrix = zeros(size(monthlyFinancials,1), size(monthlyFinancials,2)/12);
    for row = 1:size(monthlyFinancials,1)
        annualMatrix(row,:) = sum(reshape(monthlyFinancials(row,:),12,[]));
    end
end