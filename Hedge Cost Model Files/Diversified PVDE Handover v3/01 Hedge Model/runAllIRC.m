% ------------------------------runAllIRC---------------------------------%
% Script #:
% Calls: 
% Variables: 

% This script runs IRC for multiple products. Change the toggles to
% determine 
%-------------------------------------------------------------------------%

disaggregatedCells = 0;
accountValueSets = [20];
impliedVolatilitiesFile = 'Inputs/Volatilities20171231.mat';
parametersFile = 'C:\Users\hashworth\Documents\Diversified PVDE Handover v3\02 Hedge Model Inputs\HedgeModelParameterIRC.xlsx';
CSV_input_file = 'IRC Inputs\ShieldCSVs.xlsx';

load(impliedVolatilitiesFile);
[parameters,portfolio] = setparameters('fileInput', parametersFile);

[FileName,PathName] =  uigetfile({'*.mat',  'Matlab file (*.mat)'; '*.*',...
    'All Files'}, 'Select new business product/scenario input file', 'MultiSelect', 'on');
inputFiles = fullfile(PathName, FileName);
if ~iscell(inputFiles)
    inputFiles = {inputFiles};
end

for i = 1:numel(inputFiles)
    [pathstr, NB_filename, ext] = fileparts(inputFiles{i});
    productDate_end = regexp(NB_filename, '_NewBusiness');
    productDate = NB_filename(1:(productDate_end - 1));
    [~, endIndex] = regexp(NB_filename, '_S[0-9]*_');
    productName = NB_filename(endIndex+1:end);
    [startIndex, endIndex] = regexp(productName, ' \(.*\)');
    if ~isempty(startIndex)
        fileSuffix = productName((startIndex + 1):endIndex);
        productName = productName(1:(startIndex-1));
    else
        fileSuffix = NaN;
    end
    
    load(inputFiles{i})
    currentScenario = unique(NewBusinessGPVAD_table.Scenario);
    
    InForceInputs = sprintf('IRC Inputs/InForceInputs_S%d',currentScenario);
    
    baselineInputs = sprintf('IRC Inputs/%s_S1_InputSet_%s',productDate, productName);
    if ~isnan(fileSuffix)
        baselineInputs = strjoin({baselineInputs, fileSuffix});
    end
    
    inputsRealFilename = sprintf('Inputs/S%d.mat',currentScenario);
    inputsReal = load(inputsRealFilename);
    
    
    runIRC(InForceInputs, NewBusinessByCell, ...
        'ImpliedVolsEquities', ImpliedVolsEquities, 'ImpliedVolsRates', ImpliedVolsRates, ...
        'parameters', parameters, 'portfolio', portfolio, ...
        'fileSuffixProduct', fileSuffix, 'disaggregatedCells', disaggregatedCells, ...
        'accountValueSets', accountValueSets, 'baselineInputs', baselineInputs, ...
        'CSV_input_file', CSV_input_file);
end