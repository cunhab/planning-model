%------------------------CreateResultsMacroHedge--------------------------%
% Script #: 2.4.4
% Calls: N/A
% Variables: RunSummary corresponds to the values outputted in the Run
% Summary portion of output. HedgeResults and PayoutTable correspond to the 
% instrument level payoffs and details portion of output. 
% HedgeResultsSummary corresponds to the target hedge payoff table portion 
% of the output.
%
% This script prices swaps in a given macroeconomic environment from a zero
% curve
%-------------------------------------------------------------------------%

% RunSummary output
columnOutput = ['ABCDEFGHIJKLMNOPQRSTUVQXYZ'];

% Transforms variables into output format and writes table into Excel file
% assigned in the Dashboard
PurchaseAmounts = PurchaseAmounts';
CTEValue95BaselineMM = CTEValue95Base/1000000;
FundingMM = MasterFunding*1000; % for output purposes
PreHedgeAssetChangeMM = (NetCashFlowCRF)*1000;
SumPayoutRealMM = SumPayoutReal*1000; % pre tax
AdjSumPayoutMM = AdjSumPayout*1000;
DividendPayoutMM = Dividends(Node+1)*1000;
EndingAssetsMM = EoPFunding*1000;
% CTE no shock and CTE follow created in DetermineCalculatedCTEs/RunRegression
APremiumCosts = sum(PurchaseAmounts.*OptionsCosts)*1000;
if AddPAD == 1
    APremiumVol = VolPremiumCosts_EquitiesOnly;
else
    APremiumVol = APremiumCosts;
end 
if AddPADRates == 1
    APremiumVolRates = VolPremiumCosts_RatesOnly;
else
    APremiumVolRates = APremiumCosts;
end
RunSummaryInfo = [CTEValue95BaselineMM(Node+1); Deductible*1000; FundingMM; PreHedgeAssetChangeMM; SumPayoutRealMM; AdjSumPayoutMM; DividendPayoutMM; EndingAssetsMM; APremiumCosts; CTEValueBaseFollow(Node+1)/1000000000; APremiumVol; APremiumVolRates];  
RunSummary = table;
RunSummary.Run_Summary = RunSummaryInfo;
writetable(RunSummary,outputFilename,'Sheet',sprintf('Node %d',Node),'Range','C2');

%-------------------------------------------------------------------------%
% Creates table for aggregated payout results
PayoutTable = table;

% Purchase amounts from the optimizer
APurchaseAmounts=PurchaseAmounts*1000;

%Sum the payouts in each scenario and merge these sums for each scenario
%and converts to $MM
SumPayouts;
if instantaneousShock == 1
    InstantaneousShockPayoff;
end
AHedgePayouts=AHedgePayouts*1000; % converts to $MM

% Initial premium cost
AOptionsCosts=OptionsCosts;

%Creates a variable that shows premium cost spent per instrument
AHedgeCostSpent = APurchaseAmounts.*AOptionsCosts;
 
% Vega
AVega = OptionsVegas{:,:}/100;

% Implied Volatility
AImpliedVols = OptionsImpliedVols;

% Converts upper and lower bounds to MM
LowerBounds = lb*1000;
UpperBounds = ub*1000;

PayoutTable.InstrumentNum = (1:size(portfolio.allInstruments,1))';
PayoutTable.Instrument = portfolio.allInstruments.InstrumentOutputName;
PayoutTable.BaselinePayoutQuarterly = PayoutBaselineQuarterly*1000;
PayoutTable.BaselinePayoutAnnual = PayoutBaselineAnnual*1000;
PayoutTable.QuarterlyPayout = PayoutRealQuarter*1000;
PayoutTable.AnnualPayout = PayoutRealAnnual*1000;
PayoutTable.Purchase_NumberMM = APurchaseAmounts;
PayoutTable.Cost_Per_Instrument = AOptionsCosts;
PayoutTable.CostSpentMM = AHedgeCostSpent;
if(outputStrikes == 1)
    PayoutTable.StrikesLong = OptionsStrikes.BuyStrikeActual;
    PayoutTable.StrikesShort = OptionsStrikes.SellStrikeActual;
end
PayoutTable.ImpliedVolatility = AImpliedVols;
PayoutTable.Vega_Total = AVega .* [APurchaseAmounts -APurchaseAmounts];
PayoutTable.Vega = AVega;
PayoutTable.Lower_Bounds = LowerBounds;
PayoutTable.Upper_Bounds = UpperBounds;
PayoutTable.Scenario = AHedgePayouts;

writetable(PayoutTable,outputFilename,'Sheet',sprintf('Node %d',Node),'Range','B25');

HedgeResultsAdd = table;
if numel(newIndexGrowth) > 0 && ~isnan(newIndexGrowth)
    TestScenarioResults = table;
    ATestScenarioResults = [];
    for j_scenario = 1:numel(newIndexGrowth)
        ATestScenarioResults = [ATestScenarioResults,PayoutIndexGrowthQuarterly(:, j_scenario),...
            PayoutIndexGrowthAnnual(:,j_scenario)];
    end
    HedgeResultsAdd.TestScenarioResults = ATestScenarioResults*1000;
end
if instantaneousShock == 1
    HedgeResultsAdd.InstantShockScenario = AInstantHedgePayouts;
end

if (numel(newIndexGrowth) > 0 && ~isnan(newIndexGrowth))|| instantaneousShock == 1
    currentColumnIndex = 4 + size(PayoutTable, 2) + (size(HedgeResults,2)+1) + size(AHedgePayouts,2);
    if currentColumnIndex == rem(currentColumnIndex-1, 26) + 1
        currentColumn = columnOutput(currentColumnIndex);
    else
        currentColumn = sprintf('%s%s', columnOutput(floor((currentColumnIndex-1) / 26)),columnOutput(rem(currentColumnIndex-1, 26) + 1));
    end
    writetable(HedgeResultsAdd,outputFilename,'Sheet',sprintf('Node %d',Node),'Range',sprintf('%s25', currentColumn));
    AInstantShocks = [SetofInstantEquityShocks-1; SetofInstantRateShocks];
    InstantShocks = table;
    InstantShocks.InstantShockScenario = AInstantShocks;
    writetable(InstantShocks,outputFilename,'Sheet',sprintf('Node %d',Node),'Range',sprintf('%s2', currentColumn));
end

%-------------------------------------------------------------------------%
% Hedge payoff profile outputs

CurrentCTE90Baseline= CTE90BaselineFullSet(Node+1,:);
CurrentCTE95Baseline= CTE95BaselineFullSet(Node+1,:);

% Changes outputs if this toggle is on to make the target payoffs the same
% as a baseline scenario
if UseBaselineExpectation == 1
   if Node == 1
       CurrentCTE90Baseline = CTE90BaselineFullSetBaseline;
       CurrentCTE95Baseline = CTE95BaselineFullSetBaseline;
   end
end 

TransformedEquityShocks = BaseSetofEquityShocks - 1;
RequiredAndActualPayouts = [TransformedEquityShocks; BaseSetofRateShocks; CurrentCTE90Baseline; CurrentCTE95Baseline; MasterCTESetOriginal;-PreTaxHedgeNeed;SumHedgePayouts];

if numel(newIndexGrowth) > 0 && ~isnan(newIndexGrowth)
    for i_scenario = 1:numel(newIndexGrowth)
        newIndexGrowthPayout = zeros(5,1);
        newIndexGrowthPayout(1) = newIndexGrowth(i_scenario)-1;
        newIndexGrowthPayout(2) = RateShock_New;
        newIndexGrowthPayout(5) = newCTE95FullSet(i_scenario,Node+1)/10^9;
    end
end
    
HedgeResultsSummary=table;
HedgeResultsSummary.RequiredPayouts = RequiredAndActualPayouts;
startingScenarioColumn = columnOutput(3 + size(PayoutTable, 2));

writetable(HedgeResultsSummary,outputFilename,'Sheet',sprintf('Node %d',Node),'Range',sprintf('%s2',startingScenarioColumn));

% outputs detailed values used to arrive at Pre-Tax Hedge Need
if outputHedgeNeedDetails == 1
    HedgeNeedDetails = table;
    PreTaxCashFlow_allshocks = ones(size(BaseSetofEquityShocks)) * PreTaxCashFlow(Node+1)
    InterestOnExcessCapital_allshocks = ones(size(BaseSetofEquityShocks)) * InterestOnExcessCapital
    DRD_allshocks = ones(size(BaseSetofEquityShocks)) * DRD(Node+1)
    CARVMExpense_allshocks = ones(size(BaseSetofEquityShocks)) * CARVMExpense(Node+1)
    HedgeNeedDetails.RequiredPayouts = [PreTaxCashFlow_allshocks, InterestOnExcessCapital_allshocks, TaxReserveChange, DRD_allshocks, CARVMExpense_allshocks, Haircut];
    writetable(HedgeNeedDetails,outputFilename,'Sheet',sprintf('Node %d',Node),'Range',sprintf('%s15',startingScenarioColumn));
end

if numel(newIndexGrowth) > 0 && ~isnan(newIndexGrowth)
    currentColumnIndex = 4 + size(PayoutTable, 2) + (size(HedgeResults,2)+1) + size(AHedgePayouts,2);
    if currentColumnIndex == rem(currentColumnIndex-1, 26) + 1
        currentColumn = columnOutput(currentColumnIndex);
    else
        currentColumn = sprintf('%s%s', columnOutput(floor((currentColumnIndex-1) / 26)),columnOutput(rem(currentColumnIndex-1, 26) + 1));
    end
    
    newShockPayout = table;
    newShockPayout.IndexGrowthPayout = newIndexGrowthPayout;
    writetable(newShockPayout, outputFilename,'Sheet',sprintf('Node %d',Node),...
        'Range',sprintf('%s2',currentColumn));
end

% output the payoff per instrument in each of the RAS shocks
if outputPayoffPerInstrument == 1
    PayoffBaselineReal = table;
    PayoffBaselineReal.BaselineQuarterly = PayoutBaselineQuarterlyPerInstrument;
    PayoffBaselineReal.BaselineAnnual = PayoutBaselineAnnualPerInstrument;
    PayoffBaselineReal.RealQuarterly = PayoutRealPerInstrumentQuarter;
    PayoffBaselineReal.RealAnnual = PayoutRealPerInstrumentAnnual;
    writetable(PayoffBaselineReal,outputFilename,'Sheet',sprintf('Node %d',Node),'Range','D140');

    PayoffPerInstrumentTable = table;
    PayoffPerInstrumentTable.OptionPayouts = OptionsPayoutAllShocks;
    if instantaneousShock == 1
        PayoffPerInstrumentTable.InstantShock = OptionsPayoutAllShocks_0;
    end
    writetable(PayoffPerInstrumentTable,outputFilename,'Sheet',sprintf('Node %d',Node),'Range',sprintf('%s140',startingScenarioColumn));
    
%     EquityVolsTable = table;
%     EquityVolsTable.EquityVolsITMness = [EquityOptionsVolsAllShocks;...
%         PutITMness1IR10AllShocks; PutITMness1IR5AllShocks; PutITMness2IR10AllShocks; PutITMness2IR5AllShocks; ...
%         CallITMness1IR10AllShocks; CallITMness1IR5AllShocks; CallITMness2IR10AllShocks; CallITMness2IR5AllShocks];
%     writetable(EquityVolsTable, outputFilename, 'Sheet',sprintf('Node %d',Node),'Range',sprintf('%s255',startingScenarioColumn))
end

