function [ output_args ] = runAttribution( varargin )
     
    p = inputParser;
    
    addParameter(p,'fileInput',NaN);
    
    parse(p,varargin{:});
    fileInput = p.Results.fileInput;
    
    if isnan(fileInput)
        [fileName,pathName] =  uigetfile({'*.xlsx;*.xlsm',  'Excel file (*.xlsx, *.xlsm)'; '*.*',...
            'All Files'}, 'Select an attribution parameter input file');
        fileInput = sprintf('%s%s', pathName, fileName);
    end
    
    IndexNumber =xlsread(fileInput,'HedgeAttributionParameters','C2:C2');
    StartingNumber =xlsread(fileInput,'HedgeAttributionParameters','C1:C1');
    [num,str]=xlsread(fileInput,'HedgeAttributionParameters',strcat('C',string(4+StartingNumber),':I',string(IndexNumber+5)));
    
    for index =StartingNumber:IndexNumber
        AttributionNumber = index
        importscenario('fileInput', sprintf('%s%s.xlsx',pathName,[str{index-StartingNumber+1,1}]),'outputFilename',sprintf('%s%s%s','Inputs/', [str{index-StartingNumber+1,1}],'.mat'),'taxRate',num(index-StartingNumber+1,1)); 
        importscenario('fileInput', sprintf('%s%s.xlsx',pathName,[str{index-StartingNumber+1,2}]),'outputFilename',sprintf('%s%s%s','Inputs/', [str{index-StartingNumber+1,2}],'.mat'),'taxRate',num(index-StartingNumber+1,2));
        importvolatilities('fileInput', sprintf('%s%s.xlsm',pathName,[str{index-StartingNumber+1,3}]),'outputFilename',sprintf('%s%s%s','Inputs/', [str{index-StartingNumber+1,3}],'.mat')); 
        importvolatilities('fileInput', sprintf('%s%s.xlsm',pathName,[str{index-StartingNumber+1,4}]),'outputFilename',sprintf('%s%s%s','Inputs/', [str{index-StartingNumber+1,4}],'.mat')); 
        
        [parameters, portfolio] = setparameters('fileInput',sprintf('%s%s',pathName,[str{index-StartingNumber+1,5}]));
        inputsBaseline = load(strcat('Inputs/',[str{index-StartingNumber+1,1}],'.mat'));
        inputsReal = load(strcat('Inputs/',[str{index-StartingNumber+1,2}],'.mat'));
        load(strcat('Inputs/',[str{index-StartingNumber+1,3}],'.mat'))
        RealVols = load(strcat('Inputs/',[str{index-StartingNumber+1,4}],'.mat'));
        runHedgeModel('parameters', parameters, 'portfolio', portfolio, 'inputsBaseline', inputsBaseline, 'inputsReal', inputsReal, 'ImpliedVolsEquities', ImpliedVolsEquities, 'ImpliedVolsRates', ImpliedVolsRates, 'AttributionNumber',AttributionNumber,'RealVols',RealVols);
    end
     
end