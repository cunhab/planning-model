% ---------------------------------runIRC---------------------------------%
% Script #:
% Calls:
% Variables:

% This script runs IRC for a single product
%-------------------------------------------------------------------------%

function[] = runIRC(InForceInputs, NBInputs, varargin)
    %% Load inputs
    p = inputParser;
    formatOut = 'yyyymmdd';
    defaultCurrentDate = datestr(now,formatOut);
    
    addParameter(p, 'standaloneBasis',0)
    addParameter(p, 'fileSuffixProduct', NaN)
    addParameter(p, 'saveCTE', 0)
    addParameter(p, 'accountValueSets', NaN)
    addParameter(p, 'disaggregatedCells', 0)
    addParameter(p, 'baselineInputs', NaN)
    addParameter(p, 'createBaselineInputs', 0)
    addParameter(p, 'ImpliedVolsEquities', NaN)
    addParameter(p, 'ImpliedVolsRates', NaN)
    addParameter(p, 'parameters', NaN)
    addParameter(p, 'portfolio', NaN)
    addParameter(p, 'StressedTemplateInputFile', 'IRC Inputs/DefaultInputs/StressedTemplate');
    addParameter(p, 'CSV_input_file', NaN);
    addParameter(p, 'currentDate', defaultCurrentDate);
    
    parse(p,varargin{:});
    standaloneBasis = p.Results.standaloneBasis;
    fileSuffixProduct = p.Results.fileSuffixProduct;
    saveCTE = p.Results.saveCTE;
    accountValueSets = p.Results.accountValueSets;
    disaggregatedCells = p.Results.disaggregatedCells;
    baselineInputs = p.Results.baselineInputs;
    createBaselineInputs = p.Results.createBaselineInputs;
    ImpliedVolsEquities = p.Results.ImpliedVolsEquities;
    ImpliedVolsRates = p.Results.ImpliedVolsRates;
    parameters = p.Results.parameters;
    portfolio = p.Results.portfolio;
    StressedTemplateInputFile = p.Results.StressedTemplateInputFile;
    CSV_input_file = p.Results.CSV_input_file;
    currentDate = p.Results.currentDate;
    
    if createBaselineInputs == 1
        saveCTE = 1;
    end
    
    if standaloneBasis == 1 && all(isnan(accountValueSets))
        accountValueSets = [2 100];
    elseif all(isnan(accountValueSets))
        accountValueSets = (1:3)*2; % or 10
    end
    
    load(InForceInputs)
    inputsReal = inputScenario;
    currentScenario = inputsReal.scenarioSettings.scenario;

    % New Business (can either be passed an object containing NBInputs, or
    % the NewBusinessByCell table
    if istable(NBInputs)
        NewBusinessByCell = NBInputs;
    else
        load(NBInputs)
    end
    
    % Load  StressedTemplate
    load(StressedTemplateInputFile);
    %% Run Analysis
    
    % current implementation only for 1 product
    selectedProducts = unique(NewBusinessByCell.PolicyName);
    allProducts = struct;
    for i_product = 1:numel(selectedProducts) 
        product = selectedProducts(i_product);
        isShield = contains(product,'Shield');
        rows_currentProduct = strcmp(product, NewBusinessByCell.PolicyName);
        currentProduct_table = NewBusinessByCell(rows_currentProduct,:);
        
        if disaggregatedCells == 0
            NewBusinessGPVADs_weighted = rowfun(@(a,b) {a*b{:}}, currentProduct_table(:,{'Weight','GPVADs'}));
            NewBusinessGPVADs_weighted = NewBusinessGPVADs_weighted{:,:};
            NewBusinessGPVADs_weighted = sum(cat(3,NewBusinessGPVADs_weighted{:}),3);
            
            NewBusinessFinancials_weighted = rowfun(@(a,b) {a*b{:}}, currentProduct_table(:,{'Weight','Financials'}));
            NewBusinessFinancials_weighted = NewBusinessFinancials_weighted{:,:};
            NewBusinessFinancials_weighted = sum(cat(3,NewBusinessFinancials_weighted{:}),3);
            
            NewBusinessNaNs = rowfun(@(a) {any(isnan(a{:}),2)}, currentProduct_table(:,{'GPVADs'}));
            NewBusinessNaNs = NewBusinessNaNs{:,:};
            NewBusinessNaNs = any(cat(3,NewBusinessNaNs{:}),3);
            
            productCells = 0;
        else
            productCells = unique(NewBusinessByCell.PolicyID);
        end
        
        for i_cell = 1:numel(productCells)
            if disaggregatedCells == 1
                currentCell = productCells(i_cell);
                NewBusinessGPVADs_weighted = currentProduct_table.GPVADs{i_cell};
                NewBusinessFinancials_weighted = currentProduct_table.Financials{i_cell};
            end
            NewBusinessFinancials_weighted = array2table(NewBusinessFinancials_weighted);
            NewBusinessFinancials_weighted.Properties.RowNames = inputScenario.inputFinancials.Properties.RowNames(1:size(NewBusinessFinancials_weighted,1));
            NewBusinessFinancials_weighted.Properties.VariableNames = inputScenario.inputFinancials.Properties.VariableNames(1:size(NewBusinessFinancials_weighted,2));
            
            if isShield == 1
                % set CARVM runoff to 0 because CSV changes already taken into
                % account in CTE 95
                NewBusinessFinancials_weighted{'CARVM/Runoff of Surrender Charge (FV - CSV)',:} = 0;
                % read in CSV file
                if isnan(CSV_input_file)
                    [FileName,PathName] =  uigetfile({'*.xlsx;*.xlsm',  'Excel input file (*.xlsx, *.xlsm)'; '*.*',...
                        'All Files'}, 'Select CSV input file');
                    CSV_input_file = sprintf('%s%s', PathName, FileName);
                end
                allShieldCSVs = readtable(CSV_input_file, 'Sheet', 'CSV');
                allShieldCSVs = allShieldCSVs(allShieldCSVs.Scenario == currentScenario,:);
                allShieldCSVs.Properties.VariableNames{'CellNumber'} = 'PolicyID';
                varnames = allShieldCSVs.Properties.VariableNames;
                others = ~ismember(varnames,{'Scenario', 'PolicyID', 'TermLength', 'ShieldSize', 'CapRate', 'ShockSize', 'EquityPerformance'});
                yearCols = varnames(:,others);
                oddCellRows = mod(allShieldCSVs.PolicyID,2) == 1;
                
                if disaggregatedCells == 0
                    % determine the weighting for each CSV cell
                    ShieldCSVsByCell_table = outerjoin(allShieldCSVs(oddCellRows,:), NewBusinessByCell(:,{'PolicyID', 'Weight'}),...
                        'Type', 'left','Keys','PolicyID', 'MergeKeys', true);
                    
                    % weight each equity shock CSV and calculated weighted CSV
                    CSVs_array = table2array(ShieldCSVsByCell_table(:,yearCols));
                    CSVs_array_weighted = CSVs_array .* ShieldCSVsByCell_table{:,'Weight'};
                else
                    ShieldCSVsByCell_table = allShieldCSVs(allShieldCSVs.PolicyID == currentCell, :);
                    CSVs_array_weighted = ShieldCSVsByCell_table(:, yearCols);
                end
                [G, ID] = findgroups(ShieldCSVsByCell_table{:,'ShockSize'});
                ShieldCSVs_weighted = splitapply(@(a) {sum(a)}, CSVs_array_weighted, G);
                ShieldCSVs_weighted = cell2table(ShieldCSVs_weighted);
                ShieldCSVs_weighted.EquityShocks = round(ID + 1,2);
                
                CSVyears = strrep(yearCols, 'x', '');
                CSVyears = str2double(CSVyears);
                NoShockCSVs_Percent = interp1(CSVyears, table2array(ShieldCSVs_weighted(ShieldCSVs_weighted.EquityShocks == 1,1)), 0:max(CSVyears));
                
                if createBaselineInputs ~= 1
                    % get RAS Shock grid and match CSVs to the RAS Shock grid
                    RASGrid = table(parameters.BaseSetofEquityShocks.', parameters.BaseSetofRateShocks.');
                    RASGrid.Properties.VariableNames = {'EquityShocks', 'RateShocks'};
                    [RASGridCSVs, ia] = outerjoin(RASGrid, ShieldCSVs_weighted,...
                        'Type', 'left','Keys','EquityShocks', 'MergeKeys', true);
                    [~, sortinds] = sort(ia);
                    RASGridCSVs = RASGridCSVs(sortinds,:);
                    
                    ShieldCSVs_Percent = interp1(CSVyears, table2array(RASGridCSVs(:,3)).', 0:max(CSVyears));
                    ShieldCSVs_Percent = ShieldCSVs_Percent.';
                end
            end
                       
            allProducts(i_cell + (i_product-1) * numel(productCells)).PolicyName = product;
            allProducts(i_cell + (i_product-1) * numel(productCells)).GPVADs_weighted = NewBusinessGPVADs_weighted;
            allProducts(i_cell + (i_product-1) * numel(productCells)).Financials_weighted = NewBusinessFinancials_weighted;

            
            allProductInputs = struct;
            
            
            % currentProduct_taxAdjustment = taxAdjustment .* table2array(Input_CTE_TaxAdjusted_ordered(:,{'TaxReserves'}));
            
            for i_policy = 1:numel(accountValueSets)
                accountValue = accountValueSets(i_policy);
                % create CTESetImport
                if standaloneBasis == 1
                    [CTE90, CTE95] = calculateCTEs(accountValue * 10^9 / 100000, NewBusinessGPVADs_weighted);
                else
                    [CTE90, CTE95] = calculateCTEs(accountValue * 10^9 / 100000, NewBusinessGPVADs_weighted, InForceGPVADs);
                    CTE90 = -(-CTE90 + currentProduct_taxAdjustment(:,1));
                    CTE95 = -(-CTE95 + currentProduct_taxAdjustment(:,2));
                end
%                 CTESetImportReal = [CTE90 CTE90 CTE95 zeros(size(CTE95)) CTE95];
%                 CTESetImportReal(isnan(CTESetImportReal)) = 0;
                
                StressedValuesTable = StressedTemplate;
                StressedValuesTable.CTELow = CTE90;
                StressedValuesTable.CTEHigh = CTE95;
                StressedValuesTable.SSR = CTE90;
                StressedValuesTable.MCEV = CTE95;
                
                CTELow = unstack(StressedValuesTable(:,{'Time','Shock', 'CTELow'}), 'CTELow', 'Time');
                inputsReal.StressedValues.CTELow = -CTELow{:, 2:end};
                CTEHigh = unstack(StressedValuesTable(:,{'Time','Shock', 'CTEHigh'}), 'CTEHigh', 'Time');
                inputsReal.StressedValues.CTEHigh = -CTEHigh{:, 2:end};
                SSR = unstack(StressedValuesTable(:,{'Time','Shock', 'SSR'}), 'SSR', 'Time');
                inputsReal.StressedValues.SSR = -SSR{:, 2:end};
                MCEV = unstack(StressedValuesTable(:,{'Time','Shock', 'MCEV'}), 'MCEV', 'Time');
                inputsReal.StressedValues.MCEV = -MCEV{:, 2:end};
                inputsReal.StressedValues.NodesFromMilliman = sort(unique(StressedValuesTable.Time));

                % combine Milliman Inputs
                inputsRealFinancials = table2array(NewBusinessFinancials_weighted) * accountValue * 10^9 / 100000;
                if standaloneBasis ~= 1
                    InForceInputs = table2array(inputScenario.inputFinancials);
                    inputsReal_extended = zeros(size(InForceInputs));
                    inputsReal_extended(1:size(inputsRealFinancials,1),1:size(inputsRealFinancials,2)) = inputsRealFinancials;
                    inputsRealFinancials = inputsReal_extended + InForceInputs;
                end
                inputsRealFinancials_temp = inputScenario.inputFinancials;
                inputsRealFinancials_temp{:,:} = inputsRealFinancials;
                inputsReal.inputFinancials = inputsRealFinancials_temp;
                
                % store all input values
                if disaggregatedCells == 1
                    allProductInputs(i_policy + (i_cell - 1) * numel(accountValueSets)).Cell = currentCell;
                end
                
                % calculate actual CSV value from the percents
                if isShield == 1
                    NoShockCSVs = NoShockCSVs_Percent * accountValue;
                    inputsReal.NoShockCSVs = NoShockCSVs;
                    if createBaselineInputs ~= 1
                        ShieldCSVs = ShieldCSVs_Percent * accountValue;
                    end
                else
                    NoShockCSVs = NaN;
                    ShieldCSVs = NaN;
                end
                
                allProductInputs(i_policy + (i_cell - 1) * numel(accountValueSets)).numPolicies = accountValue;
                allProductInputs(i_policy + (i_cell - 1) * numel(accountValueSets)).inputsReal = inputsReal;
 
                if createBaselineInputs ~= 1
                    % load baseline inputs
                    if currentScenario == 1
                        inputsBaseline = inputsReal;
                    else
                        if isnan(baselineInputs)
                            [FileName,PathName] =  uigetfile({'*.mat',  'Matlab input file (*.mat)'},...
                                'Select baseline input file');
                            baselineInputs = sprintf('%s%s', PathName, FileName);
                        end
                        load(baselineInputs);
                        currentBaselineInputs = allProductInputs(arrayfun(@(x) all(x.numPolicies == accountValue), allProductInputs));
                        inputsBaseline = currentBaselineInputs.inputsReal;
                    end
                    
                    if standaloneBasis == 1
                        fileSuffix = sprintf(' %s %dBN Sold Standalone',product{:},accountValue);
                    else
                        fileSuffix = sprintf(' %s %dBN',product{:},accountValue);
                    end
                    if disaggregatedCells == 1
                        fileSuffix = sprintf('%s Cell %d', fileSuffix, currentCell);
                    end
                    if ~isnan(fileSuffixProduct)
                        fileSuffix = strjoin({fileSuffix, fileSuffixProduct});
                    end
                    
                    % do not run Node 1 for S5
                    if currentScenario == 5
                        parameters.NodestoRun = parameters.NodestoRun(parameters.NodestoRun ~= 1);
                    end
                    
                    disp('Running hedge model');
                    runHedgeModel('parameters', parameters, 'portfolio', portfolio, ...
                        'isNewBusiness', true, 'isShield', isShield,...
                        'ShieldCSVs', ShieldCSVs, 'NoShockCSVs', NoShockCSVs,...
                        'inputsReal', inputsReal, 'inputsBaseline', inputsBaseline, ...
                        'ImpliedVolsEquities', ImpliedVolsEquities, 'ImpliedVolsRates', ImpliedVolsRates,...
                        'fileSuffix', fileSuffix, 'accountValue', accountValue, 'InForceInitialCTE', parameters.InForceInitialCTE);
                end
            end
        end
        if saveCTE == 1
            CTE_filename = sprintf('IRC Inputs/%s_S%d_InputSet_%s', currentDate, currentScenario, product{:});
            if ~isnan(fileSuffixProduct)
                CTE_filename = strjoin({CTE_filename, fileSuffixProduct});
            end
            save(CTE_filename, 'allProductInputs');
        end
    end
end