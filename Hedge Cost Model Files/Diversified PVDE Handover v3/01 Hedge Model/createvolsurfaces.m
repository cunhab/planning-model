function [ ImpliedVols, ImpliedVolsMet ] = createvolsurfaces( volInput, volShock, ImpliedVolatilitySpread, varargin)
    %CREATEVOLSURFACES Creates the volatility surface based on inputs.
    %   If loadShockedTermStructure is TRUE, then loads a shocked term
    %   structure and interpolates rest of the points. Note that the
    %   shocked term structure should have the VIX in the first row, and
    %   then the consecutive year volatilities (i.e. 1, 2, 3,...)
    
    p = inputParser;
    
    addParameter(p,'loadShockedTermStructure',0)
    
    parse(p,varargin{:});
    loadShockedTermStructure = p.Results.loadShockedTermStructure;
    
    if loadShockedTermStructure == 1
        ShockedTermStructure = volInput;
    else
        % Repeat for every node
        % Shocked VIX values for -50% equities to 0% equities fr om baseline
        % expectations
        ShockedTermStructure = zeros(length(volInput), numel(volShock));
        
        % Anchored value of 80 for -50% equities and 40 for -25% equities
        
        % make sure volinput first line is the VIX
        
        
        ShockedVIX = interp1([-.5, -.25, 0, .25], [0.8, 0.4, volInput(1), 0.1], volShock);
      
        % Create shocked term structure following formula that relates VIX to other
        % volatility values
        
      
        ShockedTermStructure(1,:) = ShockedVIX;
        for i = 2:size(ShockedTermStructure,1)
            for j = 1:length(ShockedTermStructure)
                ShockedTermStructure(i,j) = (((ShockedVIX(j)^2)/((365*(i-1))/30)-volInput(1)^2/((365*(i-1))/30))*5 + volInput(i)^2)^0.5;
            end
        end
    end
    
    %%
    % Interpolate additional months needed for ATM term structure   
    
    FullATMTermStructure = interp1([1/12, 1:(size(ShockedTermStructure,1)-1)], ShockedTermStructure, 1/12:1/12:((size(ImpliedVolatilitySpread,1)-1)/12));
    %%
    
    % Puts linearly interpolated ATM term structure in each of the shocked
    % volatility surfaces (ATM is column 22)
    ImpliedVols = struct;
    for iShocked = 1:size(FullATMTermStructure,2)
        iImpliedVols = zeros(size(ImpliedVolatilitySpread));
        iImpliedVols(2:end,22) = FullATMTermStructure(:,iShocked);
        iImpliedVols = ApplySpread(iImpliedVols, ImpliedVolatilitySpread);
        ImpliedVols(iShocked).Shock = round(volShock(iShocked) + 1, 14);
        ImpliedVols(iShocked).Surface = iImpliedVols;
        if volShock(iShocked) == 0
            ImpliedVolsMet = iImpliedVols;
        end
    end
end