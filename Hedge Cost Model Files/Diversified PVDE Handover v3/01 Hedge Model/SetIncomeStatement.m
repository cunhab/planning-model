% create structures that hold Milliman inputs and implied IR vols
% MillimanInputs = MillimanInputsReal;
% MillimanInputsStruc = struct;
% MillimanInputsStruc.MillimanInputsReal = MillimanInputsReal;
% MillimanInputsStruc.MillimanInputsBaseline = MillimanInputsBaseline;

% extracts inputs from MillimanInputs and sets up the variables
CTEValue90Base = MillimanInputs(3,:)/1000000000;
CTEValue95Base = MillimanInputs(2,:);
CARVMExpense = MillimanInputs(5,:)/1000000000;

CTEValueBase = CTEValue95Base;
CapitalIncrease = MillimanInputs(4,:)/1000000000;

% what is the point of this?? doesn't this just overwrite capital increase?
for i = 2:numel(CTEValue90Base)
    CapitalIncrease(i) = (CTEValueBase(i) - CTEValueBase(i-1))/1000000000;
end 
PreTaxCashFlow = MillimanInputs(6,:)/1000000000;
DRD = MillimanInputs(7,:)/1000000000;
TaxReserve = MillimanInputs(8,:)/1000000000;
RevenueFees = MillimanInputs(9,:)/1000000000;