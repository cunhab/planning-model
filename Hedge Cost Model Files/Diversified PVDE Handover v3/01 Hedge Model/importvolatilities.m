function [ImpliedVolsEquities, ImpliedVolsRates] = importvolatilities( varargin )
    %IMPORTVOLATILITIES Imports scenario file into matlab variables
        %   imports provided scenario file and turns into matlab variables
        %   for use in the hedge model.
    p = inputParser;
    
    addParameter(p,'fileInput', NaN);
    addParameter(p, 'outputFilename', NaN);
    
    parse(p,varargin{:});
    fileInput = p.Results.fileInput;
    outputFilename = p.Results.outputFilename;
    
    if isnan(fileInput)
        [fileName,pathName] =  uigetfile({'*.xlsx;*.xlsm',  'Excel file (Rate*.xlsx, *.xlsm)'; '*.*',...
            'All Files'}, 'Select volatility input file');
        fileInput = sprintf('%s%s', pathName, fileName);
    end
    
    ImpliedVolatilitySpread = xlsread(fileInput, 'Spread', 'A2:GT86');
    disp('Finished reading Spread')
    ATMVolTermStructureBase_Y = xlsread(fileInput, 'ATM', 'B5:U12');
    ATMVolTermStructureBase_Q = xlsread(fileInput, 'ATM', 'B16:U23');
    disp('Finished reading ATM')
    ShockedTermStructure_Input = xlsread(fileInput, 'ATMShockedTermStructure', 'B2:Q9');
    disp('Finished reading ATMShockedTermStructure')
    
    ImpliedVolsEquities = struct;
    ImpliedVolsEquities.ImpliedVolatilitySpread = ImpliedVolatilitySpread;
    ImpliedVolsEquities.ATMVolTermStructureBase_Y = ATMVolTermStructureBase_Y;
    ImpliedVolsEquities.ATMVolTermStructureBase_Q = ATMVolTermStructureBase_Q;
    ImpliedVolsEquities.ShockedTermStructure_Input = ShockedTermStructure_Input;
    
    RateYears = readtable(fileInput,'Sheet', 'Rates>>');
    ImpliedVolsRates = struct;
    for i = 1:numel(RateYears)
        ImpliedVolsRates(i).Underlying = RateYears.Time(i);
        sheetName = sprintf('%dYear', RateYears.Time(i));
        ImpliedVolsRates(i).Surface = xlsread(fileInput, sheetName);
    end
    
    if isnan(outputFilename)
        outputFilename = inputdlg('Enter output file name:',...
             'Output File Name', [1 50]);
    end
    if iscell(outputFilename)
        outputname = outputFilename{:};
    else
        outputname = outputFilename;
    end
    save(outputname, 'ImpliedVolsEquities', 'ImpliedVolsRates');
end

