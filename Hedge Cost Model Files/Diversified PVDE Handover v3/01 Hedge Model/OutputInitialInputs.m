%-------------------------------------------------------------------------%
% Scenario inputs outputs

% turn Stressed Values into single matrix
CTEImportBaseline = table;
CTEImportBaseline.EquityShocks = repelem(inputsBaseline.StressedValues.EquityShocksMilliman, numel(inputsBaseline.StressedValues.NodesFromMilliman));
CTEImportBaseline.RateShocks = repelem(inputsBaseline.StressedValues.InterestRateShocksMilliman, numel(inputsBaseline.StressedValues.NodesFromMilliman));
CTEImportBaseline.Node = repmat(inputsBaseline.StressedValues.NodesFromMilliman,[numel(inputsBaseline.StressedValues.EquityShocksMilliman) 1]);
CTEImportBaseline.SSR = reshape(inputsBaseline.StressedValues.SSR',[],1);
CTEImportBaseline.CTELow = reshape(inputsBaseline.StressedValues.CTELow',[],1);
CTEImportBaseline.CTEHigh = reshape(inputsBaseline.StressedValues.CTEHigh',[],1);
CTEImportBaseline.MCEV = reshape(inputsBaseline.StressedValues.MCEV',[],1);

CTEImportReal = table;
CTEImportReal.EquityShocks = repelem(inputsReal.StressedValues.EquityShocksMilliman, numel(inputsReal.StressedValues.NodesFromMilliman));
CTEImportReal.RateShocks = repelem(inputsReal.StressedValues.InterestRateShocksMilliman, numel(inputsReal.StressedValues.NodesFromMilliman));
CTEImportReal.Node = repmat(inputsReal.StressedValues.NodesFromMilliman,[numel(inputsReal.StressedValues.EquityShocksMilliman) 1]);
CTEImportReal.SSR = reshape(inputsReal.StressedValues.SSR',[],1);
CTEImportReal.CTELow = reshape(inputsReal.StressedValues.CTELow',[],1);
CTEImportReal.CTEHigh = reshape(inputsReal.StressedValues.CTEHigh',[],1);
CTEImportReal.MCEV = reshape(inputsReal.StressedValues.MCEV',[],1);

writetable(CTEImportBaseline,outputFilename,'Sheet','CTESetImportBaseline','WriteVariableNames',true,'WriteRowNames',false);
writetable(CTEImportReal,outputFilename,'Sheet','CTESetImportReal','WriteVariableNames',true,'WriteRowNames',false);

MillimanBaseline_Table = array2table(MillimanInputsBaseline);
writetable(MillimanBaseline_Table,outputFilename,'Sheet','MillimanInputsBaseline','Range','B1','WriteVariableNames',false,'WriteRowNames',false);
MillimanReal_Table = array2table(MillimanInputsReal);
writetable(MillimanReal_Table,outputFilename,'Sheet','MillimanInputsReal','Range','B1','WriteVariableNames',false,'WriteRowNames',false);

OuterLoopBaseline_Table = array2table(OuterLoopBaseline);
writetable(OuterLoopBaseline_Table,outputFilename,'Sheet','OuterLoopBaseline','Range','A2','WriteVariableNames',false,'WriteRowNames',false);
OuterLoopReal_Table = array2table(OuterLoopReal);
writetable(OuterLoopReal_Table,outputFilename,'Sheet','OuterLoopReal','Range','A2','WriteVariableNames',false,'WriteRowNames',false);

if newBusiness == 1
    writetable(inputsReal.inputFinancials,outputFilename,'Sheet','HedgeModelInputsReal', 'Range', 'B2', 'WriteVariableNames', false, 'WriteRowNames', false);
    writetable(inputsBaseline.inputFinancials,outputFilename,'Sheet','HedgeModelInputsBaseline', 'Range', 'B2', 'WriteVariableNames', false, 'WriteRowNames', false);
    if isShield == 1
        NoShockCSVs_table = array2table(NoShockCSVs * 10^9);
        writetable(NoShockCSVs_table,outputFilename,'Sheet','HedgeModelInputsReal', 'Range', 'B14', 'WriteVariableNames', false, 'WriteRowNames', false);
        NoShockCSVsBaseline_table = array2table(inputsBaseline.NoShockCSVs * 10^9);
        writetable(NoShockCSVsBaseline_table,outputFilename,'Sheet','HedgeModelInputsBaseline', 'Range', 'B14', 'WriteVariableNames', false, 'WriteRowNames', false);
    end
elseif currentScenario == 5
    writetable(inputsReal.inputFinancials,outputFilename,'Sheet','HedgeModelInputsReal', 'Range', 'B2', 'WriteVariableNames', false, 'WriteRowNames', false);
    writetable(inputsBaseline.inputFinancials,outputFilename,'Sheet','HedgeModelInputsBaseline', 'Range', 'B2', 'WriteVariableNames', false, 'WriteRowNames', false);
end
xlswrite(outputFilename, NodestoRun, 'Parameters', 'B2');
if(OptimizerA == 1)
    xlswrite(outputFilename, ['A'], 'Parameters', 'B3');
elseif(OptimizerB == 1)
    xlswrite(outputFilename, ['B'], 'Parameters', 'B3');
elseif(OptimizerC == 1)
    xlswrite(outputFilename, ['C'], 'Parameters', 'B3');
elseif(OptimizerD == 1)
    xlswrite(outputFilename, ['D'], 'Parameters', 'B3');
end
parameters_matrix1 = [DeductibleSchedule; PercentTARReleaseToSell; 
    PercentIRShockTARReleaseToSell; Grading; RebalancedNodes];
parameters_matrix2 = [BSRateVols; priceOnIndex; loadShockedTermStructure; KeepCapitalBuffer; CapitalBuffer; 
    T0Buffer; CapitalRollforward; NoDividends; noInterestOnCapital; SpecifyStartingFunding;
    StartingFunding; SellIRUpside; PositiveTAROnly; RestrictEquityShocks; 
    FloorBearMarket; MCEVHedging; MCEVLink; AddPAD; VolSensitivity; PADMin; 
    PADMax; VolMultiplier; HaircutCashflows; UseBaselineExpectation; UseRescaledShocks; 
    ChangeATMVols; PinchPayout; CreateBedrock; BedrockPercent];

if newBusiness == 1
    parameters_matrix2 = [parameters_matrix2; accountValue*10^9; InForceInitialCTE];
end

xlswrite(outputFilename, parameters_matrix1, 'Parameters', 'B5');
xlswrite(outputFilename, parameters.AdditionalExpense * 10^9, 'Parameters', 'B10');
xlswrite(outputFilename, parameters_matrix2, 'Parameters', 'B11');
