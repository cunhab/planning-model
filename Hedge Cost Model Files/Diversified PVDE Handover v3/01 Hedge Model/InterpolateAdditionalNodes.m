% ---------------------------InterpolateAdditionalNodes-------------------%
% Script #: 2.2.2.2
% Calls: N/A
% Variables: CombinedMCEVSetAllNodes, CombinedSSRAllNodes,
% CombinedMCEVSetAllNodes, SSRBaselineGrid, CombinedCTEBaselineShock used
% in IndividualOuterLoopRun and CalculateRequiredPayoffAdjustment.
% CTE90FullSet and CTE95FullSet used in the output in
% CreateResultsMacroHedge
% 
% This script interpolates between the already calculated nodes to reach a
% full, 20 node set of
%   - Target fundings across all shocks, graded to CTE 95
%   - SSR across all shocks
%   - MCEV across all shocks
%   - SSR across the outer loop
%   - Target funding in a baseline scenario
%   - Target funding in a "zero growth" scenario
%   - CTE 90 in a baseline scenario
%   - CTE 90 in all shocks
%   - CTE 95 in all shocks

%%

% Interpolates the graded grid
CombinedCTESetAllNodes95 = interp1(NodesFromMilliman,CombinedCTESetAllNodes95(NodesFromMilliman+1,:), 0:maxNodetoRun,'linear');

% Interpolates between nodes for SSR in all shocks
CombinedSSRAllNodes = interp1(NodesFromMilliman,CombinedSSRAllNodes(NodesFromMilliman+1,:), 0:maxNodetoRun,'linear');

% MCEV grid
CombinedMCEVSetAllNodes = interp1(NodesFromMilliman,CombinedMCEVSetAllNodes(NodesFromMilliman+1,:), 0:maxNodetoRun,'linear');

% Interpolates between grids for CTE 70 along the outer loop (one row)
SSRBaselineGrid = interp1(NodesFromMilliman,SSRBaselineGrid(NodesFromMilliman+1), 0:maxNodetoRun,'linear');

% Interpolates the baseline shock (for other outer loops that need it for
% upside definition)
CombinedCTEBaselineShock = interp1(NodesFromMilliman,CombinedCTEBaselineShock(NodesFromMilliman+1), 0:maxNodetoRun,'linear');

% Target funding given zero equity growth
CombinedCTENoShock = interp1(NodesFromMilliman,CombinedCTENoShock(NodesFromMilliman+1), 0:maxNodetoRun,'linear');

% Interpolates baseline shock at 90 (used for S2)
CombinedCTEBaselineShock90 = interp1(NodesFromMilliman,CombinedCTEBaselineShock90(NodesFromMilliman+1), 0:maxNodetoRun,'linear');

% Interpolates CTE 90 across all shocks
CTE90BaselineFullSet = interp1(NodesFromMilliman,CTE90BaselineFullSetTemp, 0:maxNodetoRun,'linear');

% Interpolates CTE 95 across all shocks
CTE95BaselineFullSet = interp1(NodesFromMilliman,CTE95BaselineFullSetTemp, 0:maxNodetoRun,'linear');

% Divides all post-interpolated grids to convert into $BN
CTE90BaselineFullSet = CTE90BaselineFullSet/1000000000;
CTE95BaselineFullSet = CTE95BaselineFullSet/1000000000;


