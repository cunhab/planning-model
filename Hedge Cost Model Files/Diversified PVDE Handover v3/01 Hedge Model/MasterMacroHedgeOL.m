% -------------------MasterMacroHedgeOL----------------------------------%
% Script #: 2.4.2
% Calls: T0SscenarioSpreads (2.4.2.1), CalculateBaselineHedgeLoss (2.4.2.2)
% CalculateRealHedgeLoss (2.4.3.3), GlobalOptimizer (2.4.2.4)
% Variables: rates and equities in the outer loop and in a baseline scenario
% used in pricing scripts, CombinedUpsidePayoffs representing the
% per-instrumnet payoffs in all scenarios used in optimization, and
% SumPayoutReal representing the hedge cost used in the capital roll
% forward (accounting for rebalancing). PayoutBaselineAnnual,
% PayoutBaselineQuarterly, PayoutRealAnnual and PayoutRealQuarterly are
% output in the Excel in CreateResultsMacroHedge.
% 
% This script 
%   - Assigns rates and equities at the beginning of the year to Matlab
%   variables
%   - Calls function to price all hedge instruments in all risk appetite
%   shocks
%   - Calls function to price all hedge instrumnets in a baseline scenario
%   - Calls function to price all hedge instrumnets at the end of the year
%   - Calls function to run solver algorithm
%   - Estimates hedge cost of selected hedge portfolio, accounting for
%   rebalancing
%

%%
        
NodePerRebalance = RebalanceMonths / NodeMonths;
StepsPerNode_R = NodeMonths / inputsReal.scenario.monthsPerTimestep;
StepsPerNode_B = NodeMonths / inputsBaseline.scenario.monthsPerTimestep;
    
% 1. Establish a set of Starting Data based on t0 Macro Economic Scenario

% for Form 10 attribution (removing theta)
if RebalanceMonths == 0 && Node == 1
    StartingIndex=OuterLoopBaseline(Node+1,2); %S&P in baseline
else
    StartingIndex=OuterLoopReal(Node+1,2); %S&P in real outer loop
end
StartingIndexBaseline=OuterLoopBaseline(Node+1,2); %S&P in baseline outer loop
StartingDate=datenum(2016,3,31); 
StartingDate=addtodate(StartingDate,NodeMonths * (Node - 1),'month');
StartingRates = OuterLoopReal(Node+1,3:10); % 1, 2, 3, 4, 5, 7, 10 and 20Y treasury rates
StartingRates = StartingRates'; 
ExpectedEquityReturns = OuterLoopReal(Node+1,19:25); % 1 through 7 year equity expectations
StartingSwapRates = OuterLoopReal(Node+1, 26:33)'; % 1, 2, 3, 4, 5, 7, 10 and 20 year spot swap rates
StartingSwapRatesQuarterly = OuterLoopRealSwaps(1 + (Node-1) * StepsPerNode_B, :)'; % 1-20 year spot swap rates

% Rates following one year forward path 
RatesDelay = OuterLoopReal(Node+1,13:20)'; % Treasury forwards
SwapRatesDelay = OuterLoopReal(Node+1, 34:41)'; % Swap forwards

% Treasury and swap spot rates the following year if the outer loop had returned to baseline
% Estimated using the change in the baseline outer loop.
RatesDelayBaseline = StartingRates + (OuterLoopBaseline(Node+2,3:10)'-OuterLoopBaseline(Node+1,3:10)');
SwapRatesProjectionBaseline = StartingSwapRates + (OuterLoopBaseline(Node+2,26:33)'-OuterLoopBaseline(Node+1, 26:33)');
SwapRatesProjectionBaselineQuarterly = StartingSwapRatesQuarterly +...
    (OuterLoopBaselineSwaps(1 + (Node) * StepsPerNode_B,:)' - OuterLoopBaselineSwaps(1 + (Node-1) * StepsPerNode_B,:)');

% Special formatting accounting for node 0
if Node == 0 
    RatesDelay = OuterLoopReal(Node+2,13:20)';
    SwapRatesDelay = OuterLoopReal(Node+2,34:41)';
    
    RatesDelayBaseline = StartingRates + (OuterLoopBaseline(Node+2,13:20)'-OuterLoopBaseline(Node+2,3:10)');
    SwapRatesProjectionBaseline = StartingSwapRates + (OuterLoopBaseline(Node+3,26:33)'-OuterLoopBaseline(Node+2, 26:33)');
end

% doesn't seem to be used? first line is replacement code for second line
% SwapRatesQuarterDelay_2 = interp1([0 1],[StartingSwapRate SwapRatesDelay], RebalanceMonths);
% SwapRatesQuarterDelay = (SwapRatesDelay-StartingSwapRates)*NodePerRebalance + StartingSwapRates; % Linear interpolation for the swap forwards one quarter in (3 month expectation)

StartingIndexDelay = OuterLoopReal(Node+2, 2); % Next node's S&P level in real outer loop
StartingIndexDelayBaseline = OuterLoopBaseline(Node + 2, 2); % Next node's S&P level in baseline
IndexGrowth = StartingIndexDelay/StartingIndex; % Equity growth in real outer loop
IndexGrowthBaseline = StartingIndexDelayBaseline/StartingIndexBaseline; % Equity growth in baseline

% Transformation of equity and rate shocks to be relative to baseline
SetofEquityShocks = BaseSetofEquityShocks*(IndexGrowthBaseline);
SetofRateShocks = BaseSetofRateShocks; % transformed in getinstrumentpayoffs

% Sets the Equity shock in the cost scenario as the growth in the real
% outer loop
CostEquityShock = IndexGrowth;

% Sets equity shock if the outer loop were to begin following baseline as
% the growth in the baseline
CostEquityShockBaseline = IndexGrowthBaseline;

% Sets the real purchase cost of a future based on % of starting
% index--defaulted to zero
FutureCostPercentage = 0; 
FutureCost=FutureCostPercentage*StartingIndex;

% Funding is equal to the master funding value that comes from the outer
%loop file
Funding = MasterFunding;

% Sets TestingEquitiesTheta to zero which toggles as to whether or not 
% the model reprices 1 year into the future 
TestingEquitiesTheta=0;

% Used as an indicator in SetImpliedVols
RunningTestCycle = 0;
%%
%----------------------------- Pricing -----------------------------------%
% 2. Do initial instrument costing, determine how much each macroeconomic shock 
% would cause each instrument to payoff, and calculate baseline and real
% hedge payoff per instrument

% Instrument payoffs in risk appetite shocks
getinstrumentpayoffs;

% instrument payoffs in given index growth scenarios
if numel(newIndexGrowth) > 0 && ~isnan(newIndexGrowth)
    IndexGrowthScenarioPayout = zeros(size(OptionsCosts,1), numel(newIndexGrowth));
    IndexGrowthScenarioPayout_Q = zeros(size(OptionsCosts,1), numel(newIndexGrowth));
    IndexGrowthCTEs = zeros(1, numel(newIndexGrowth));
    for i_scenario = 1:numel(newIndexGrowth)
        IndexGrowthBaseline_New = newIndexGrowth(i_scenario);
        CalculateHedgeLoss_ChangeIndexGrowth;
        IndexGrowthScenarioPayout(:,i_scenario) = PayoutBaselineAnnualPerInstrument;
        IndexGrowthScenarioPayout_Q(:,i_scenario) = PayoutBaselineQuarterlyPerInstrument;
        IndexGrowthCTEs(i_scenario) = newCTE95;
    end
end

BaselineTestPayout = PayoutBaselineAnnualPerInstrument;
BaselineTestPayout_Q = PayoutBaselineQuarterlyPerInstrument;

% Payoff of different instruments in the "baseline" scenario, and
CombinedUpsidePayoffs = BaselineTestPayout;

% The upside payoffs for the other upside equity shocks are added onto it
% to be passed onto the optimizer. 
CombinedUpsidePayoffsNoBase = [];
PositiveTARRelease = [];
SelectedRateShocks = [];
for i = 1: numel(BaseSetofEquityShocks)
%     if BaseSetofEquityShocks(i) > 1.25
%         SelectedRateShocks = [SelectedRateShocks, BaseSetofRateShocks(i)];
%         PositiveTARRelease = [PositiveTARRelease, 6];
%         CombinedUpsidePayoffs = [CombinedUpsidePayoffs, OptionsPayoutAllShocks(:,i)];
%         CombinedUpsidePayoffsNoBase = [CombinedUpsidePayoffsNoBase, OptionsPayoutAllShocks(:,i)];
%     else
    if PositiveTAROnly == 1 && RestrictEquityShocks == 1
        if TARRelease(i) > 0 && BaseSetofEquityShocks(i) > 1
            SelectedRateShocks = [SelectedRateShocks, BaseSetofRateShocks(i)];
            PositiveTARRelease = [PositiveTARRelease, TARRelease(i)];
            CombinedUpsidePayoffs = [CombinedUpsidePayoffs, OptionsPayoutAllShocks(:,i)];
            CombinedUpsidePayoffsNoBase = [CombinedUpsidePayoffsNoBase, OptionsPayoutAllShocks(:,i)];
        end
    elseif PositiveTAROnly == 1 && RestrictEquityShocks == 0
        if TARRelease(i) > 0
            SelectedRateShocks = [SelectedRateShocks, BaseSetofRateShocks(i)];
            PositiveTARRelease = [PositiveTARRelease, TARRelease(i)];
            CombinedUpsidePayoffs = [CombinedUpsidePayoffs, OptionsPayoutAllShocks(:,i)];
            CombinedUpsidePayoffsNoBase = [CombinedUpsidePayoffsNoBase, OptionsPayoutAllShocks(:,i)];
        end
    elseif SellIRUpside == 1 
        if BaseSetofEquityShocks(i) > 1 && BaseSetofRateShocks(i) == 0
          CombinedUpsidePayoffs = [CombinedUpsidePayoffs, OptionsPayoutAllShocks(:,i)];
          CombinedUpsidePayoffsNoBase = [CombinedUpsidePayoffsNoBase, OptionsPayoutAllShocks(:,i)];
        end 
    else
        if BaseSetofEquityShocks(i) > 1 
          CombinedUpsidePayoffs = [CombinedUpsidePayoffs, OptionsPayoutAllShocks(:,i)];
          CombinedUpsidePayoffsNoBase = [CombinedUpsidePayoffsNoBase, OptionsPayoutAllShocks(:,i)];
        end  
    end
%     end 
end 
%%
% ------------------------- Optimization---------------------------------%
% 3. Optimize the proper amount of each instrument to purchase such that upside 
% sold is minimized, but payout exceeds required hedge payoffs
GlobalOptimizer;
%%
%-------------------------- Payoffs --------------------------------------%
% 4. Determine hedge cost in the actual outer loop and if the outer loop
% returned to baseline both quarterly and annually. If quarterly, an
% annualized cost is calculated

% ---------------------Payoffs in real outer loop ------------------------%
% Hedge asset gain / loss of modeled hedge portfolio, accounting for
% rebalancin

if optimizeToNewBaseline == 1
    PayoutRealAnnual = IndexGrowthScenarioPayout.*PurchaseAmounts';
    PayoutRealQuarter = IndexGrowthScenarioPayout_Q.*PurchaseAmounts';
else
    PayoutRealAnnual = PayoutRealPerInstrumentAnnual.*PurchaseAmounts';
    PayoutRealQuarter = PayoutRealPerInstrumentQuarter.*PurchaseAmounts';
end

if RebalanceQuarterly == 0
    SumPayoutReal = sum(PayoutRealAnnual);
else 
    % RebalanceQuarter == 1 -- quarterly cost used for 1 year instruments.
    % Needs to be aggregated so that it flows through on the capital roll
    % forward
    
    % instruments to use quarterly costs
    InstrumentQuarter = (ismember(portfolio.allInstruments.Instrument, {'Put', 'Call'})...
        & portfolio.allInstruments.Term == 1) |  ...
        (ismember(portfolio.allInstruments.Instrument, {'Receiver swaption',...
        'Payer swaption', 'Swap', 'Put spread', 'Call spread', 'Receiver swaption spread',...
        'Payer swaption spread'}));
    
    SumPayoutReal = sum(PayoutRealAnnual(~InstrumentQuarter));
    SumPayoutReal = SumPayoutReal + sum(PayoutRealQuarter(InstrumentQuarter) / NodePerRebalance);
end

% ------------------------ Baseline outer loop payout ------------------ %
PayoutBaselineAnnual = PayoutBaselineAnnualPerInstrument.*PurchaseAmounts';
% SumPayoutBaselineAnnual = sum(PayoutBaselineAnnual);

PayoutBaselineQuarterly = PayoutBaselineQuarterlyPerInstrument.*PurchaseAmounts';
% SumPayoutBaselineQuarterly = sum(PayoutBaselineQuarterly);

if numel(newIndexGrowth) > 0 && ~isnan(newIndexGrowth)
    PayoutIndexGrowthAnnual = IndexGrowthScenarioPayout.*PurchaseAmounts';
    PayoutIndexGrowthQuarterly = IndexGrowthScenarioPayout_Q.*PurchaseAmounts';
end


 
