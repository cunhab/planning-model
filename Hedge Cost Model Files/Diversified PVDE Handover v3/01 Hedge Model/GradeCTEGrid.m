% ----------------------------------GradeCTEGrid--------------------------%
% Script #: 2.2.2.2
% Calls: N/A
% Variables: GradedGrid is returned to InterpolateGridSquares as the target
% statutory funding for all shocks
%
% This script grades the target funding levels in shocks, from 50% between
% CTE 90 and CTE 95 in the most severe stresses to CTE 95 
% in all scenarios that perform at or better than baseline
%
% ----------------------------------------------------------------------- %

function [GradedGrid] = GradeCTEGrid(parameters, Node, CTESet_high, CTESet_low, BaseSetofEquityShocks)
Grading = parameters.Grading;
GradingStart = parameters.GradingStart;
GradingStart_Shock = parameters.GradingStart_Shock;
GradingEnd = parameters.GradingEnd;
GradingEnd_Shock = parameters.GradingEnd_Shock;

if Node > 0 && Grading(Node) == 2
    CTESet_low = CTESet_high - 1*10^9;
end

% The difference between high (CTE 95) and low (CTE 90)
CTEDifference = CTESet_high - CTESet_low;

% Grading factors begin at 50% CTE 90, 50% CTE 95 at a -50% equity shock, then linearly
% decrease % of CTE 90 until a 0% equity shock is 100% CTE 95
GradingFactors = zeros(1, numel(BaseSetofEquityShocks));

% Sets all grading factors equal to 1 so there is no relaxation under CTE
% 95 for any shocks (user parameter set in Dashboard)
% 0 to keep target statutory funding level equal to CTE 95 for a node across all stresses
% 1 to relax target statutory funding by grading between CTE 90 and 95 based on the severity of the stress, 
% i.e. if it's a -25% shock, grade to CTE90 + 0.75 * (CTE95 - CTE90)
% 2 to relax target statutory funding by grading between CTE 95 and 1 billion below CTE 95 (-50% equity shock or worse)
% 3 to relax target statutory funding by grading from CTE 95 starting at GradingStart_Shock to CTE 90 at GradingEnd_Shock (expressed as 0.75 for a -25% shock)
% GradingStart and GradingEnd is the amount to grade from and to
% i.e. if GradingEnd = 0.25 and GradingStart = 1, grades from between CTE90 + 0.25 * (CTE95 - CTE90) and CTE95

if Node > 0 && Grading(Node) == 0
    GradingFactors = ones(1,numel(BaseSetofEquityShocks));
elseif Node > 0 && Grading(Node) == 1 % otherwise sets grading factors
    for i = 1:numel(GradingFactors)
        GradingFactors(i) = BaseSetofEquityShocks(i); % shocks are expressed between 0.5 and 1.25
        if BaseSetofEquityShocks(i) >= 1
            GradingFactors(i) = 1;
        end
    end
elseif Node > 0 && Grading(Node) == 2
    for i = 1:numel(GradingFactors)
        if BaseSetofEquityShocks(i) >= 1
            GradingFactors(i) = 1;
        elseif BaseSetofEquityShocks(i) < 0.5
            GradingFactors(i) = 0;
        else
            GradingFactors(i) = 2 * BaseSetofEquityShocks(i) - 1;
        end
    end
elseif Node > 0 && Grading(Node) == 3
    for i = 1:numel(GradingFactors)
        if BaseSetofEquityShocks(i) <= GradingEnd_Shock 
            GradingFactors(i) = GradingEnd;
        elseif BaseSetofEquityShocks(i) <= GradingStart_Shock 
            m = (GradingStart - GradingEnd) / (GradingStart_Shock - GradingEnd_Shock);
            b = GradingStart - GradingStart_Shock * m;
            GradingFactors(i) = m * BaseSetofEquityShocks(i) + b;
        else
            GradingFactors(i) = GradingStart;
        end
    end
end

GradedGrid = zeros(1,numel(BaseSetofEquityShocks));

% Grades grid according to grading factors
for i = 1: numel(GradedGrid)
    GradedGrid(i) = CTESet_low(i) + CTEDifference(i)*GradingFactors(i);
end;
