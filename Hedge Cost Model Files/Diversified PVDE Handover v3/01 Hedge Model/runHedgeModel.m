function [ output_args ] = runHedgeModel( varargin )
    %runHedgeModel Runs the hedge model and creates an excel output file
    %   Used to interface with IRC model and to run hedge model.
    %
    %   Sample code to run:
    %   [parameters, portfolio] = setparameters(); % you will be prompted to select the parameters input file
    %   inputsReal = load('Inputs/S1_201909_1106.mat'); % real  scenario e.g. S1,S4,S5
    %   inputsBaseline = load('Inputs/S1_201909_1106.mat'); % baseline scenario, always set to scenario 1
    %   load('Inputs/Volatilities_201903.mat');
    %   runHedgeModel('parameters', parameters, 'portfolio', portfolio, 'inputsBaseline', inputsBaseline, 'inputsReal', inputsReal, 'ImpliedVolsEquities', ImpliedVolsEquities, 'ImpliedVolsRates', ImpliedVolsRates);
    %
    %   If you want to be able to run many different deductibles, like for
    %   the form 10 calculations, run the code with this configuration 
    %   (change deductibleSet to desired deductibles to run):
    %   runHedgeModel('runDeductibles', true, 'deductibleSet', [0,1,2,3], 'parameters', parameters, 'portfolio', portfolio, 'inputsBaseline', inputsBaseline, 'inputsReal', inputsReal, 'ImpliedVolsEquities', ImpliedVolsEquities, 'ImpliedVolsRates', ImpliedVolsRates);

    p = inputParser;
    
    addParameter(p,'parameters', NaN); % if parameters & portfolio fed, overrides parameter value in inputs file
    addParameter(p,'portfolio', NaN); 
    addParameter(p,'isShield', false); % turn to true if running IRC for shield
    addParameter(p,'isNewBusiness', false); % turn to true if running IRC
    addParameter(p, 'ShieldCSVs', NaN); % feed a matrix of CSVs under shocks if running IRC for shield
    addParameter(p, 'NoShockCSVs', NaN); % feed an array of CSVs under baseline if running IRC for shield
    addParameter(p, 'accountValue', NaN);
    addParameter(p, 'InForceInitialCTE', NaN);
    addParameter(p,'runDeductibles', false); % if true, sets deductibles to deductibleSet, otherwise runs deductibles as defined in parameters
    addParameter(p, 'deductibleSet', [1]); % set of deductibles to run
    addParameter(p, 'inputsBaseline', NaN); % if not fed in, will be prompted to select baseline scenario input file
    addParameter(p, 'inputsReal', NaN); % if not fed in, will be prompted to select real scenario input file
    addParameter(p, 'MillimanInputsBaseline', NaN); % if fed in, this will override the values in the inputsBaseline file
    addParameter(p, 'MillimanInputsReal', NaN); % if fed in, this will override the values in the inputsReal file
    addParameter(p, 'ImpliedVolsEquities', NaN); % if not fed in, will be prompted to select file containing desired ImpliedVolsEquities
    addParameter(p, 'ImpliedVolsRates', NaN); % if not fed in, will be prompted to select file containing desired ImpliedVolsRates
    addParameter(p, 'outputFilename', NaN); % if fed in, overrides automatic output filename 
    addParameter(p, 'controlPanel', NaN); % excel sheet that contains all the files to load as inputs
    addParameter(p, 'fileSuffix', ''); % if fed in, adds a suffix to the output file
    addParameter(p, 'AttributionNumber', NaN)
    addParameter(p, 'RealVols',  NaN)
    
    parse(p,varargin{:});
    parameters = p.Results.parameters;
    portfolio = p.Results.portfolio;
    isShield = p.Results.isShield;
    newBusiness = p.Results.isNewBusiness;
    ShieldCSVs = p.Results.ShieldCSVs;
    NoShockCSVs = p.Results.NoShockCSVs;
    accountValue = p.Results.accountValue;
    InForceInitialCTE = p.Results.InForceInitialCTE;
    runDeductibles = p.Results.runDeductibles;
    deductibleSet = p.Results.deductibleSet;
    inputsBaseline = p.Results.inputsBaseline;
    inputsReal = p.Results.inputsReal;
    MillimanInputsBaseline = p.Results.MillimanInputsBaseline;
    MillimanInputsReal = p.Results.MillimanInputsReal;
    ImpliedVolsEquities = p.Results.ImpliedVolsEquities;
    ImpliedVolsRates = p.Results.ImpliedVolsRates;
    outputFilename = p.Results.outputFilename;
    controlPanel = p.Results.controlPanel;
    fileSuffix = p.Results.fileSuffix;
    AttributionNumber = p.Results.AttributionNumber
    RealVols = p.Results.RealVols
    
    
    if ~isnan(controlPanel)
        [~,varString,varAll]=xlsread(controlPanel);
        for iVariable = 1:size(varString)
            varValue = varAll{iVariable, 2};
            if strcmp(varAll{iVariable,1}, 'inputsBaseline')
                inputsBaseline = load(varValue);
            elseif strcmp(varAll{iVariable,1}, 'inputsReal')
                inputsReal = load(varValue);
            end
            load(varValue);
        end
    end
    
    if ~isstruct(parameters) || ~isa(portfolio, 'HedgePortfolio')
        [parameters, portfolio] = setparameters();
    end
    
    if ~isstruct(ImpliedVolsEquities) || ~isstruct(ImpliedVolsRates)
        prompt = {'Enter volatility input file name:'};
        answer = inputdlg(prompt,'Inputs', [1 50]);
        load(answer{1});
    end
    ImpliedVolatilitySpread  = ImpliedVolsEquities.ImpliedVolatilitySpread ;
    ATMVolTermStructureBase_Y  = ImpliedVolsEquities.ATMVolTermStructureBase_Y ;
    ATMVolTermStructureBase_Q  = ImpliedVolsEquities.ATMVolTermStructureBase_Q ;
    ShockedTermStructure_Input  = ImpliedVolsEquities.ShockedTermStructure_Input ;

    if ~isstruct(inputsBaseline) && ~isstruct(inputsReal)
        prompt = {'Enter baseline input file name:', 'Enter real input file name:'};
        answer = inputdlg(prompt,'Inputs', [1 50]);
        inputsBaseline = load(answer{1});
        inputsReal = load(answer{2});
    elseif ~isstruct(inputsBaseline)
        prompt = {'Enter baseline input file name:'};
        answer = inputdlg(prompt,'Inputs', [1 50]);
        inputsBaseline = load(answer{1});
    elseif ~isstruct(inputsReal)
        prompt = {'Enter real input file name:'};
        answer = inputdlg(prompt,'Inputs', [1 50]);
        inputsReal = load(answer{1});
    end
    currentScenario = inputsReal.scenarioSettings.scenario;

    createParameterVariables;

    if useRealVols == 1
        if isnan(AttributionNumber)==0
             if ~isstruct(RealVols) %hca added 6-4-2018 to eliminate prompt if already present
             prompt = {'Enter real volatility input file name:'};
             answer = inputdlg(prompt,'Inputs', [1 50]);
             RealVols = load(answer{1});
             end %hca added 6-4-2018
        end
        RealImpliedVolsEquities = RealVols.ImpliedVolsEquities;
        RealImpliedVolsRates = RealVols.ImpliedVolsRates;
    end
    
    if hasDynamicHedge == 1
        [dynamicFileName,dynamicPathName] =  uigetfile({'*.xlsx;*.xlsm',  'Excel file (*.xlsx, *.xlsm)'; '*.*',...
            'All Files'}, 'Select dynamic hedge input file');
        fileInput_dynamic = sprintf('%s%s', dynamicPathName, dynamicFileName);
        inputDynamicHedge = readtable(fileInput_dynamic);
        dynamicHedgePayoff = unstack(inputDynamicHedge, 'DynamicHedgePayoff', 'Time');
        
        % Order shocks in the same order BaseSetofEquityShocks and BaseSetofRateShocks
        RASGrid = table(parameters.BaseSetofEquityShocks.', parameters.BaseSetofRateShocks.');
        RASGrid.Properties.VariableNames = {'EquityShocks', 'RateShocks'};
        [RASGridHedgePayoffs, ia] = outerjoin(RASGrid, dynamicHedgePayoff,...
            'Type', 'left','Keys',{'EquityShocks','RateShocks'}, 'MergeKeys', true);
        [~, sortinds] = sort(ia);
        RASGridHedgePayoffs = RASGridHedgePayoffs(sortinds,:);
        
        dynamicHedgePayoff_bn = RASGridHedgePayoffs{:,3:end} / 10^9;
    end

    if aboveCSV == 2
    	CARVM_runoff_Real = inputsReal.inputFinancials{'CARVM/Runoff of Surrender Charge (FV - CSV)',1} - inputsReal.inputFinancials{'CARVM/Runoff of Surrender Charge (FV - CSV)',:};
		CARVM_runoff_Baseline = inputsBaseline.inputFinancials{'CARVM/Runoff of Surrender Charge (FV - CSV)',1} - inputsBaseline.inputFinancials{'CARVM/Runoff of Surrender Charge (FV - CSV)',:};
		
		inputsReal.StressedValues.CTE70 = inputsReal.StressedValues.CTE70 + CARVM_runoff_Real(inputsReal.StressedValues.NodesFromMilliman + 1);
		inputsBaseline.StressedValues.CTE70 = inputsBaseline.StressedValues.CTE70 + CARVM_runoff_Baseline(inputsBaseline.StressedValues.NodesFromMilliman + 1);
	
		inputsReal.StressedValues.CTELow = inputsReal.StressedValues.CTELow + CARVM_runoff_Real(inputsReal.StressedValues.NodesFromMilliman + 1);
		inputsBaseline.StressedValues.CTELow = inputsBaseline.StressedValues.CTELow + CARVM_runoff_Baseline(inputsBaseline.StressedValues.NodesFromMilliman + 1);
		
		inputsReal.StressedValues.CTEHigh = inputsReal.StressedValues.CTEHigh + CARVM_runoff_Real(inputsReal.StressedValues.NodesFromMilliman + 1);
		inputsBaseline.StressedValues.CTEHigh = inputsBaseline.StressedValues.CTEHigh + CARVM_runoff_Baseline(inputsBaseline.StressedValues.NodesFromMilliman + 1);
	end

    for iDeductible = deductibleSet
        if runDeductibles == 1
            % runDeductibles runs all deductibles in deductibleSet, and
            % sets the capital buffer equal to the current deductible value
            parameters.DeductibleSchedule = ones(1,numel(parameters.DeductibleSchedule))*iDeductible;
            DeductibleSchedule = parameters.DeductibleSchedule;
            parameters.CapitalBuffer = iDeductible;
            CapitalBuffer = parameters.CapitalBuffer;
        end
        
        % if outputFilename is left empty, automatically generate file name
        if any(isnan(outputFilename)) || runDeductibles == true
            formatOut = 'yyyymmdd';
            currentDate = datestr(now,formatOut);
            if currentScenario == 5
                fileext = 'xlsm';
            else
                fileext = 'xlsx';
            end
            if isnan(AttributionNumber) ==0 
               outputFilename =sprintf('Outputs/%s S%d %0.0f%% %0.0f Dct%s%s Attribution #%d%s%s%s.%s',...
               currentDate, currentScenario, parameters.PercentTARReleaseToSell(1)*100, parameters.DeductibleSchedule(5),...
               Setting_AddPAD(parameters.AddPAD+1), Setting_rollover(parameters.CapitalRollforward+1),AttributionNumber,...
               Setting_grading(parameters.Grading(1)+1), ...
               Setting_priceIndex(parameters.priceOnIndex + 1), fileSuffix, fileext);
            else
                outputFilename = sprintf('Outputs/%s S%d %0.0f%% %0.0f Dct%s%s%s%s%s.%s',...
                    currentDate, currentScenario, parameters.PercentTARReleaseToSell(1)*100, parameters.DeductibleSchedule(5),...
                    Setting_AddPAD(parameters.AddPAD+1), Setting_rollover(parameters.CapitalRollforward+1),...
                    Setting_grading(parameters.Grading(1)+1), ...
                    Setting_priceIndex(parameters.priceOnIndex + 1), fileSuffix, fileext);
            end
        end
        
        if newBusiness == 1 && isShield == 1 && currentScenario == 5
            templateFile = 'Templates/Template_NewBusinessForm10_DRDNotAdjusted_S5_50Y.xlsx';
        elseif newBusiness == 1 && currentScenario == 5
            templateFile = 'Templates/Template_NewBusinessForm10_DRDAdjusted_S5_50Y.xlsx';
        elseif newBusiness == 1 && isShield == 1
            templateFile = 'Templates/Template_NewBusinessForm10_DRDNotAdjusted_50Y.xlsx';
        elseif newBusiness == 1
            templateFile = 'Templates/Template_NewBusinessForm10_DRDAdjusted_50Y.xlsx';
        elseif outputStrikes == 1
            if includeCover == 1 && numel(newIndexGrowth) && ~isnan(newIndexGrowth) > 0
                templateFile = 'Templates/HedgeTemplate_Strikes_Cover_Scenarios.xlsx';
            elseif outputPayoffPerInstrument == 1
                if currentScenario == 5
                    if outputAttribution ==1
                        templateFile ='Templates/Template_Strikes_Cover_Payoffs_Attribution_S5.xlsm';
                    else
                        templateFile = 'Templates/Template_Strikes_Cover_Payoffs_S5.xlsm';
                    end
                else
                   if outputAttribution ==1
                        templateFile = 'Templates/Template_Strikes_Cover_Payoffs_Attribution.xlsx';
                   else
                        templateFile = 'Templates/Template_Strikes_Cover_Payoffs.xlsx';
                   end
                end
            elseif instantaneousShock == 1 && capUpsideLosses == 1
                templateFile = 'Templates/HedgeTemplate_Strikes_Cover_InstantShocks_CapUpside.xlsx';
            elseif instantaneousShock == 1
                templateFile = 'Templates/HedgeTemplate_Strikes_Cover_InstantShocks.xlsx';
            else
                if currentScenario == 5
                    templateFile = 'Templates/Template_Strikes_Cover_S5.xlsm';
                else
                    templateFile = 'Templates/Template_Strikes_Cover.xlsx';
                end
            end
        else
            if currentScenario == 5
                templateFile = 'Templates/Template_Cover_S5.xlsm';
            else
                templateFile = 'Templates/Template_Cover.xlsx';
            end
        end
        copyfile(templateFile, outputFilename); % copy the template file
        
        
        maxNodetoRun = max(parameters.NodestoRun);
        NodesFromMilliman = inputsBaseline.StressedValues.NodesFromMilliman; 
        maxNode = max(NodesFromMilliman);
        if maxNodetoRun > maxNode
            error('Node being run is after years projected by Milliman. Pick an earlier year.')
        end
        NodesFromMilliman = NodesFromMilliman(NodesFromMilliman<=maxNodetoRun);
        notNaN = ~isnan(inputsBaseline.StressedValues.CTELow(1,:));
        inputsBaseline.CTE90_allTimes = interp1(inputsBaseline.StressedValues.NodesFromMilliman(notNaN), inputsBaseline.StressedValues.CTELow(1, notNaN), 0:maxNode);
        notNaN = ~isnan(inputsBaseline.StressedValues.CTEHigh(1,:));
        inputsBaseline.CTE95_allTimes = interp1(inputsBaseline.StressedValues.NodesFromMilliman(notNaN), inputsBaseline.StressedValues.CTEHigh(1, notNaN), 0:maxNode);
        notNaN = ~isnan(inputsBaseline.StressedValues.SSR(1,:));
        inputsBaseline.SSR_baseline = interp1(inputsBaseline.StressedValues.NodesFromMilliman(notNaN), inputsBaseline.StressedValues.SSR(1,notNaN), 0:maxNode);
        
        notNaN = ~isnan(inputsReal.StressedValues.CTELow(1,:));
        inputsReal.CTE90_allTimes = interp1(inputsReal.StressedValues.NodesFromMilliman(notNaN), inputsReal.StressedValues.CTELow(1, notNaN), 0:maxNode);
        notNaN = ~isnan(inputsReal.StressedValues.CTEHigh(1,:));
        inputsReal.CTE95_allTimes = interp1(inputsReal.StressedValues.NodesFromMilliman(notNaN), inputsReal.StressedValues.CTEHigh(1, notNaN), 0:maxNode);
        notNaN = ~isnan(inputsReal.StressedValues.SSR(1,:));
        inputsReal.SSR_baseline = interp1(inputsReal.StressedValues.NodesFromMilliman(notNaN), inputsReal.StressedValues.SSR(1,notNaN), 0:maxNode);

        inputsBaseline.scenario = Scenario(inputsBaseline.scenarioSettings,...
            'isPriceIndex', parameters.priceOnIndex, ...
            'PriceIndexAdjustment', parameters.PriceIndexAdjustment);
        inputsReal.scenario = Scenario(inputsReal.scenarioSettings,...
            'isPriceIndex', parameters.priceOnIndex, ...
            'PriceIndexAdjustment', parameters.PriceIndexAdjustment);
        
        OuterLoopReal = createOuterLoop(inputsReal.scenario, NodeMonths);
        OuterLoopBaseline = createOuterLoop(inputsBaseline.scenario, NodeMonths);
        OuterLoopRealSwaps = inputsReal.scenario.swapRates{:,:};
        OuterLoopBaselineSwaps = inputsBaseline.scenario.swapRates{:,:};
        if isnan(MillimanInputsBaseline)
            MillimanInputsBaseline = createMillimanInputs(inputsBaseline, OuterLoopBaseline, parameters);
        end
        if isnan(MillimanInputsReal)
            MillimanInputsReal = createMillimanInputs(inputsReal, OuterLoopReal, parameters);
        end
        
        OutputInitialInputs;
        
        % create structures that hold Milliman inputs and implied IR vols
        MillimanInputsStruc = struct;
        MillimanInputsStruc.MillimanInputsReal = MillimanInputsReal;
        MillimanInputsStruc.MillimanInputsBaseline = MillimanInputsBaseline;
        
        tic;
        % Sets statutory financials imports to Matlab variables
        MillimanInputs = MillimanInputsReal;
        SetIncomeStatement;
        
        % Adjusts program settings to account for selected options above
        AdjustSettings;
    
        % Interpolates the different CTE levels for all shocks specified, given the
        % limited CTE levels and shocks provided % check how material this is for
        % interpolating between nodes
        DetermineCalculatedCTEs;
        
        % Calculates the TAR release that occurs in upside scenarios (and
        % determines the target funding level according to user parameters)
        CalculateUpside;
        
        % Runs hedge optimization
        IndividualOuterLoopRun;
        
        disp('Run complete');
        exitflags
        toc
    end
    
end

% creates the OuterLoop format (from old hedge model templates)
function [OuterLoop] = createOuterLoop(currentScenario, NodeMonths)
    nYears = (size(currentScenario.equityRates, 1) - 1) / currentScenario.timestepPerYear;
    nTimesteps = size(currentScenario.equityRates, 1) - 1;
    OuterLoop = zeros(nTimesteps * (currentScenario.monthsPerTimestep / NodeMonths) + 1, 45);
    rateYears = [1,2,3,4,5,7,10,20];
    stepYears = [0, 0:NodeMonths/12:nYears];
    stepYears = stepYears(1:end-1);
    OuterLoop(:,1) = 0:(size(OuterLoop,1)-1);
    OuterLoop(:,2) = interp1(0:(nYears-1), currentScenario.annualIndex(2:end), stepYears).';
    for iMaturity = 1:numel(rateYears)
        OuterLoop(:, 2 + iMaturity) = currentScenario.getTreasuryRate(stepYears, 0, rateYears(iMaturity));
        OuterLoop(:, 10 + iMaturity) = currentScenario.getTreasuryRate(stepYears, 1, rateYears(iMaturity));
        OuterLoop(:, 25 + iMaturity) = currentScenario.getSwapRate(stepYears, 0, rateYears(iMaturity));
        OuterLoop(:, 33 + iMaturity) = currentScenario.getSwapRate(stepYears, 1, rateYears(iMaturity));
    end
    SwapDelay = [1,5];
    SwapMaturity = [5,10];
    for iMaturity = 1:length(SwapDelay)
        for iDelay = 1:(length(SwapMaturity))
            OuterLoop(:, 41 + ((iMaturity - 1) * numel(SwapMaturity)) + iDelay) = currentScenario.getSwapRate(stepYears, SwapDelay(iDelay), SwapMaturity(iMaturity));
        end
    end
    OuterLoop(:, 18 + (1:7)) = cumprod(exp(currentScenario.swapRates{stepYears * currentScenario.timestepPerYear + 1, 1:7}) / (1 - currentScenario.PriceIndexAdjustment), 2);
end

% creates the Milliman inputs template (from old hedge model inputs)

function [MillimanInputs] = createMillimanInputs(scenarioInputs, OuterLoop, parameters) 
    inputFinancials = scenarioInputs.inputFinancials;
    CTE90 = scenarioInputs.CTE90_allTimes;
    CTE95 = scenarioInputs.CTE95_allTimes;
    SSR = scenarioInputs.SSR_baseline;
    
%     lastYear = max(parameters.NodestoRun) + 1;
    lastYear = min([numel(CTE90), numel(CTE95), numel(SSR), size(inputFinancials,2)]);
    MillimanInputs = zeros(9, lastYear);
    MillimanInputs(1,:) = 0:(lastYear - 1);
    MillimanInputs(2,:) = CTE95(1:lastYear);
    MillimanInputs(3,:) = CTE90(1:lastYear);
    MillimanInputs(4,2:end) = MillimanInputs(2,2:end) - MillimanInputs(2,1:end-1); %Target funding increase

    AdditionalExpense = zeros(1, size(inputFinancials,2));
    AdditionalExpense(1:(numel(parameters.AdditionalExpense)+1)) = [0, parameters.AdditionalExpense * 10^9];
    AdditionalExpense = array2table(AdditionalExpense, 'RowNames', {'Additional expense'}, 'VariableNames', inputFinancials.Properties.VariableNames);
    inputFinancials = [inputFinancials; AdditionalExpense];
    
    % CARVM expense
    CARVMExpense = inputFinancials{'CARVM/Runoff of Surrender Charge (FV - CSV)',1:lastYear-1} - inputFinancials{'CARVM/Runoff of Surrender Charge (FV - CSV)',2:lastYear};
    MillimanInputs(5,2:end) = CARVMExpense;

    % Pre-tax cashflows
    Income = sum(inputFinancials{{'Revenue Fees', 'Per Policy Charge', 'Surrender Charge', 'Rider Fee'},1:lastYear});
    Expenses = sum(inputFinancials{{'Expenses', 'Commissions', 'Guarantee Rider Claims (Net)', 'Additional expense'},1:lastYear});

    TreasuryRates_10Yr = OuterLoop(2:end, 9).';
    
    if parameters.aboveCSV == 2
    	CARVM_runoff = inputFinancials{'CARVM/Runoff of Surrender Charge (FV - CSV)',1} - inputFinancials{'CARVM/Runoff of Surrender Charge (FV - CSV)',:};
    	CTE90 = CTE90 - CARVM_runoff;
    end
    Reserves = max([CTE90(1:lastYear); SSR(1:lastYear)]);
    InvIncOnReserves = (.01+TreasuryRates_10Yr(1:lastYear-1)) .* ...
        (Reserves(1:lastYear-1) - inputFinancials{'CARVM/Runoff of Surrender Charge (FV - CSV)',1:lastYear-1});
    MillimanInputs(6,2:end) = Income(2:end) - Expenses(2:end) + InvIncOnReserves;

    MillimanInputs(7,2:end) = inputFinancials{'DRD',2:lastYear};
    MillimanInputs(8,:) = inputFinancials{'Tax Reserves in Excess of CSV',1:lastYear};
    MillimanInputs(9,2:end) = inputFinancials{'Revenue Fees',2:lastYear};
end