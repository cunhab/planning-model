function [ parameters, portfolio ] = setparameters( varargin )
    %setparameters Reads the parameters from file and sets their values
    %   Reads all parameters from provided file and creates a parameters
    %   structure to hold all parameters
    %   parameters = setparameters('fileInput', 'C:\Users\Jean.Wang\Documents\Brighthouse\7 IRC\HedgeModelInputs\HedgeModelParameterTemplate.xlsx')
    p = inputParser;
    
    addParameter(p,'fileInput', NaN);
    
    parse(p,varargin{:});
    fileInput = p.Results.fileInput;
    
    if isnan(fileInput)
        [fileName,pathName] =  uigetfile({'*.xlsx;*.xlsm',  'Excel file (*.xlsx, *.xlsm)'; '*.*',...
            'All Files'}, 'Select parameters input file');
        fileInput = sprintf('%s%s', pathName, fileName);
    end
    
    [varNum,varString,varAll]=xlsread(fileInput, 'Parameters');
    
    for iVariable = 1:size(varString)
        varValue = varAll{iVariable, 2};
        if ischar(varValue) || isstring(varValue)
            varValue = str2num(varAll{iVariable, 2});
        end
        parameters.(varAll{iVariable,1})=varValue;
    end
    
    portfolio = HedgePortfolio(fileInput);
    
    disp('Finished setting parameters');
end

