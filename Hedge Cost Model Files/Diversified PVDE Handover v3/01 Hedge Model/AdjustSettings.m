% ---------------------------- AdjustSettings ----------------------------%
% % Script #: 2.1
% Calls: N/A
% Variables: BaseSetofEquityShocks, BaseSetofRateShocks represent all
% shocks within the risk appetite tested by the model. EquityShocksBase and
% InterestRateShocksBase represent all shocks used as dependent variables
% in the regression in RunRegression 
%
% This script generates Matlab variables needed to run the remaining
% scripts
%   
%-------------------------------------------------------------------------%

% First node to be run
Node = NodestoRun(1);

% Optimizer variables required, determines the # of starting guesses for
% the optimizer and the number of iterations it goes from those starting
% points
% OptimizerLoops=100; 
GlobalMaxFunctionEvaluations = 3000*5; 
GlobalMaxIterations = 1000*5;

% if UseBaselineExpectation == 1
%     if Node == 1
% %         CTEValueBase(2) = MillimanInputsBaseline(1, 2); % need CTE97 from baseline for adverse, then what expected baseline
% %         if GradeTo95 == 1
%             CTEValueBase(2) = MillimanInputsBaseline(2, 2);
% %             CapitalIncrease(1) = 0;
%             for i = 2:numel(CapitalIncrease)
%                 CapitalIncrease(i) = (CTEValueBase(i) - CTEValueBase(i-1))/1000000000;
%             end 
% %         end 
%     end 
% end 


% Increases max evaluations for optimizers B and C
if OptimizerB == 1 || OptimizerC == 1
    GlobalMaxFunctionEvaluations = 3000*20000;
    GlobalMaxIterations = 1000*20000;
else
    GlobalMaxFunctionEvaluations = 3000*5; 
    GlobalMaxIterations = 1000*5;
%     GlobalMaxFunctionEvaluations = 3000*20;
%     GlobalMaxIterations = 1000*20;
end 

% Sets implied volatility for the first node that is run (for the
% alternative method)
if ChangeATMVols == 0
    [ImpliedVols, ImpliedVolsMet] = createvolsurfaces(ShockedTermStructure_Input, -0.5:0.05:0.25, ImpliedVolatilitySpread, 'loadShockedTermStructure', 1);
    ImpliedVolsCurrent = ImpliedVolsMet;
    ImpliedVolsLastYear = ImpliedVolsCurrent; % PROBABLY CAN DELETE THIS LINE, NOT USED
elseif ChangeATMVols == 1
elseif ChangeATMVols == 2
else
end

if useRealVols == 1
    [ImpliedVols_Real, ImpliedVolsMet_Real] = createvolsurfaces(RealImpliedVolsEquities.ShockedTermStructure_Input, -0.5:0.05:0.25, ImpliedVolatilitySpread, 'loadShockedTermStructure', 1);
end 
