%----------------------------DefaultUserParameters------------------------%
% Script #: 

% This script sets the default user paramaters.

%-------------------------------------------------------------------------%
% 0. Inputs from Milliman
NodesFromMilliman = [0, 1, 2, 3, 5, 7, 10, 15, 20];

% 1. Determine target hedge payoff profile

% Sets the deductible / maximum unhedged loss above CTE 95 used to reduce
% target hedge gains. Each level is associated for one year (e.g.
% deductible of $1bn in years 1 and 2) 
DeductibleSchedule = [1, 1, 2, 2.5, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3];

% Requires that a capital buffer above CTE 95 is kept. The size of the 
% capital buffer is equal to total funding less CTE 95, less the deductible
KeepCapitalBuffer = 1; 

% 1 to apply MCEV overlay to target hedge gains, i.e. target funding must
% equal the maximum of the target statutory funding and the target economic
% funding, 0 to consider statutory protection only
MCEVHedging = 1; 

% Percent change of MCEV that should be protected by the target economic funding
% (0.4 = 40%) S2 = 0.25, all other scenarios = 0.4
MCEVLink = 0.4; 

% 1 to set the model to express the equity and rate shock of a risk 
% appetite stress as relative to baseline expectations
UseRescaledShocks = 1; 

% 0 to keep target statutory funding level equal to CTE 95 for a node across all
% stresses; 1 to relax target statutory funding to the midpoint between CTE
% 90 and CTE 95 in the most severe shocks; 2 to relax target statutory
% funding to 1 billion below CTE 95 in most severe shocks
Grading = zeros(1,20);

% Percentage to haircut revenue fees in risk appetite shocks
% (1 = no haircut, 0.7 = 30% of the equity shock haircut, e.g. if the shock is
% -25% in equities, the haircut would be 0.3*0.25, or 7.5% )
HaircutCashflows = 0.6;

% Only restrict the percent of upside to sell in positive TARs
PositiveTAROnly = 1;

% Define upside (bull scenario) to be only equity shocks > 0%
RestrictEquityShocks = 1;

% Cap the selling of upside in positive TARs up to equities +50%
capUpsideLosses = 0;

% 1 to disallow selling of upside in bear markets, by flooring hedge needs at 0
FloorBearMarket = 1;

% do not include interestOnExcessCapital in calculations of hedge need
noInterestOnCapital = 0; % NOT FULLY TESTED

% 1 to use the year 1 target hedge gains calculated using target funding and 
% cash flows from a baseline scenario; 0 to use target funding and cash 
% flows from the "real" capital markets scenario
UseBaselineExpectation = 1; % use for not S1--always have to run once with this = 0, otherwise will throw error

% 2. Define and price hedge instruments across risk appetite

% 1 to assume that implied volatility surfaces change over time, 0 to assume that
% they remain constant to the values provided in the imported implied
% volatility surfaces (alternative method).
ChangeATMVols = 1;

% Increases the implied volatility surface by the specified percentage 
% The impact of this volatility sensitivity on hedge cost is multiplied by
% the vol multiplier and limited by PADMin and PADMax.
AddPAD = 1; % 1 to generate hedge costs calculated with a equity vol sensitivity, 0 otherwise
AddPADRates = 1; % 1 to generate hedge costs calculated with a rate vol sensitivity, 0 otherwise
VolSensitivity = .02; % Increase to the volatility surface at purchase (.02 = 2%) 
PADMin = 1.2; % Minimum multiplier to hedge cost, post volatility sensitivity
PADMax = 1.45; % Maximum multiplier to hedge cost, post volatlity senstivity
VolMultiplier = 2; % Multiplier to the percent increase in premium outlay applied to hedge costs

% 3. Select hedge instruments approximating target hedge payoff profile

% Choose optimizer for run. Optimizer D is the default optimizer described 
% in the user guide. A is flat hedge cost in up scenarios. B minimizes %
% TAR release sold in up scenarios from baseline, and C minimizes % TAR release
% sold in up scenarios from a "no shock" value. Only one optimizer can be
% selected
OptimizerD = 1;
OptimizerA = 0;
OptimizerB = 0;
OptimizerC = 0;

% Optimize to quarterly hedge costs

optimizeQuarterly = 0;

% For Optimizer D, the % of TAR Release, defined as 
% (upside hedge cost - baseline hedge)/(upside target funding - baseline target funding) 
% that can be used to absorb hedge losses, for each node (0.3 = 30%), for
% each model year
PercentTARReleaseToSell = ones(1, 20) * .3;

% if 1, allows for selling more of the upside in IR shocks
SellIRUpside = 1;
PercentIRShockTARReleaseToSell = PercentTARReleaseToSell;

% 1 sets the model to require that payoff in the -50, -100 bps scenario is
% exactly equal to the target hedge gain
PinchPayout = 0;

% 1 to create a "bedrock" of an 80% 5yr put that must payoff at least the bedrock
% percent specified of the target hedge gain of the most severe
% (-50% equity shock, -100 bps rates) shock
CreateBedrock = 1; 
BedrockPercent = 0.25; 

% Use and price forward 5yr and 10yr swaps (2 years forward) 
ForwardSwap = 0;

% 1 is long swaps only (no swaptions/swaption spreads), 2 is long payer
% swaptions and swaptions spreads only, 3 is swaps and swaptions and
% swaption spreads, 4 is swaps/swaptions/call/futures; 5 is no linear
% instruments (swaps / futures)
% 6 is linear equity instruments only; 7 is linear rates onlys
instrumentsAllowed = 4; % 4 is default setting

if instrumentsAllowed == 6 || instrumentsAllowed == 8
    CreateBedrock = 0;
end

no5yrSwaptions = 1;

% For other instrument level inclusions and exclusions within the solver algorithm: 
% see the script SetHedgeInstruments 

% 1 to limits notional of all 5y puts other than the bedrock put to 0, 
% 1yr puts to 20bn, and 1yr call spreads to 40bn. 0 to keep the values set
% in SetHedgeInstruments and the bedrock established with CreateBedrock /
% BedrockPercent
LimitNotional = 1; 

% 4. Roll forward to next year

% Nodes that should be run, separate by commas. E.g. to run nodes 1-5,
% format should be [1, 2, 3, 4, 5];
NodestoRun = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10];
if UseBaselineExpectation == 0
    NodestoRun = [1];
end

% Sets the capital buffer in $bn held by BHF in excess of CTE 95 at the beginning
% of year 1. Funding in excess of CTE 95 plus the capital buffer are dividended
CapitalBuffer = 3;

% 1 to specify a different level of funding than CTE 95 + capital buffer to
% the StartingFunding amount (in $bn)
SpecifyStartingFunding = 0;
StartingFunding = 17.269411859; % perfect hedging for scenario 5 + 2bn

% 1 if rolled-forward hedge cost of the node should reflect rebalancing, 0
% if it should not
% First number represents node 1
RebalancedNodes = [1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1];

% 1 to conduct a capital roll forward, 0 to assume that beginning of year
% funding is equal to previous year's target funding plus the capital
% buffer (e.g. CTE 97 + $3BN)
CapitalRollforward = 0;

% 1 to set the model to not dividend when funding is greater than CTE 95 + 
% the capital buffer
NoDividends = 0;

% 5. Output format

% output strikes to excel sheet
outputStrikes = 1;

% include cover sheet
includeCover = 1;

% output payoff per instrument
outputPayoffPerInstrument = 0;
%outputAttribution = 0

Setting_AddPAD = [string(''), string(' AddPAD')];
Setting_rollover = [string(' no rollforward'), string(' rollforward')];
Setting_grading = [string(''), string(' CTE90Grading'), string(' Grading')];
Setting_instruments = [string(''), string(' SwapsOnly'), string(' SwaptionsOnly'),...
    string(' Swaps&Swaptions'), string(' allInstruments'), string(' noLinearInstruments'),...
    string(' LinearEquitiesAllRate'), string(' AllEquitiesLinearRate'), string(' allLinear')];
Setting_priceIndex = [string(' TotalReturn'), string('')];
Setting_FlattenVols = [string(''), string('FlattenVols')];

% 6. Parameters for calculating the Form10 table / rate shocks
instantaneousShock = 0;
useBaselineImpliedVols = 0;
SetofInstantEquityShocks = [.6, .75, .9, .95, 1.05, 1.10, 1.25, 1.40, 1, 1];
SetofInstantRateShocks = [0 0 0 0 0 0 0 0 -.01 .01];

RateShock_New = 0;
optimizeToNewBaseline = 0;
newIndexGrowth = [];
newIndexGrowthAbsolute = 1;

%7. Select which version of model code to use

% modelVersion 0 is the new model sans Swaps
% modelVersion 1 is the old model
% modelVersion 2 is the new Equities scripts, with new ITMness calc using
% the original PriceSwaptions script - available for editing

modelVersion = 0;

% only in use if useOldSwaptionITMness = 1
% interpolates the rate vols (allows for seeing impact of
% just rate interpolation using old swaption ITMness calcs)
useInterpolatedRateVols = 1; 

if modelVersion == 0
    useOldEquityOptionsPricing = 0;
    useOldSwaptionsPricing = 0;
    useOldSwaptionITMness = 0;
    dividendQuarterly = 0; % dividendQuarterly uses the cash version instead of the continuous version
    
    % new swap pricing not fully debugged
    testEquityOptionsDiff = 1;
    testSwaptionClassDiff = 1;
    
    % new swap pricing not fully debugged
    testSwapDifference = 0;
    useOldSwapsPricing = 1;
    useContinuousSwaps = 0;
elseif modelVersion == 1
    useOldEquityOptionsPricing = 1;
    useOldSwaptionsPricing = 1;
    useOldSwaptionITMness = 1;
    dividendQuarterly = 1; % dividendQuarterly uses the cash version instead of the continuous version
    
    testEquityOptionsDiff = 0;
    testSwaptionClassDiff = 0;

    % new swap pricing not fully debugged
    testSwapDifference = 0;
    useOldSwapsPricing = 1;
    useContinuousSwaps = 0;
else
    useOldEquityOptionsPricing = 0;
    useOldSwaptionsPricing = 1;
    useOldSwaptionITMness = 0;
    dividendQuarterly = 0; % dividendQuarterly uses the cash version instead of the continuous version
    
    testEquityOptionsDiff = 1;
    testSwaptionClassDiff = 0;
    
    % new swap pricing not fully debugged
    testSwapDifference = 0;
    useOldSwapsPricing = 1;
    useContinuousSwaps = 0;
end

if testEquityOptionsDiff == 1
    OptionDiff = [];
end

if testSwaptionClassDiff == 1
    SwaptionDiff = [];
    SwaptionVolPricesAll = []; % currently code is commented out
end

if testSwapDifference == 1
    LegRateDiff = [];
    SwapPrice_SpecDiff = [];
    SwapPriceNoCFsDiff = [];
    SwapCFsDiff= [];
end

% select which inputs to use
useOldFFactor = 0;

% End of user parameters
%-------------------------------------------------------------------------%
