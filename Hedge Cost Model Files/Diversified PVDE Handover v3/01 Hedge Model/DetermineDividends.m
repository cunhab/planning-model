%------------------------------DetermineDividends-------------------------%
% Dividends out end-of-year funding greater than the target CTE level plus
% the deductible amount.

%-------------------------------------------------------------------------%
%%
% Determines the amount by which end-of-year funding exceeds the target CTE level plus the deductible 
% AboveTargetFunding = MasterFunding+SumPayoutReal+AdjustedDistributableEarnings(Node+1)+CapitalIncrease(Node+1) - (CTEValueBase(Node+1)/1000000000 + Deductible);
% MasterFundingBeforeDividends = MasterFunding;
% 
% % If it exceeds the target amount, funding is reduced to the target
% % level...
% % if AboveTargetFunding > 0    
% %     if Node ~= 0
% %         MasterFunding = CTEValueBase(Node)/1000000000+Deductible;
% %     end 
% % end 
% 
% %  ...and the excess amount is dividended out
% if AboveTargetFunding < 0 || Node == 0
%     Dividends(Node+1) = 0;
% else  
%     Dividends(Node+1) = AboveTargetFunding;
% end

EoPFundingOriginal = EoPFunding;

if NoDividends == 1
    Dividends(Node+1) = 0;
elseif EoPFunding > CTEValueBase(Node+1)/1000000000 + CapitalBuffer;
    EoPFunding = CTEValueBase(Node+1)/1000000000 + CapitalBuffer;
    Dividends(Node+1) = EoPFundingOriginal - EoPFunding;
end 

