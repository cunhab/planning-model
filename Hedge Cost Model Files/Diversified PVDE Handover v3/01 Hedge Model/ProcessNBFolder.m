% -----------------------------processNBFolder----------------------------%
% This script processes all the selected NB files. When you run this script
% a box will pop up and you should select all the files you wish to
% process.

% Make sure that the NB file is named such that the relevant scenario 
% (i.e. S4) is within the parentheses in the files name 
% i.e. Flex IRC (60-40 PGS Level S5).xlsx. If you want to process all 
% the scenarios in the file, leave out the scenario number 
% i.e. Flex IRC (60-40 PGS Level).xlsx.
% Also, make sure your S1 is your baseline.

% The output file will be of the format: IRC Inputs/[TODAYSDATE]_NewBusiness_S1_[PRODUCTNAME] [PRODUCT SUFFIX]'
% For example: IRC Inputs/20170614_NewBusiness_S1_MetLife FlexChoice (60-40 PGS)
%-------------------------------------------------------------------------%

function [] = ProcessNBFolder(varargin)
    p = inputParser;
    
    addParameter(p,'inputFiles',NaN)
    addParameter(p,'processFiles',1) % set processFiles to 2 if processing for a file failed, and you want to process using temp file (saves time)
    addParameter(p,'startT0', 1); %  financials inputs start with a T0 column (note: set to 0 if using VASB outputs)
    parse(p,varargin{:});
    inputFiles = p.Results.inputFiles;
    processFiles = p.Results.processFiles;
    startT0 = p.Results.startT0;
    
    if isnan(inputFiles)
        [FileName,PathName] =  uigetfile({'*.xlsx;*.xlsm',  'Excel input file (*.xlsx, *.xlsm)'; '*.*',...
            'All Files'}, 'Select processed VASB input file', 'MultiSelect', 'on');
        inputFiles = fullfile(PathName, FileName);
        if ~iscell(inputFiles)
            inputFiles = {inputFiles};
        end
    end
        
    formatOut = 'yyyymmdd';
    currentDate = datestr(now,formatOut);
    
    for i = 1:numel(inputFiles)
        [pathstr, NB_filename, ext] = fileparts(inputFiles{i});
        start_identifier = strfind(NB_filename, '(');
        end_identifier = strfind(NB_filename, ')');
        fileSuffix = NB_filename(start_identifier:end_identifier);
        
        % identify the scenario (as specified inside the parenthesis)
        start_identifier_sc = regexp(fileSuffix, 'S[1-6]');
        if ~isempty(start_identifier_sc)
            fileScenarios = fileSuffix(start_identifier_sc+1);
            fileScenarios = str2double(fileScenarios);
            fileSuffix = fileSuffix(1:start_identifier_sc-1);
            fileSuffix = strcat(fileSuffix, ')');
        else
            fileScenarios = NaN;
        end
        
        if strcmp(fileSuffix, '()')
            fileSuffix = '';
        end

         if processFiles == 1
            [~, NBCell, scenarioSet] = ProcessNBInputs('NB_input_file', inputFiles{i}, 'fileSuffix', fileSuffix, ...
                'scenarioSet', fileScenarios,'startT0', startT0);
            if any(scenarioSet == 1) % if any of the scenarios processed are the baseline scenario (S1), make sure to create a baselineInput set
                productName = unique(NBCell.PolicyName);
                productName = productName{:};
                NBInput_filename = sprintf('IRC Inputs/%s_NewBusiness_S1_%s %s', ...
                    currentDate, productName,fileSuffix);
                % create baseline input
                createBaselineInputs('NB_filename', NBInput_filename, 'accountValueSets', (0:8)*2); 
            end
        elseif processFiles == 2 % if processing failed and using temp file
            % if loaded excel inputs but processing failed
            [~, NBCell, scenarioSet] = ProcessNBInputs('NB_input_file', 'IRC Inputs/temp', ...
                'PreloadedInputs', 1, 'fileSuffix', fileSuffix, 'scenarioSet', scenarioSet,'startT0', startT0); % only outputs results for last scenario of the set
            if any(scenarioSet == 1)
                productName = unique(NBCell.PolicyName);
                productName = productName{:};
                NBInput_filename = sprintf('IRC Inputs/%s_NewBusiness_S1_%s %s', ...
                    currentDate, productName,fileSuffix);
                % create baseline input
                createBaselineInputs('NB_filename', NBInput_filename, 'accountValueSets', (0:8)*2);
            end
        end
    end
end