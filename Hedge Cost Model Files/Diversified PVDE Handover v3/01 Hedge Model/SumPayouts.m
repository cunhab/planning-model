
%Uses the optimized purchase amounts to figure out the actual payout of the
%hedge portfolio in each macro economic scenarios

AHedgePayouts = [];
SumHedgePayouts = [];
for k=1:numel(SetofEquityShocks)
HedgePayout = PurchaseAmounts.*OptionsPayoutAllShocks(:,k);

%Hedge Payout per instrument
AHedgePayouts = [AHedgePayouts, HedgePayout];

%Total hedge Payout
SumHedgePayouts = [SumHedgePayouts, sum(HedgePayout)];

end
