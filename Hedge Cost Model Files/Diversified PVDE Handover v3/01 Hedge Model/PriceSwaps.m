%-----------------------------PriceSwaps---------------------------------%
% Script #: 3.6
% Calls: N/A
% Variables: SwapPrices represents the price of the 5y and 10yr swap at the
% given instances specified by MasterMacroHedgeOL
%
% This script prices swaps in a given macroeconomic environment from a zero
% curve. The swap is of a 5 or 10 year maturity with the receiver paying
% the 1 year Treasury rate and the payer paying a fixed rate determined at
% the beginning of the year to ensure that the swap price is 0 at purchase.
%-------------------------------------------------------------------------%

% Payment frequency--4 is once a quarter
LegReset = [4, 4];

% Swap principal -- this is $1
SwapPrincipal = [1];

% Settle date 
SettleSwap = CurrentDate;

% [Fixed rate; floating spread], set by the originally calculated value in
% T0ScenarioSpreads
LegRate = [allSwaps.FixedCouponRate allSwaps.BuyStrike];
LegType = [1 0]; % Receive fixed

StartDate = allSwaps.StartDate;
StartDate = max(StartDate, SettleSwap); % make sure start date is aafter settle date
                    
% Keeps the maturity constant, as it is the same instrument.
Maturity = allSwaps.Maturity;

% Determine swap price at the given time -- this does not account for cash
% flows that have already been disbursed. 
SwapRateSpecOriginal = SwapRateSpec;
OriginalRates = SwapRateSpec.Rates;
    
if useContinuousSwaps == 0
    LegRateCC = [allSwaps.FixedCouponRateCC allSwaps.BuyStrike];

    % Change to zero rates
    ZeroRates = exp(SwapRateSpec.Rates)-1;
    SwapRateSpec.Rates = ZeroRates; % why do we do this? doesn't it not matter - matlab can take either input?
    SwapRateSpec.Compounding = 1;
    
    [Price, SwapRate, AI, RecCF, RecCFDates, PayCF, PayCFDates] =...
        swapbyzero(SwapRateSpec, LegRate, SettleSwap, Maturity, 'LegReset', LegReset, 'Principal', SwapPrincipal,...
        'LegType', LegType, 'StartDate', StartDate);
    
    if testSwapDifference == 1
        [Price_temp, SwapRate_temp, AI_temp, RecCF_temp, RecCFDates_temp, PayCF_temp, PayCFDates_temp] =...
            swapbyzero(SwapRateSpecOriginal, LegRateCC, SettleSwap, Maturity, 'LegReset', LegReset, 'Principal', SwapPrincipal,...
            'LegType', LegType, 'StartDate', StartDate);
        
        SwapPrice_SpecDiff = [SwapPrice_SpecDiff, Price_temp - Price];
    end
    
    % Reset rates
    SwapRateSpec.Rates = OriginalRates;
    SwapRateSpec.Compounding = -1;
else
    [Price, SwapRate] = swapbyzero(SwapRateSpec, LegRate, SettleSwap, Maturity, 'LegReset', LegReset, 'Principal', SwapPrincipal,...
                            'LegType', LegType, 'StartDate', StartDate);
end

SwapCashFlows = zeros(size(allSwaps,1),1);
iQuarter = 1;
while iQuarter < (ShockDelay/3 + 1)
    SwapCashFlows = SwapCashFlows + ((allSwaps.FixedCouponRate - allFloatingRates(iQuarter))* SwapPrincipal)/4;
    iQuarter = iQuarter + 1;
end
allSwaps.SwapCashFlows = SwapCashFlows;

% Determines the cash-flow adjusted swap price 
SwapPricesNoCF = Price;
SwapPrices = SwapPricesNoCF;
notForwardSwap = allSwaps.StartDate < CurrentDate; 
% only add CFs to swaps that are not forward swaps
SwapPrices(notForwardSwap) = SwapPrices(notForwardSwap) + SwapCashFlows(notForwardSwap);
allSwaps.Price = SwapPrices;
