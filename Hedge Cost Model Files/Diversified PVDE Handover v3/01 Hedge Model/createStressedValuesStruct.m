% -----------------------------SetCTEValues------------------------------ %
% Script #: 2.2.1
% Calls: N/A
% Variables: CTEHigh_Baseline_400_all_95, CTELow_Baseline_400_all,
% SSR_Baseline_400_all, MCEV_all are carried into InterpolateGridSquares as
% the dependent variables of the second order interpolation
% 
% This script assigns the input CTE values into variables used for the rest
% of the program. Format is pre-defined, so inputs should be aligned to the
% template provided. 
% Input is determined in DetermineCalculatedCTEs, and will either be the
% values associated with the real outer loop or a baseline scenario. 

% ----------------------------------------------------------------------- %

function[StressedValues] = createStressedValuesStruct(CTESetImport)


EquityShocksMilliman = [0;-0.5;-0.5;-0.25;-0.25;0.25;0.25];
InterestRateShocksMilliman = [0;-.01; -.005; -.005; .005; -.01; 0.005];

% Nodes that were provided from Milliman
NodesFromMilliman = [0, 1, 2, 3, 5, 7, 10, 15, 20];

% Assigns variables to CTE 95
high_Baseline_400_base = -CTESetImport(1:9,3)';
high_Baseline_400_shock1 = -CTESetImport(10:18,3)';
high_Baseline_400_shock2 = -CTESetImport(19:27,3)';
high_Baseline_400_shock3 = -CTESetImport(28:36,3)';
high_Baseline_400_shock4 = -CTESetImport(37:45,3)';
high_Baseline_400_shock5 = -CTESetImport(46:54,3)';
high_Baseline_400_shock6 = -CTESetImport(55:63,3)'; 
CTEHigh_Baseline_400_all_95 = vertcat(high_Baseline_400_base,high_Baseline_400_shock1,high_Baseline_400_shock2,high_Baseline_400_shock3,high_Baseline_400_shock4,high_Baseline_400_shock5,high_Baseline_400_shock6);

% Assigns variables to CTE 90 
low_Baseline_400_base = -CTESetImport(1:9,2)';
low_Baseline_400_shock1 = -CTESetImport(10:18,2)';
low_Baseline_400_shock2 = -CTESetImport(19:27,2)';
low_Baseline_400_shock3 = -CTESetImport(28:36,2)';
low_Baseline_400_shock4 = -CTESetImport(37:45,2)';
low_Baseline_400_shock5 = -CTESetImport(46:54,2)';
low_Baseline_400_shock6 = -CTESetImport(55:63,2)';
CTELow_Baseline_400_all = vertcat(low_Baseline_400_base,low_Baseline_400_shock1,low_Baseline_400_shock2,low_Baseline_400_shock3,low_Baseline_400_shock4,low_Baseline_400_shock5,low_Baseline_400_shock6);

% Assigns variables to AG43 SSR
SSR_Baseline_400_base = -CTESetImport(1:9,1)';
SSR_Baseline_400_shock1 = -CTESetImport(10:18, 1)';
SSR_Baseline_400_shock2 = -CTESetImport(19:27, 1)'; 
SSR_Baseline_400_shock3 = -CTESetImport(28:36, 1)';
SSR_Baseline_400_shock4 = -CTESetImport(37:45, 1)'; 
SSR_Baseline_400_shock5 = -CTESetImport(46:54, 1)'; 
SSR_Baseline_400_shock6 = -CTESetImport(55:63, 1)';
SSR_Baseline_400_all = vertcat(SSR_Baseline_400_base, SSR_Baseline_400_shock1, SSR_Baseline_400_shock2, SSR_Baseline_400_shock3, SSR_Baseline_400_shock4, SSR_Baseline_400_shock5, SSR_Baseline_400_shock6);


% Assigns variables to MCEV values
MCEV_base = -CTESetImport(1:9,5)';
MCEV_shock1 = -CTESetImport(10:18,5)';
MCEV_shock2 = -CTESetImport(19:27,5)';
MCEV_shock3 = -CTESetImport(28:36,5)';
MCEV_shock4 = -CTESetImport(37:45,5)';
MCEV_shock5 = -CTESetImport(46:54,5)';
MCEV_shock6 = -CTESetImport(55:63,5)';
MCEV_all = vertcat(MCEV_base, MCEV_shock1, MCEV_shock2, MCEV_shock3, MCEV_shock4, MCEV_shock5, MCEV_shock6);

StressedValues = struct;
StressedValues.CTELow = CTELow_Baseline_400_all;
StressedValues.CTEHigh = CTEHigh_Baseline_400_all_95;
StressedValues.SSR = SSR_Baseline_400_all;
StressedValues.MCEV = MCEV_all;
StressedValues.EquityShocksMilliman = EquityShocksMilliman;
StressedValues.InterestRateShocksMilliman = InterestRateShocksMilliman;
StressedValues.NodesFromMilliman = NodesFromMilliman;

end

