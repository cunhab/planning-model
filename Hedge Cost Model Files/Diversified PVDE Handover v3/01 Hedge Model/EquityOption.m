% ------------------------------EquityOptions-----------------------------%
% Script #: 
% Calls: 
% Variables: 
%
% This class inclues all the methods for pricing equity options
%-------------------------------------------------------------------------%

classdef EquityOption
   properties (SetAccess = protected) 
       CallStrike
       PutStrike
       StartingDate
       Term
       PutExerciseDates
       CallExerciseDates
       priceOnIndex
       OptSpecPut
       OptSpecCall
   end
   
   methods
       % constructor of equity option starting on StartingDate with given 
       % Term, CallStrike and PutStrike moneyness levels
      function obj = EquityOption(StartingDate, Term, StartingIndex, ExpectedEquityReturns, CallStrikeMoneyness, PutStrikeMoneyness, priceOnIndex)
          obj.StartingDate = StartingDate;
          obj.Term = Term;
          obj.PutStrike = PutStrikeMoneyness * StartingIndex * ExpectedEquityReturns(Term/12);
          obj.CallStrike = CallStrikeMoneyness * StartingIndex * ExpectedEquityReturns(Term/12);
          obj.PutExerciseDates = ones(size(obj.PutStrike)) *  addtodate(StartingDate, Term, 'Month');
          obj.CallExerciseDates = ones(size(obj.CallStrike)) *  addtodate(StartingDate, Term, 'Month');
          obj.priceOnIndex = priceOnIndex;
          obj.OptSpecPut = cell(size(obj.PutStrike));
          obj.OptSpecPut(:) = {'put'};
          obj.OptSpecCall = cell(size(obj.CallStrike));
          obj.OptSpecCall(:) = {'call'};
      end
      
      function [PutPrice, CallPrice, PutVega, CallVega, PutSigma, CallSigma] = getEquityOptionPrice(obj, ShockDelay, CurrentIndex, CurrentImpliedVols, CurrentRateSpec, getVega)
          if nargin < 6 || isempty(getVega)
              getVega = 0;
          end
          
          PutVega = NaN(size(obj.PutStrike));
          CallVega = NaN(size(obj.CallStrike));
          PutSigma = NaN(size(obj.PutStrike));
          CallSigma = NaN(size(obj.CallStrike));
          
          CurrentDate = addtodate(obj.StartingDate, ShockDelay, 'Month');
          PutSettle = CurrentDate*ones(size(obj.PutStrike));
          CallSettle = CurrentDate*ones(size(obj.CallStrike));
          
          % return NA if pricing an equity option that has already matured
          if obj.PutExerciseDates(1) < CurrentDate
              PutPrice = NaN(size(obj.PutStrike));
              CallPrice = NaN(size(obj.CallStrike));
              return
          % return difference between Strike and CurrentIndex on date of
          % maturity
          elseif obj.PutExerciseDates(1) == CurrentDate
              PutPrice = max(0, obj.PutStrike-CurrentIndex);
              CallPrice= max(0,CurrentIndex-obj.CallStrike);
          else
              [PutStockSpec, CallStockSpec] = createStockSpec(obj, ShockDelay, CurrentIndex, CurrentImpliedVols);
              PutSigma = PutStockSpec.Sigma;
              CallSigma = CallStockSpec.Sigma;
              
              % REMOVE: WHY DO WE NEED TO ADD 1 DAY TO AN OPTION THAT WILL
              % EXPIRE IN 5 YEARS INSTEAD OF PRICING ON THE ACTUAL MATURITY
              % DATE
              if ShockDelay == 12
                  PutExerciseDatesTemp = obj.PutExerciseDates + 1;
                  CallExerciseDatesTemp = obj.CallExerciseDates + 1;
              else
                  PutExerciseDatesTemp = obj.PutExerciseDates;
                  CallExerciseDatesTemp = obj.CallExerciseDates;
              end
                  
              if getVega == 1
                  OutSpec = {'Price', 'Vega'};
                  [PutPrice, PutVega] = optstocksensbybls(CurrentRateSpec, PutStockSpec, PutSettle, PutExerciseDatesTemp, obj.OptSpecPut, obj.PutStrike, 'OutSpec', OutSpec);
                  [CallPrice, CallVega] = optstocksensbybls(CurrentRateSpec, CallStockSpec, CallSettle, CallExerciseDatesTemp, obj.OptSpecCall, obj.CallStrike, 'OutSpec', OutSpec);
              else
                  OutSpec = {'Price'};
                  [PutPrice] = optstocksensbybls(CurrentRateSpec, PutStockSpec, PutSettle, PutExerciseDatesTemp, obj.OptSpecPut, obj.PutStrike, 'OutSpec', OutSpec);
                  [CallPrice] = optstocksensbybls(CurrentRateSpec, CallStockSpec, CallSettle, CallExerciseDatesTemp, obj.OptSpecCall, obj.CallStrike, 'OutSpec', OutSpec);
              end
          end
      end
      
   end
   
   methods (Access = private)
       % creates the StockSpec structure needed for Matlab opstocksensbybls
       % function. Sets the option's underlying stock's asset price,
       % dividend, and volatility.
       function [PutStockSpec, CallStockSpec] = createStockSpec(obj, ShockDelay, CurrentIndex, CurrentImpliedVols)
           % Asset price
           if CurrentIndex<0
               AssetPrice = 1;
           else
               AssetPrice = CurrentIndex;
           end
           
           % Dividends
           DividendType = {'continuous'}; 

           % S&P dividend percent per year
           if obj.priceOnIndex == 1
               DividendPerc = [0.02]; 
           else
               DividendPerc = [0];
           end
           
           [PutSigma, CallSigma] = getSigma(obj, ShockDelay, CurrentIndex, CurrentImpliedVols);
           
           % Runs the stockspec formula which includes volatility, asset price,
           % dividend information
           PutStockSpec= stockspec(PutSigma, AssetPrice, DividendType, DividendPerc);
           CallStockSpec= stockspec(CallSigma, AssetPrice, DividendType, DividendPerc);
       end
       
       % get the volatilities that correspond to options at given time
      % (given moneyness and MTM)
      function [PutSigma, CallSigma] = getSigma(obj, ShockDelay, CurrentIndex, CurrentImpliedVols)
          VolatilityITM = CurrentImpliedVols(1,2:size(CurrentImpliedVols,2));
          VolatilityMatureMonths = CurrentImpliedVols(2:size(CurrentImpliedVols,1),1);
          CurrentImpliedVols = CurrentImpliedVols(2:size(CurrentImpliedVols,1),2:size(CurrentImpliedVols,2));

          PutITMnessTrue = getMoneyness(obj.PutStrike, CurrentIndex);
          CallITMnessTrue = getMoneyness(obj.CallStrike, CurrentIndex);

          %Recalculates the months to maturity based on how much time has passed and
          %pulls out the row from the volatility tables that that months to maturity
          %corresponds to
          [rowIndices,~]=find(VolatilityMatureMonths>=(obj.Term-ShockDelay));
          PutMTMrow = ones(size(obj.PutStrike)) * rowIndices(1);
          CallMTMrow = ones(size(obj.CallStrike)) * rowIndices(1);
                    
          PutSigma = interp1(VolatilityITM, CurrentImpliedVols(PutMTMrow(1),:),PutITMnessTrue, 'linear');
          CallSigma = interp1(VolatilityITM, CurrentImpliedVols(CallMTMrow(1),:),CallITMnessTrue, 'linear');
      end
   end
end

%Recalculate ITMness of the option based on current market conditions
function ITMness = getMoneyness(optionStrikes, CurrentIndex)
ITMness = zeros(size(optionStrikes));

%Recalculate ITMness of the option based on current market conditions
for n=1:numel(optionStrikes)
    ITMnessTemp = (100*optionStrikes(n)/CurrentIndex);
    if ITMnessTemp >1000
        ITMnessTemp = 1000;
    elseif ITMnessTemp < 0
        ITMnessTemp = 0;
    end
    ITMness(n) = ITMnessTemp;
end
end